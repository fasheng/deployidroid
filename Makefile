gencode:
	./gencode.sh

.PHONY: test
test:
	cd test; bats -p ./test.bats
