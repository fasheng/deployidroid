#!/usr/bin/env bats

source ../include/utils.sh
source ../include/adb_utils.sh
source ../include/fdroid_cmds.sh
source ../include/backup_cmds.sh

deployidroid="../deployidroid --config ./config.sh"

# print to console
function bats_echo {
  echo "${@}" >&3
}

@test "help command" {
  run ${deployidroid} --help
  [ "${status}" -eq 0 ]
  [[ "${output}" =~ ^"deployidroid <command> [Global Options] [args] [-h|--help]" ]]

  run ${deployidroid} backup --help
  [ "${status}" -eq 0 ]
  [[ "${output}" =~ ^"backup [--name name] [--password pwd]" ]]

  run ${deployidroid} unknown --help
  [ "${status}" -eq 127 ]
}

@test "list command" {
  run ${deployidroid} list fdroid
  [[ "${output}" =~ "F-Droid" ]]

  run ${deployidroid} list --appid "org.fdroid.fdroid" --ver --cat --desc --sug
  [[ "${output}" =~ "category: System" ]]
  [[ "${output}" =~ "description: F-Droid is" ]]
  [[ "${output}" =~ "versions:" ]]
  [[ "${output}" =~ "suggest version:" ]]

  run ${deployidroid} list --appid "org.fdroid.fdroid" --sug
  [[ "${output}" =~ "suggest version: 1.9 (1009000)" ]]
  run ${deployidroid} list --appid "org.fdroid.fdroid" --sug --allow-unstable
  [[ "${output}" =~ "suggest version: 1.10-alpha0 (1010000)" ]]

  run ${deployidroid} list --appid "org.fdroid.fdroid" --appid "com.nextcloud.client"
  [[ "${output}" =~ "F-Droid" ]]
  [[ "${output}" =~ "Nextcloud" ]]

  run ${deployidroid} list --appid "com.termux" --repo "fdroid" --ver
  [[ "${output}" =~ "url=https://f-droid.org/repo/com.termux_96.apk" ]]

  run ${deployidroid} list --appid "com.termux" --repo "nethunter" --ver
  [[ "${output}" =~ "url=https://store.nethunter.com/repo/com.termux_95.apk" ]]
}

@test "fdroid get suggest version" {
  declare -A fdroid_repos
  local func_output=
  source ./config.sh

  func_output=$(fdroid_get_app_suggest_version_code "com.ubergeek42.WeechatAndroid" "16" "armeabi")
  [ "${func_output}" = "617" ]
  func_output=$(fdroid_get_app_suggest_version_code "com.ubergeek42.WeechatAndroid" "21" "armeabi")
  [ "${func_output}" = "1000" ]
  func_output=$(fdroid_get_app_suggest_version_code "com.ubergeek42.WeechatAndroid" "15" "armeabi")
  [ "${func_output}" = "" ]

  func_output=$(fdroid_get_app_suggest_version_code "eu.flatworld.android.slider" "16" "armeabi")
  [ "${func_output}" = "20301" ]
  func_output=$(fdroid_get_app_suggest_version_code "eu.flatworld.android.slider" "16" "armeabi-v7a")
  [ "${func_output}" = "20301" ]
  func_output=$(fdroid_get_app_suggest_version_code "eu.flatworld.android.slider" "16" "arm64-v8a")
  [ "${func_output}" = "20301" ]

  func_output=$(fdroid_get_app_suggest_version_code "com.xda.labs" "16" "arm64-v8a")
  [ "${func_output}" = "11600" ]
  func_output=$(fdroid_get_app_suggest_version_code "com.xda.labs" "16" "armeabi")
  [ "${func_output}" = "11400" ]
  func_output=$(fdroid_get_app_suggest_version_code "com.xda.labs" "16" "unknownarch")
  [ "${func_output}" = "" ]
}

@test "parse profile application line" {
  local appuser= appid=; unset appline_options; declare -A appline_options
  backup_parse_appline "org.fdroid.fdroid:type=app+data;repo=fdroid;version_code=1009000;install_path=/system/app;allow_unstable=1" "system"
  [ "${appuser}" = "system" ]
  [ "${appid}" = "org.fdroid.fdroid" ]
  [ "${appline_options[type]}" = "app+data" ]
  [ "${appline_options[repo]}" = "fdroid" ]
  [ "${appline_options[version_code]}" = "1009000" ]
  [ "${appline_options[install_path]}" = "/system/app" ]
  [ "${appline_options[allow_unstable]}" = "1" ]

  appuser= appid=; unset appline_options; declare -A appline_options
  backup_parse_appline "user_0:org.fdroid.fdroid:type=app+data;repo=fdroid"
  [ "${appuser}" = "0" ]
  [ "${appid}" = "org.fdroid.fdroid" ]
  [ "${appline_options[type]}" = "app+data" ]
  [ "${appline_options[repo]}" = "fdroid" ]

  appuser= appid=; unset appline_options; declare -A appline_options
  backup_parse_appline "org.fdroid.fdroid:type=app+data;repo=fdroid"
  [ "${appuser}" = "" ]
  [ "${appline_options[type]}" = "app+data" ]
  [ "${appline_options[repo]}" = "fdroid" ]

  appuser= appid=; unset appline_options; declare -A appline_options
  backup_parse_appline "org.fdroid.fdroid"
  [ "${appuser}" = "" ]

  appuser= appid=; unset appline_options; declare -A appline_options
  backup_parse_appline "user_1.pkg.com:type=app+data"
  [ "${appuser}" = "" ]
  [ "${appid}" = "user_1.pkg.com" ]
  [ "${appline_options[type]}" = "app+data" ]

  appuser= appid=; unset appline_options; declare -A appline_options
  local backup_arg_options="type=app;version_code="
  backup_parse_appline "org.fdroid.fdroid:type=app+data;version_code=1009000"
  [ "${appuser}" = "" ]
  [ "${appid}" = "org.fdroid.fdroid" ]
  [ "${appline_options[type]}" = "app" ]
  [ "${appline_options[version_code]}" = "" ]
}

@test "compare string versions" {
  compver_eq "0.9.5" "0.9.5"

  compver_gt "0.9.5" ""
  compver_gt "0.9.5" "0"
  compver_gt "0.9.5" "0.9.4"
  compver_gt "0.9.5" "0.9"
  compver_ge "0.9.5" "0.9"
  compver_ge "0.9.5" "0.9.5"

  compver_lt "" "0.9.5"
  compver_lt "0.9.5" "1.0.0"
  compver_lt "0.9.5" "1.0"
  compver_le "0.9.5" "1.0"
  compver_le "0.9.5" "0.9.5"
}

@test "fix old profile format" {
  local tmp_profile="profile/_tmp_old_1.sh"
  cp -f "profile/old_1.sh" "${tmp_profile}"
  run ${deployidroid} check --fix --profile "${tmp_profile}"
  run cat "${tmp_profile}"
  [[ "${output}" =~ "version=" ]]
  [[ "${output}" =~ "system_apps=" ]]
  [[ "${output}" =~ user_apps[+]= ]]
  [[ "${output}" =~ "'org.fdroid.fdroid:type=app'" ]]
  [[ "${output}" =~ "'user_10:org.fdroid.fdroid:type=app+data;repo=fdroid'" ]]
  [[ "${output}" =~ "'user_10:@user.wallpaper:type=data'" ]]

  tmp_profile="profile/_tmp_old_2.sh"
  cp -f "profile/old_2.sh" "${tmp_profile}"
  run ${deployidroid} check --fix --profile "${tmp_profile}"
  run cat "${tmp_profile}"
  [[ "${output}" =~ "version=" ]]
  [[ "${output}" =~ "system_apps=" ]]
  [[ "${output}" =~ user_apps[+]= ]]
  [[ "${output}" =~ "'org.fdroid.fdroid:type=app'" ]]
  [[ "${output}" =~ "'user_10:org.fdroid.fdroid:type=app+data;repo=fdroid'" ]]
  [[ "${output}" =~ "'user_10:@user.wallpaper:type=data'" ]]
}

@test "build default profile" {
  source ./mock/mock_adb_users.sh
  local profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=()
  backup_prepare_args "org.fdroid.fdroid"
  [ "${#user_apps[@]}" -eq 1 ]
  [ x"${user_apps[0]}" = x"user_0:org.fdroid.fdroid" ]

  profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=()
  backup_prepare_args "user_all:org.fdroid.fdroid"
  [ "${#user_apps[@]}" -eq 2 ]
  [ x"${user_apps[0]}" = x"user_0:org.fdroid.fdroid" ]
  [ x"${user_apps[1]}" = x"user_10:org.fdroid.fdroid" ]

  profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=()
  backup_prepare_args "user_sys:com.google.android.gms" "user_cur:org.fdroid.fdroid"
  [ "${#system_apps[@]}" -eq 1 ]
  [ x"${system_apps[0]}" = x"user_system:com.google.android.gms" ]
  [ "${#user_apps[@]}" -eq 1 ]
  [ x"${user_apps[0]}" = x"user_0:org.fdroid.fdroid" ]

  profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=()
  backup_prepare_args "@wifi" "@user.accounts"
  [ "${#system_apps[@]}" -eq 1 ]
  [ x"${system_apps[0]}" = x"user_system:@wifi" ]
  [ "${#user_apps[@]}" -eq 1 ]
  [ x"${user_apps[0]}" = x"user_0:@user.accounts" ]

  profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=()
  backup_prepare_args ./data/default/user_system/@wifi/appinfo.sh ./data/default/user_0/@user.accounts/appinfo.sh ./data/default/user_0/org.fdroid.fdroid/appinfo.sh
  [ "${#system_apps[@]}" -eq 1 ]
  [ x"${system_apps[0]}" = x"user_system:@wifi" ]
  [ "${#user_apps[@]}" -eq 2 ]
  [ x"${user_apps[0]}" = x"user_0:@user.accounts" ]
  [ x"${user_apps[1]}" = x"user_0:org.fdroid.fdroid" ]
}
