#!/usr/bin/env bats

bats_require_minimum_version 1.5.0

source ../include/utils.sh
source ../include/adb_utils.sh
source ../include/fdroid_cmds.sh
source ../include/backup_cmds.sh

deployidroid="../deployidroid --config ./config.sh"

# print to console
function bats_echo {
  echo "${@}" >&3
}

@test "compare string versions" {
  compver_eq "0.9.5" "0.9.5"

  compver_gt "0.9.5" ""
  compver_gt "0.9.5" "0"
  compver_gt "0.9.5" "0.9.4"
  compver_gt "0.9.5" "0.9"
  compver_ge "0.9.5" "0.9"
  compver_ge "0.9.5" "0.9.5"

  compver_lt "" "0.9.5"
  compver_lt "0.9.5" "1.0.0"
  compver_lt "0.9.5" "1.0"
  compver_le "0.9.5" "1.0"
  compver_le "0.9.5" "0.9.5"
}

@test "safe eval" {
  local a= b=
  safe_eval "a=123;b=456"
  [ "${a}" = "123" ]
  [ "${b}" = "456" ]

  local a= b=
  safe_eval "a=123;b=456" a
  [ "${a}" = "123" ]
  [ "${b}" = "" ]

  local a= b=
  safe_eval "  a=123;  b=456"
  [ "${a}" = "123" ]
  [ "${b}" = "456" ]

  local a= b=
  safe_eval "  a='123';  b=\"456\""
  [ "${a}" = "123" ]
  [ "${b}" = "456" ]

  local a= b=
  safe_eval "  a='123'
b='456'"
  [ "${a}" = "123" ]
  [ "${b}" = "456" ]

  local install_path=
  safe_eval "install_path='/data/app/~~loj2yiMVo8MDxFyRJd4H3w=='"
  [ "${install_path}" = "/data/app/~~loj2yiMVo8MDxFyRJd4H3w==" ]
}

@test "safe source" {
  local id= display_name= version_name= version_code= android_version= android_api_level= is_system= is_encrypted= install_dir= data_timestamp=
  safe_source ./data/default/user_0/org.fdroid.fdroid/appinfo.sh
  [ "${id}" = 'org.fdroid.fdroid' ]
  [ "${version_name}" = '1.8' ]
  [ "${version_code}" = '1008050' ]
  [ "${android_api_level}" = '29' ]
  [ "${is_system}" = '' ]
  [ "${is_encrypted}" = '' ]
  [ "${install_path}" = '/data/app/~~loj2yiMVo8MDxFyRJd4H3w==' ]
  [ "${archive_type}" = 'tar.gz' ]
  [ "${data_timestamp}" = '1579084590' ]
}

@test "remove item from array" {
  local -a test_array=(
    'org.fdroid.fdroid:type=app+data'
    'org.fossify.gallery:type=app+data;repo=fdroid'
    'net.sourceforge.opencamera:type=app+data'
  )
  [ "${#test_array[@]}" = 3 ]

  remove_array_match test_array "^org"
  [ "${#test_array[@]}" = 1 ]

  remove_array_match test_array "opencamera"
  [ "${#test_array[@]}" = 0 ]

  local -a test_array=(
    'org.fdroid.fdroid:type=app+data'
    'org.fossify.gallery:type=app+data;repo=fdroid'
    'net.sourceforge.opencamera:type=app+data'
  )
  [ "${#test_array[@]}" = 3 ]

  remove_array_unmatch test_array "opencamera"
  [ "${#test_array[@]}" = 1 ]

  remove_array_unmatch test_array "^org"
  [ "${#test_array[@]}" = 0 ]
}

@test "replace item content in array" {
  local -a test_array=(
    'org.fdroid.fdroid:type=app+data'
    'org.fossify.gallery:type=app+data;repo=fdroid'
    'net.sourceforge.opencamera:type=app+data'
  )
  replace_array test_array "type=app+data" "type=app"
  [ "${test_array[0]}" = 'org.fdroid.fdroid:type=app' ]
  [ "${test_array[1]}" = 'org.fossify.gallery:type=app;repo=fdroid' ]
  [ "${test_array[2]}" = 'net.sourceforge.opencamera:type=app' ]
}

@test "get fdroid available repo names" {
  declare -A fdroid_repos=(
    ["fdroid"]="https://f-droid.org/repo"
    ["fdroid2"]="https://f-droid2.org/repo"
    ["fdroid 3"]="https://f-droid3.org/repo"
  )
  local fdroid_arg_repos=()
  local func_output=

  func_output=$(fdroid_get_repo_names | sort)
  [ "${func_output}" = "fdroid
fdroid2
fdroid 3" ]

  fdroid_arg_repos=("fdroid" "fdroid 3")
  func_output=$(fdroid_get_repo_names | sort)
  [ "${func_output}" = "fdroid
fdroid 3" ]

  fdroid_arg_repos=("fdroid")
  func_output=$(fdroid_get_repo_names | sort)
  [ "${func_output}" = "fdroid" ]
}

@test "get suggest version from fdroid repo" {
  declare -A fdroid_repos
  local func_output=
  source ./config.sh

  func_output=$(fdroid_get_app_suggest_version_code "com.ubergeek42.WeechatAndroid" "16" "armeabi")
  [ "${func_output}" = "617" ]
  func_output=$(fdroid_get_app_suggest_version_code "com.ubergeek42.WeechatAndroid" "21" "armeabi")
  [ "${func_output}" = "1000" ]
  func_output=$(fdroid_get_app_suggest_version_code "com.ubergeek42.WeechatAndroid" "15" "armeabi")
  [ "${func_output}" = "" ]

  func_output=$(fdroid_get_app_suggest_version_code "eu.flatworld.android.slider" "16" "armeabi")
  [ "${func_output}" = "20301" ]
  func_output=$(fdroid_get_app_suggest_version_code "eu.flatworld.android.slider" "16" "armeabi-v7a")
  [ "${func_output}" = "20301" ]
  func_output=$(fdroid_get_app_suggest_version_code "eu.flatworld.android.slider" "16" "arm64-v8a")
  [ "${func_output}" = "20301" ]

  func_output=$(fdroid_get_app_suggest_version_code "com.xda.labs" "16" "arm64-v8a")
  [ "${func_output}" = "11600" ]
  func_output=$(fdroid_get_app_suggest_version_code "com.xda.labs" "16" "armeabi")
  [ "${func_output}" = "11400" ]
  func_output=$(fdroid_get_app_suggest_version_code "com.xda.labs" "16" "unknownarch")
  [ "${func_output}" = "" ]
}

@test "get local app file path from fdroid repo" {
  declare -A fdroid_repos
  source ./config.sh
  [ $(fdroid_get_app_path "org.fdroid.fdroid" "1009000") = "./fdroid-cache/app/fdroid/org.fdroid.fdroid_1009000.apk" ]
}

@test "parse profile application line" {
  local appuser= appid=; unset appline_options; declare -A appline_options
  backup_parse_appline "org.fdroid.fdroid:type=app+data;repo=fdroid;version_code=1009000;install_path=/system/app;allow_unstable=1" "system"
  [ "${appuser}" = "system" ]
  [ "${appid}" = "org.fdroid.fdroid" ]
  [ "${appline_options[type]}" = "app+data" ]
  [ "${appline_options[repo]}" = "fdroid" ]
  [ "${appline_options[version_code]}" = "1009000" ]
  [ "${appline_options[install_path]}" = "/system/app" ]
  [ "${appline_options[allow_unstable]}" = "1" ]

  appuser= appid=; unset appline_options; declare -A appline_options
  backup_parse_appline "user_0:org.fdroid.fdroid:type=app+data;repo=fdroid"
  [ "${appuser}" = "0" ]
  [ "${appid}" = "org.fdroid.fdroid" ]
  [ "${appline_options[type]}" = "app+data" ]
  [ "${appline_options[repo]}" = "fdroid" ]

  appuser= appid=; unset appline_options; declare -A appline_options
  backup_parse_appline "org.fdroid.fdroid:type=app+data;repo=fdroid"
  [ "${appuser}" = "" ]
  [ "${appline_options[type]}" = "app+data" ]
  [ "${appline_options[repo]}" = "fdroid" ]

  appuser= appid=; unset appline_options; declare -A appline_options
  backup_parse_appline "org.fdroid.fdroid"
  [ "${appuser}" = "" ]

  appuser= appid=; unset appline_options; declare -A appline_options
  backup_parse_appline "user_1.pkg.com:type=app+data"
  [ "${appuser}" = "" ]
  [ "${appid}" = "user_1.pkg.com" ]
  [ "${appline_options[type]}" = "app+data" ]

  appuser= appid=; unset appline_options; declare -A appline_options
  local backup_arg_options="type=app;version_code="
  backup_parse_appline "org.fdroid.fdroid:type=app+data;version_code=1009000"
  [ "${appuser}" = "" ]
  [ "${appid}" = "org.fdroid.fdroid" ]
  [ "${appline_options[type]}" = "app" ]
  [ "${appline_options[version_code]}" = "" ]
}

@test "build default profile" {
  source ./mock/mock_adb_users.sh
  local profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=()
  backup_prepare_args "org.fdroid.fdroid"
  [ "${#user_apps[@]}" -eq 1 ]
  [ "${user_apps[0]}" = "user_0:org.fdroid.fdroid" ]

  profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=()
  backup_prepare_args "user_all:org.fdroid.fdroid"
  [ "${#user_apps[@]}" -eq 2 ]
  [ "${user_apps[0]}" = "user_0:org.fdroid.fdroid" ]
  [ "${user_apps[1]}" = "user_10:org.fdroid.fdroid" ]

  profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=()
  backup_prepare_args "user_sys:com.google.android.gms" "user_cur:org.fdroid.fdroid"
  [ "${#system_apps[@]}" -eq 1 ]
  [ "${system_apps[0]}" = "user_system:com.google.android.gms" ]
  [ "${#user_apps[@]}" -eq 1 ]
  [ "${user_apps[0]}" = "user_0:org.fdroid.fdroid" ]

  profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=()
  backup_prepare_args "@wifi" "@user.accounts"
  [ "${#system_apps[@]}" -eq 1 ]
  [ "${system_apps[0]}" = "user_system:@wifi" ]
  [ "${#user_apps[@]}" -eq 1 ]
  [ "${user_apps[0]}" = "user_0:@user.accounts" ]

  profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=()
  backup_prepare_args ./data/default/user_system/@wifi/appinfo.sh ./data/default/user_0/@user.accounts/appinfo.sh ./data/default/user_0/org.fdroid.fdroid/appinfo.sh
  [ "${#system_apps[@]}" -eq 1 ]
  [ "${system_apps[0]}" = "user_system:@wifi" ]
  [ "${#user_apps[@]}" -eq 2 ]
  [ "${user_apps[0]}" = "user_0:@user.accounts" ]
  [ "${user_apps[1]}" = "user_0:org.fdroid.fdroid" ]
}

@test "help command" {
  run ${deployidroid} --help
  [ "${status}" -eq 0 ]
  [[ "${output}" =~ ^"deployidroid <command> [Global Options] [args] [-h|--help]" ]]

  run ${deployidroid} backup --help
  [ "${status}" -eq 0 ]
  [[ "${output}" =~ ^"backup [--name name] [--password pwd]" ]]

  run -127 ${deployidroid} unknown --help
  [ "${status}" -eq 127 ]
}

@test "list command" {
  run ${deployidroid} list fdroid
  [[ "${output}" =~ "F-Droid" ]]

  run ${deployidroid} list --appid "org.fdroid.fdroid" --ver --cat --desc --sug
  [[ "${output}" =~ "categories: System" ]]
  [[ "${output}" =~ "description: F-Droid is" ]]
  [[ "${output}" =~ "versions:" ]]
  [[ "${output}" =~ "suggest version:" ]]

  run ${deployidroid} list --appid "org.fdroid.fdroid" --sug
  [[ "${output}" =~ "suggest version: 1.9 (1009000)" ]]
  run ${deployidroid} list --appid "org.fdroid.fdroid" --sug --allow-unstable
  [[ "${output}" =~ "suggest version: 1.10-alpha0 (1010000)" ]]

  run ${deployidroid} list --appid "org.fdroid.fdroid" --appid "com.nextcloud.client"
  [[ "${output}" =~ "F-Droid" ]]
  [[ "${output}" =~ "Nextcloud" ]]

  run ${deployidroid} list --appid "com.termux" --repo "fdroid" --ver
  [[ "${output}" =~ "url=https://f-droid.org/repo/com.termux_96.apk" ]]

  run ${deployidroid} list --appid "com.termux" --repo "nethunter" --ver
  [[ "${output}" =~ "url=https://store.nethunter.com/repo/com.termux_95.apk" ]]
}

@test "check command" {
  local tmp_profile="profile/_tmp_old_1.sh"
  cp -f "profile/old_1.sh" "${tmp_profile}"
  run ${deployidroid} check --fix --profile "${tmp_profile}"
  run cat "${tmp_profile}"
  [[ "${output}" =~ "version=" ]]
  [[ "${output}" =~ "system_apps=" ]]
  [[ "${output}" =~ user_apps[+]= ]]
  [[ "${output}" =~ "'org.fdroid.fdroid:type=app'" ]]
  [[ "${output}" =~ "'user_10:org.fdroid.fdroid:type=app+data;repo=fdroid'" ]]
  [[ "${output}" =~ "'user_10:@user.wallpaper:type=data'" ]]

  tmp_profile="profile/_tmp_old_2.sh"
  cp -f "profile/old_2.sh" "${tmp_profile}"
  run ${deployidroid} check --fix --profile "${tmp_profile}"
  run cat "${tmp_profile}"
  [[ "${output}" =~ "version=" ]]
  [[ "${output}" =~ "system_apps=" ]]
  [[ "${output}" =~ user_apps[+]= ]]
  [[ "${output}" =~ "'org.fdroid.fdroid:type=app'" ]]
  [[ "${output}" =~ "'user_10:org.fdroid.fdroid:type=app+data;repo=fdroid'" ]]
  [[ "${output}" =~ "'user_10:@user.wallpaper:type=data'" ]]
}
