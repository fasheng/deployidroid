# cache_dir="${HOME}/Downloads/fdroid-cache"
fdroid_cache_dir="./fdroid-cache"

# adb_device_tmpdir='/sdcard/tmp'

# utils_download_cmd="wget -c '%s'"

# fdroid_allow_unstable=1

fdroid_repos=(
  ["fdroid"]="https://f-droid.org/repo"
  ["nethunter"]="https://store.nethunter.com/repo"
  ["izzy"]="https://apt.izzysoft.de/fdroid/repo"
)
