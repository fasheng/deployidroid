name='old'
appdir='./app'
datadir='./data/old'
android_version='9'
android_api_level=28
users=(system 0 10)

user_system_name=''
user_system_datadir="${datadir}/user_system"
user_system_apps=(
  # spec system data
  '@bluetooth'
  '@datausage'
  '@wifi'
)

user_0_name='Owner'
user_0_datadir="${datadir}/user_0"
user_0_apps=(
  # 3rd apps
  'org.fdroid.fdroid:app'
  # spec user data
  '@user.accounts'
  '@user.appwidgets'
  '@user.icon'
  '@user.wallpaper'
)

user_10_name='10'
user_10_datadir="${datadir}/user_10"
user_10_apps=(
  # 3rd apps
  'org.fdroid.fdroid:app+data:repo=fdroid'
  # spec user data
  '@user.accounts'
  '@user.appwidgets'
  '@user.icon'
  '@user.wallpaper'
)
