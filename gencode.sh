#!/usr/bin/env bash

# merge include code
target_file=./deployidroid
include_files=(
  ./include/utils.sh
  ./include/adb_utils.sh
  ./include/adb_cmds.sh
  ./include/fastboot_cmds.sh
  ./include/backup_cmds.sh
  ./include/deploy_cmds.sh
  ./include/fdroid_cmds.sh
)
seprate_begin='#### BEGIN_INCLUDE'
seprate_end='#### END_INCLUDE'

target_file_bak="${target_file}.bak"
cp -f "${target_file}" "${target_file_bak}"
awk -F"${seprate_begin}" '{print $1}' RS='^$' ORS='' "${target_file_bak}" > "${target_file}"
echo "${seprate_begin}" >> "${target_file}"

for f in "${include_files[@]}"; do
  echo -e "\n###** Include $(basename "${f}")\n" >> "${target_file}"
  cat "${f}" >> "${target_file}"
done

echo -n "${seprate_end}" >> "${target_file}"
awk -F"${seprate_end}" '{print $2}' RS='^$' ORS='' "${target_file_bak}" >> "${target_file}"
rm "${target_file_bak}"

# generate completion code
mkdir -p completion
"${target_file}" complete --fish > completion/"${target_file}".fish
"${target_file}" complete --bash > completion/"${target_file}".bash

# generate usage.org
touch usage.org
echo '#+begin_example' > usage.org
"${target_file}" --help -v >> usage.org
echo '#+end_example' >> usage.org
