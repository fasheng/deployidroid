function __fish_complete_deployidroid_no_subcommand
  for i in (commandline -opc)
    if contains -- $i adb backup check cleanup complete deploy download install list new restore uninstall update upgrade 
      return 1
    end
  end
  return 0
end

function __fish_complete_deployidroid_contains_file
  set -l cmd (commandline -poc)
  if string match -r -q -- '^[./~]' $cmd[(count $cmd)]
    return 0
  end
  return 1
end

function __fish_complete_deployidroid_apps_cond
  set -l cmd (commandline -po)
  set -e cmd[1]
  if __fish_seen_subcommand_from $argv
     and string match -r -q -- '^[^./~-]' $cmd[(count $cmd)]
    return 0
  end
  return 1
end

function __fish_complete_deployidroid_adb_devices
  adb devices -l | string replace -rf '(\S+) +.' '$1'\t | string replace -rf \t'.*model:(\S+).*' \t'$1'
end

function __fish_complete_deployidroid_adb_users
  adb shell pm list users 2>/dev/null | grep -o '{.*}' | tr -d '{}' | awk -F: '{print $1}'
end

function __fish_complete_deployidroid_backupapplines
  for i in @bluetooth @datausage @user.accounts @user.appwidgets @user.icon @user.wallpaper @wifi
    echo $i\tSpecialApp
  end
  for i in (__fish_complete_deployidroid_adb_users)
    if test $i -eq 0
      adb shell pm list packages -3 --user $i 2>/dev/null | string replace 'package:' '' | string replace -a \r '' | string replace -r '$' '\tUserApp'
    else
      adb shell pm list packages -3 --user $i 2>/dev/null | string replace 'package:' "user_$i:" | string replace -a \r '' | string replace -r '$' '\tUserApp'
    end
  end
  adb shell pm list packages -s 2>/dev/null | string replace 'package:' 'user_system:' | string replace -a \r '' | string replace -r '$' '\tSystemApp'
end
function __fish_complete_deployidroid_deviceapps
  adb shell pm list packages -3 2>/dev/null | string replace 'package:' '' | string replace -a \r '' | string replace -r '$' '\tUserApp'
  adb shell pm list packages -s 2>/dev/null | string replace 'package:' '' | string replace -a \r '' | string replace -r '$' '\tSystemApp'
end
function __fish_complete_deployidroid_fdroid_apps
  set -l cmd (commandline -po)
  set -l app_file $cmd[1]
  $app_file list --no-color | sed 's/ - /	/' 2>/dev/null
end
function __fish_complete_deployidroid_fdroid_verapps
  set -l cmd (commandline -po)
  set -l app_file $cmd[1]
  $app_file list --only-ver --appid 2>/dev/null
end

complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a '-h' -d 'show help message'
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a '--help' -d 'show help message'
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'adb' -d 'run adb wrapped commands, helps for debug work'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'autoshell' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'check-online' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'check-root' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'chown-recursive' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'clear-default-launcher' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'clear-package' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'create-user' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'disable' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'disable-autostart' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'disable-battery-optimization' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'disable-run-background' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'enable' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'enable-autostart' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'enable-battery-optimization' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'enable-run-background' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'ensure-user-exists' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'fix-package-datadir-permission' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'fix-selinux-permission' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-android-abi-list' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-android-api-level' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-android-version' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-current-user' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-default-launcher' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-file-timestamp' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-kernel-version' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-package-datadir' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-package-display-name' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-package-path' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-package-uid' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-package-version-code' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-package-version-name' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'get-user-name' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'grant-location-permission' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'grant-permission' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'grant-storage-permission' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'has-root' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'install' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'install-sys' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-3rd-package' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-cmd-exists' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-dir-exists' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-file-exists' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-mountpoint' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-mountpoint-ro' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-mountpoint-rw' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-online' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-only-one-user' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-package-installed' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-pm-support-multiuser' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-support-multiuser' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-sys-package' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'is-user-exists' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'kill-package' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'list-3rd-packages' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'list-autostart-components' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'list-packages' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'list-sys-packages' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'list-users' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'make-pkg-installed-for-user' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'pull' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'push' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'remount-ro' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'remount-rw' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'remount-system-ro' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'remount-system-rw' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'revoke-location-permission' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'revoke-permission' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'revoke-storage-permission' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'rootshell' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'set-default-launcher' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'set-file-timestamp' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'shell' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'start-package' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'stop-package' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'uninstall' -d 'Subcommand'
complete -n '__fish_seen_subcommand_from adb' -c deployidroid -a 'uninstall-sys' -d 'Subcommand'
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'backup' -d 'backup android application and user data'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'name' -d 'profile name [default: 'default' or profile filename]'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'password' -d 'encrypt the user data archive with password'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'options' -d 'override options in APPLINE, such as type=app+data;repo=fdroid'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'filter' -d 'only apply APPLINE that match the filter'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'appdir' -d 'special application direcotory [default: './app']'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'device-tmpdir' -d 'special tmpdir in android [default: '/data/local/tmp']'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'archive-side' -d 'create and extract archive in device or host [default: 'auto']'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'profile' -d 'target profile file'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -s 'c' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'config' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'busybox' -d 'special busybox command in android'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'device-tmpdir' -d 'special tmpdir in android [default: '/data/local/tmp']'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'root-type' -d 'could be 'auto', 'su', 'adb-root' or 'none' (default: 'auto')'
complete -x -n '__fish_seen_subcommand_from backup' -c deployidroid -s 's' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -x -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'serial' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'no-color' -d 'remove colorful message'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -s 'v' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'verbose' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -s 'vv' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'verbose2' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -s 'h' -d 'show help message'
complete -n '__fish_seen_subcommand_from backup' -c deployidroid -l 'help' -d 'show help message'
complete -f -n '__fish_complete_deployidroid_apps_cond backup' -c deployidroid -a "(__fish_complete_deployidroid_backupapplines)"
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'check' -d 'check backup app and data in special directory or profile'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -l 'fix' -d 'fix incompatible issues if possible'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -l 'clean' -d 'remove apk files that lost reference, only works without --profile'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -l 'copy-to' -d 'copy the related app and data to another directory, only works with --profile'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -l 'move-to' -d 'move the related app and data to another directory, only works with --profile'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -l 'appdir' -d 'special application direcotory [default: './app']'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -l 'profile' -d 'target profile file'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -s 'c' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -l 'config' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -l 'no-color' -d 'remove colorful message'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -s 'v' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -l 'verbose' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -s 'vv' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -l 'verbose2' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -s 'h' -d 'show help message'
complete -n '__fish_seen_subcommand_from check' -c deployidroid -l 'help' -d 'show help message'
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'cleanup' -d 'cleanup applications from profile'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'options' -d 'override options in APPLINE, such as type=app+data;repo=fdroid'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'filter' -d 'only apply APPLINE that match the filter'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'disable' -d 'disable applications instead of uninstalling them'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'enable' -d 'enable applications instead of uninstalling them'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'profile' -d 'target profile file'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -s 'c' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'config' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'busybox' -d 'special busybox command in android'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'device-tmpdir' -d 'special tmpdir in android [default: '/data/local/tmp']'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'root-type' -d 'could be 'auto', 'su', 'adb-root' or 'none' (default: 'auto')'
complete -x -n '__fish_seen_subcommand_from cleanup' -c deployidroid -s 's' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -x -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'serial' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'no-color' -d 'remove colorful message'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -s 'v' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'verbose' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -s 'vv' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'verbose2' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -s 'h' -d 'show help message'
complete -n '__fish_seen_subcommand_from cleanup' -c deployidroid -l 'help' -d 'show help message'
complete -f -n '__fish_complete_deployidroid_apps_cond cleanup' -c deployidroid -a "(__fish_complete_deployidroid_fdroid_apps)"
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'complete' -d 'generate command completion code for shell'
complete -n '__fish_seen_subcommand_from complete' -c deployidroid -l 'bash' -d 'show completion code for bash'
complete -n '__fish_seen_subcommand_from complete' -c deployidroid -l 'fish' -d 'show completion code for fish'
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'deploy' -d 'deploy applications from profile'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -l 'options' -d 'override options in APPLINE, such as type=app+data;repo=fdroid'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -l 'filter' -d 'only apply APPLINE that match the filter'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -l 'profile' -d 'target profile file'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -s 'c' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -l 'config' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -l 'busybox' -d 'special busybox command in android'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -l 'device-tmpdir' -d 'special tmpdir in android [default: '/data/local/tmp']'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -l 'root-type' -d 'could be 'auto', 'su', 'adb-root' or 'none' (default: 'auto')'
complete -x -n '__fish_seen_subcommand_from deploy' -c deployidroid -s 's' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -x -n '__fish_seen_subcommand_from deploy' -c deployidroid -l 'serial' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -l 'no-color' -d 'remove colorful message'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -s 'v' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -l 'verbose' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -s 'vv' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -l 'verbose2' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -s 'h' -d 'show help message'
complete -n '__fish_seen_subcommand_from deploy' -c deployidroid -l 'help' -d 'show help message'
complete -f -n '__fish_complete_deployidroid_apps_cond deploy' -c deployidroid -a "(__fish_complete_deployidroid_fdroid_apps)"
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'download' -d 'download applications'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -s 'r' -d 'special repo, could set multiple times'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -l 'repo' -d 'special repo, could set multiple times'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -l 'allow-unstable' -d 'allow unstable application release'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -l 'force-download' -d 'always download application even through file exists'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -s 'c' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -l 'config' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -l 'cache-dir' -d 'special cache directory [default: '~/.cache/deployidroid']'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -l 'no-color' -d 'remove colorful message'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -s 'v' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -l 'verbose' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -s 'vv' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -l 'verbose2' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -s 'h' -d 'show help message'
complete -n '__fish_seen_subcommand_from download' -c deployidroid -l 'help' -d 'show help message'
complete -f -n '__fish_complete_deployidroid_apps_cond download' -c deployidroid -a "(__fish_complete_deployidroid_fdroid_verapps)"
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'install' -d 'download and install applications'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -s 'r' -d 'special repo, could set multiple times'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'repo' -d 'special repo, could set multiple times'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -s 'u' -d 'special user id, could set multiple times'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'user' -d 'special user id, could set multiple times'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'allow-unstable' -d 'allow unstable application release'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'allow-downgrade' -d 'allow downgrade application'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'install-path' -d 'install system application to special directory [default: '/system/app']'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -s 'c' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'config' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'cache-dir' -d 'special cache directory [default: '~/.cache/deployidroid']'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'busybox' -d 'special busybox command in android'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'device-tmpdir' -d 'special tmpdir in android [default: '/data/local/tmp']'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'root-type' -d 'could be 'auto', 'su', 'adb-root' or 'none' (default: 'auto')'
complete -x -n '__fish_seen_subcommand_from install' -c deployidroid -s 's' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -x -n '__fish_seen_subcommand_from install' -c deployidroid -l 'serial' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'no-color' -d 'remove colorful message'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -s 'v' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'verbose' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -s 'vv' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'verbose2' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -s 'h' -d 'show help message'
complete -n '__fish_seen_subcommand_from install' -c deployidroid -l 'help' -d 'show help message'
complete -f -n '__fish_complete_deployidroid_apps_cond install' -c deployidroid -a "(__fish_complete_deployidroid_fdroid_verapps)"
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'list' -d 'list matched applications'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -s 'r' -d 'special repo, could set multiple times'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'repo' -d 'special repo, could set multiple times'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'cat' -d 'show and match for category'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'desc' -d 'show and match for application description'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'ver' -d 'show all application version details'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'sug' -d 'show suggest version'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'only-ver' -d 'show application versions only'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'allow-unstable' -d 'allow unstable application release'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'appid' -d 'use application id instead keyword'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -s 'c' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'config' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'cache-dir' -d 'special cache directory [default: '~/.cache/deployidroid']'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'no-color' -d 'remove colorful message'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -s 'v' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'verbose' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -s 'vv' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'verbose2' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -s 'h' -d 'show help message'
complete -n '__fish_seen_subcommand_from list' -c deployidroid -l 'help' -d 'show help message'
complete -f -n '__fish_complete_deployidroid_apps_cond list' -c deployidroid -a "(__fish_complete_deployidroid_fdroid_apps)"
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'new' -d 'new template profile for different commands'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'profile-type' -d 'append different template code for different types, could set multiple times'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l '3rd-app' -d 'backup 3rd application'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l '3rd-data' -d 'backup 3rd application data'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'sys-app' -d 'backup system application'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'sys-data' -d 'backup system application data'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'freq-app' -d 'backup frequently used application which defined in $backup_freq_apps'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'freq-data' -d 'backup frequently used application data'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'spec-data' -d 'backup special data which defined in $backup_spec_apps'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'name' -d 'profile name [default: 'default' or profile filename]'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -s 'u' -d 'special user id, could set multiple times'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'user' -d 'special user id, could set multiple times'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'appdir' -d 'special application direcotory [default: './app']'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'device-tmpdir' -d 'special tmpdir in android [default: '/data/local/tmp']'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'profile' -d 'target profile file'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -s 'c' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'config' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'busybox' -d 'special busybox command in android'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'device-tmpdir' -d 'special tmpdir in android [default: '/data/local/tmp']'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'root-type' -d 'could be 'auto', 'su', 'adb-root' or 'none' (default: 'auto')'
complete -x -n '__fish_seen_subcommand_from new' -c deployidroid -s 's' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -x -n '__fish_seen_subcommand_from new' -c deployidroid -l 'serial' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'no-color' -d 'remove colorful message'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -s 'v' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'verbose' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -s 'vv' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'verbose2' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -s 'h' -d 'show help message'
complete -n '__fish_seen_subcommand_from new' -c deployidroid -l 'help' -d 'show help message'
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'restore' -d 'restore android application and user data, for multiple users, will create user if need'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'name' -d 'profile name [default: 'default' or profile filename]'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -s 'u' -d 'special user id, could set multiple times'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'user' -d 'special user id, could set multiple times'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'options' -d 'override options in APPLINE, such as type=app+data;repo=fdroid'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'filter' -d 'only apply APPLINE that match the filter'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'wait-sys-app' -d 'wait for new installed system app ready [default: 3]'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'default-install-path' -d 'install app to /data/app or /system/app instead of the defined install_path in appinfo.sh'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'appdir' -d 'special application direcotory [default: './app']'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'device-tmpdir' -d 'special tmpdir in android [default: '/data/local/tmp']'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'archive-side' -d 'create and extract archive in device or host [default: 'auto']'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'profile' -d 'target profile file'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -s 'c' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'config' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'busybox' -d 'special busybox command in android'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'device-tmpdir' -d 'special tmpdir in android [default: '/data/local/tmp']'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'root-type' -d 'could be 'auto', 'su', 'adb-root' or 'none' (default: 'auto')'
complete -x -n '__fish_seen_subcommand_from restore' -c deployidroid -s 's' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -x -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'serial' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'no-color' -d 'remove colorful message'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -s 'v' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'verbose' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -s 'vv' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'verbose2' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -s 'h' -d 'show help message'
complete -n '__fish_seen_subcommand_from restore' -c deployidroid -l 'help' -d 'show help message'
complete -f -n '__fish_complete_deployidroid_apps_cond restore' -c deployidroid -a "(__fish_complete_deployidroid_backupapplines)"
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'uninstall' -d 'uninstall applications'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -s 'u' -d 'special user id, could set multiple times'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -l 'user' -d 'special user id, could set multiple times'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -l 'keep-data' -d 'keep the data and cache directories after removal'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -s 'c' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -l 'config' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -l 'busybox' -d 'special busybox command in android'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -l 'device-tmpdir' -d 'special tmpdir in android [default: '/data/local/tmp']'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -l 'root-type' -d 'could be 'auto', 'su', 'adb-root' or 'none' (default: 'auto')'
complete -x -n '__fish_seen_subcommand_from uninstall' -c deployidroid -s 's' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -x -n '__fish_seen_subcommand_from uninstall' -c deployidroid -l 'serial' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -l 'no-color' -d 'remove colorful message'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -s 'v' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -l 'verbose' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -s 'vv' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -l 'verbose2' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -s 'h' -d 'show help message'
complete -n '__fish_seen_subcommand_from uninstall' -c deployidroid -l 'help' -d 'show help message'
complete -f -n '__fish_complete_deployidroid_apps_cond uninstall' -c deployidroid -a "(__fish_complete_deployidroid_deviceapps)"
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'update' -d 'download fdroid repo index.xml file to cache direcotry'
complete -n '__fish_seen_subcommand_from update' -c deployidroid -s 'r' -d 'special repo, could set multiple times'
complete -n '__fish_seen_subcommand_from update' -c deployidroid -l 'repo' -d 'special repo, could set multiple times'
complete -n '__fish_seen_subcommand_from update' -c deployidroid -s 'c' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from update' -c deployidroid -l 'config' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from update' -c deployidroid -l 'cache-dir' -d 'special cache directory [default: '~/.cache/deployidroid']'
complete -n '__fish_seen_subcommand_from update' -c deployidroid -l 'no-color' -d 'remove colorful message'
complete -n '__fish_seen_subcommand_from update' -c deployidroid -s 'v' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from update' -c deployidroid -l 'verbose' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from update' -c deployidroid -s 'vv' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from update' -c deployidroid -l 'verbose2' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from update' -c deployidroid -s 'h' -d 'show help message'
complete -n '__fish_seen_subcommand_from update' -c deployidroid -l 'help' -d 'show help message'
complete -x -n '__fish_complete_deployidroid_no_subcommand' -c deployidroid -a 'upgrade' -d 'upgrade applications'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -s 'r' -d 'special repo, could set multiple times'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'repo' -d 'special repo, could set multiple times'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -s 'u' -d 'special user id, could set multiple times'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'user' -d 'special user id, could set multiple times'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'allow-unstable' -d 'allow unstable application release'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -s 'l' -d 'only list all upgradeable applications'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'list' -d 'only list all upgradeable applications'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -s 'c' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'config' -d 'special config file [default: '~/.config/deployidroid/config.sh']'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'cache-dir' -d 'special cache directory [default: '~/.cache/deployidroid']'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'busybox' -d 'special busybox command in android'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'device-tmpdir' -d 'special tmpdir in android [default: '/data/local/tmp']'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'root-type' -d 'could be 'auto', 'su', 'adb-root' or 'none' (default: 'auto')'
complete -x -n '__fish_seen_subcommand_from upgrade' -c deployidroid -s 's' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -x -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'serial' -d 'use device with given serial (overrides $ANDROID_SERIAL)' -a "(__fish_complete_deployidroid_adb_devices)"
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'no-color' -d 'remove colorful message'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -s 'v' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'verbose' -d 'show verbose message'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -s 'vv' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'verbose2' -d 'show more verbose message'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -s 'h' -d 'show help message'
complete -n '__fish_seen_subcommand_from upgrade' -c deployidroid -l 'help' -d 'show help message'
complete -f -n '__fish_complete_deployidroid_apps_cond upgrade' -c deployidroid -a "(__fish_complete_deployidroid_deviceapps)"
