function _comp_deployidroid_adb_users {
  adb shell pm list users 2>/dev/null | grep -o '{.*}' | tr -d '{}' | awk -F: '{print $1}'
}

function _comp_deployidroid_backupapplines {
  echo '@bluetooth'
  echo '@datausage'
  echo '@wifi'
  echo '@apatch'
  echo '@kernelsu'
  echo '@user.accounts'
  echo '@user.appwidgets'
  echo '@user.icon'
  echo '@user.wallpaper'
  adb shell pm list packages -s 2>/dev/null | tr -d '\r' | sed 's/package:/user_system:/' 2>/dev/null
  local u=
  for u in $(_comp_deployidroid_adb_users); do
    if [ ${u} -eq 0 ]; then
      adb shell pm list packages -3 --user ${u} 2>/dev/null | tr -d '\r' | sed 's/package://' 2>/dev/null
    else
      adb shell pm list packages -3 --user ${u} 2>/dev/null | tr -d '\r' | sed "s/package:/user_${u}:/" 2>/dev/null
    fi
  done
}
function _comp_deployidroid_deviceapps {
  adb shell pm list packages -3 2>/dev/null | tr -d '\r' | sed 's/package://' 2>/dev/null
}
function _comp_deployidroid_fdroid_apps {
  local app_file="${COMP_WORDS[0]}"
  ${app_file} list --no-color | awk '{print $1}' 2>/dev/null
}
function _comp_deployidroid_fdroid_verapps {
  local app_file="${COMP_WORDS[0]}"
  ${app_file} list --only-ver --appid ${1} 2>/dev/null | awk -F"\t"  '{print $1}' | awk -F= '{print $2}'
}
function _comp_deployidroid {

  local cur prev_index prev
  cur="${COMP_WORDS[${COMP_CWORD}]}"
  cur_index="${COMP_CWORD}"
  prev_index=$((COMP_CWORD - 1))
  prev2_index=$((COMP_CWORD - 2))
  prev="${COMP_WORDS[${prev_index}]}"
  prev2="${COMP_WORDS[${prev2_index}]}"

  local cmd= subcmd=
  local i=1
  while [ "${i}" -lt "${#COMP_WORDS[@]}" ]; do
    local word="${COMP_WORDS[${i}]}"
    if [[ ! "${word}" == -* ]]; then
      if [ -z "${cmd}" ]; then
        cmd="${word}"
      else
        subcmd="${word}"
      fi
      if [ -n "${subcmd}" ]; then
        break
      fi
    fi
    ((i++))
  done

  if [ "${cur_index}" -eq "1" ]; then
    COMPREPLY=( $(compgen -W '-h --help adb backup check cleanup complete deploy diff disable download enable fastboot install list new restore uninstall update upgrade ' -- "${cur}" ) )
    return
  fi

  if [ "${cmd}" = "adb" -a "${cur_index}" -eq "2" ]; then
    COMPREPLY=( $(compgen -W 'assert-arch assert-device assert-mode assert-online assert-root autoshell check-arch check-device check-file-hash check-mode check-mount-mode chown-recursive clear-default-launcher clear-package create-user disable-autostart disable-battery-optimization disable-package disable-run-background download-package dump-image enable-autostart enable-battery-optimization enable-package enable-run-background ensure-mount-rw ensure-user-exists erase-partition find-block find-mount-point fix-package-datadir-permission fix-selinux-permission flash-image flash-zip get-android-abi-list get-android-api-level get-android-version get-current-slot get-current-user get-default-launcher get-file-timestamp get-imei get-kernel-version get-mount-mirror get-mount-rw-path get-package-datadir get-package-datadir-de get-package-display-name get-package-path get-package-uid get-package-version-code get-package-version-name get-root-type get-twrp-version get-user-name grant-background-location-permission grant-location-permission grant-notification-permission grant-permission grant-storage-permission has-mount-mirror has-root install install-sys is-3rd-package is-boot-mode is-cmd-exists is-dir-exists is-dynamic-partition is-file-exists is-mount-point is-mount-ro is-mount-rw is-online is-only-one-user is-package-installed is-pm-support-multiuser is-recovery-mode is-same-file is-support-multiuser is-sys-package is-system-as-root is-twrp-mode is-user-exists is-waydroid-mode kill-package list-3rd-packages list-autostart-components list-devices list-packages list-partition-names list-sys-packages list-users make-pkg-installed-for-user mount-mirror pull push rawpull rawpush rawrootshell rawshell rawsushell reboot remount remount-ro remount-rw revoke-background-location-permission revoke-location-permission revoke-notification-permission revoke-permission revoke-storage-permission rootshell run-script set-default-launcher set-file-timestamp shell start-package stop-package twrp-dump-image twrp-erase-partition twrp-factory-reset twrp-flash-image twrp-flash-sparse-image twrp-flash-zip twrp-format-data twrp-wipe-cache uninstall uninstall-sys unmount-mirror utilshell waydroid-rawshell waydroid-reboot ' -- "${cur}" ) )
    return
  fi

  if [[ "${cmd}" != "adb" && "${cur}" == -* ]]; then
    case "${cmd}" in
      backup) COMPREPLY=( $(compgen -W '--name --password --options --filter --appdir --datadir --device-tmpdir --archive-side --ignore-check-profile --ignore-check-timestamp --profile -c --config --busybox --device-tmpdir --root-type -s --serial --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      check) COMPREPLY=( $(compgen -W '--fix --clean --copy-to --move-to --appdir --datadir --encrypt-data --decrypt-data --password --profile -c --config --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      cleanup) COMPREPLY=( $(compgen -W '--options --filter --options --profile -c --config --busybox --device-tmpdir --root-type -s --serial --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      complete) COMPREPLY=( $(compgen -W '--bash --fish ' -- "${cur}" ) ); return;;
      deploy) COMPREPLY=( $(compgen -W '--options --password --filter --wait-sys-app --device-tmpdir --archive-side --files-ignore-unpack --ignore-check-profile --ignore-check-timestamp --profile -c --config --busybox --device-tmpdir --root-type -s --serial --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      diff) COMPREPLY=( $(compgen -W '--device-tmpdir --profile -c --config --busybox --device-tmpdir --root-type -s --serial --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      disable) COMPREPLY=( $(compgen -W '--options --filter --ignore-check-profile --profile -c --config --busybox --device-tmpdir --root-type -s --serial --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      download) COMPREPLY=( $(compgen -W '-r --repo --allow-unstable --force-download -c --config --cache-dir --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      enable) COMPREPLY=( $(compgen -W '--options --filter --ignore-check-profile --profile -c --config --busybox --device-tmpdir --root-type -s --serial --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      fastboot) COMPREPLY=( $(compgen -W '-subcommand -c --config -s --serial --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      install) COMPREPLY=( $(compgen -W '-r --repo -u --user --allow-unstable --allow-downgrade --install-path -c --config --cache-dir --busybox --device-tmpdir --root-type -s --serial --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      list) COMPREPLY=( $(compgen -W '-r --repo --cat --desc --ver --sug --only-ver --allow-unstable --appid -c --config --cache-dir --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      new) COMPREPLY=( $(compgen -W '--profile-type --3rd-app --3rd-data --sys-app --sys-data --freq-app --freq-data --spec-data --name -u --user --appdir --datadir --device-tmpdir --profile -c --config --busybox --device-tmpdir --root-type -s --serial --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      restore) COMPREPLY=( $(compgen -W '--name --password -u --user --options --filter --wait-sys-app --appdir --datadir --device-tmpdir --archive-side --ignore-check-profile --ignore-check-timestamp --profile -c --config --busybox --device-tmpdir --root-type -s --serial --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      uninstall) COMPREPLY=( $(compgen -W '-u --user --keep-data -c --config --busybox --device-tmpdir --root-type -s --serial --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      update) COMPREPLY=( $(compgen -W '-r --repo -c --config --cache-dir --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
      upgrade) COMPREPLY=( $(compgen -W '-r --repo -u --user --allow-unstable -l --list -c --config --cache-dir --busybox --device-tmpdir --root-type -s --serial --no-color -v --verbose -vv --verbose2 -h --help ' -- "${cur}" ) ); return;;
    esac
  fi

  if [[ ! "${cur}" == @(-*|.*|../*|~/*|/*) ]]; then
    if [ "${cmd}" != "adb" ]; then
      case "${cmd}" in
        backup|cleanup|disable|enable) COMPREPLY=( $(compgen -W "$(_comp_deployidroid_backupapplines)" -- "${cur}" ) ); return;;
        restore) COMPREPLY=( $(compgen -W "$(_comp_deployidroid_backupapplines)" -- "${cur}" ) ); return;;
        deploy|list) COMPREPLY=( $(compgen -W "$(_comp_deployidroid_fdroid_apps)" -- "${cur}" ) ); return;;
        uninstall|upgrade) COMPREPLY=( $(compgen -W "$(_comp_deployidroid_deviceapps)" -- "${cur}" ) ); return;;
        download|install)
          if [ x"${cur}" = x'=' ]; then
            COMPREPLY=( $(compgen -W "$(_comp_deployidroid_fdroid_verapps "${prev}")") )
          elif [ x"${prev}" = x'=' ]; then
            COMPREPLY=( $(compgen -W "$(_comp_deployidroid_fdroid_verapps "${prev2}")" -- "${cur}" ) )
          else
            COMPREPLY=( $(compgen -W "$(_comp_deployidroid_fdroid_apps)" -- "${cur}" ) )
          fi
          return;;
      esac
    else
      case "${subcmd}" in
        clear-package|disable-autostart|disable-battery-optimization|disable-package|disable-run-background|download-package|enable-autostart|enable-battery-optimization|enable-package|enable-run-background|fix-package-datadir-permission|get-package-datadir|get-package-datadir-de|get-package-display-name|get-package-path|get-package-uid|get-package-version-code|get-package-version-name|grant-background-location-permission|grant-location-permission|grant-notification-permission|grant-permission|grant-storage-permission|is-3rd-package|is-package-installed|is-sys-package|kill-package|list-autostart-components|make-pkg-installed-for-user|revoke-background-location-permission|revoke-location-permission|revoke-notification-permission|revoke-permission|revoke-storage-permission|start-package|stop-package|uninstall|uninstall-sys) COMPREPLY=( $(compgen -W "$(_comp_deployidroid_deviceapps)" -- "${cur}" ) ); return;;
      esac
    fi
  fi

  if [[ "${cur}" == @(.*|../*|~/*|/*) ]]; then
    COMPREPLY=( $(compgen -d -f -- "${cur}") )
  fi
} &&
complete -o filenames -o bashdefault -F _comp_deployidroid deployidroid
