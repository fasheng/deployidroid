# Depends: utils.sh

declare -A adb_depends=(
  [tr]=''
  [basename]=''
  [dirname]=''
  [grep]=''
  [sed]=''
  [awk]=''
  [sha1sum]=''
  [adb]='please install android-sdk or android-platform-tools firstly'
)

# Constant variables

ANDROID_VERSION_CODES_VANILLA_ICE_CREAM=35   # 15
ANDROID_VERSION_CODES_UPSIDE_DOWN_CAKE=34    # 14
ANDROID_VERSION_CODES_TIRAMISU=33            # 13
ANDROID_VERSION_CODES_S=31                   # 12
ANDROID_VERSION_CODES_R=30                   # 11
ANDROID_VERSION_CODES_Q=29                   # 10
ANDROID_VERSION_CODES_P=28                   # 9
ANDROID_VERSION_CODES_O=26                   # 8.0
ANDROID_VERSION_CODES_N=24                   # 7.0
ANDROID_VERSION_CODES_M=23                   # 6
ANDROID_VERSION_CODES_LOLLIPOP=21            # 5.0
ANDROID_VERSION_CODES_KITKAT=19              # 4.4
ANDROID_VERSION_CODES_JELLY_BEAN_MR2=18      # 4.3
ANDROID_VERSION_CODES_JELLY_BEAN_MR1=17      # 4.2
ANDROID_VERSION_CODES_ICE_CREAM_SANDWICH=14  # 4.0.1 - 4.0.2
ANDROID_FIRST_APPLICATION_UID=10000

ADB_DEFAULT_DEVICE_TMPDIR='/data/local/tmp'
ADB_DEFAULT_ROOT_TYPE='auto'

# Cache variables

declare -A ADB_CACHE_USER_PACKAGES
ADB_CACHE_ALL_PACKAGES=
ADB_CACHE_ALL_SYS_PACKAGES=
ADB_CACHE_ALL_3RD_PACKAGES=
ADB_CACHE_ANDROID_VERSION=
ADB_CACHE_ANDROID_API_LEVEL=
ADB_CACHE_USERS=
ADB_CACHE_ANDROID_ABI_LIST=
ADB_CACHE_IS_BOOT_MODE=
ADB_CACHE_IS_SYSTEM_AS_ROOT=

# Configurable variables, could override in host script or config file
adb_cmd="adb"
adb_verbose=
adb_device_tmpdir="${ADB_DEFAULT_DEVICE_TMPDIR}"
adb_root_type="${ADB_DEFAULT_ROOT_TYPE}"
adb_allow_downgrade=
adb_keep_data=
adb_busybox=
adb_serial=
adb_mount_mirror_dir="/dev/mount_mirror"
adb_ignore_reboot=; declare -n ignore_reboot=adb_ignore_reboot
waydroid_cmd="waydroid"
waydroid_dir="/var/lib/waydroid"

###*** ADB subcommands

function _cmd_usage_adb {
  echo "adb <adb-subcommand>
    run adb subcommands"
}
function cmd_adb {
  local adb_subcmd="${1}"; shift
  adb_"${adb_subcmd}" "${@}"
}

###**** Helper
function _adb_ensure_tmpdir_exists {
  adb_autoshell "${adb_busybox} mkdir -p '${adb_device_tmpdir}'"
  adb_autoshell "${adb_busybox} chmod 777 '${adb_device_tmpdir}'"
}

function _adb_usage_is-cmd-exists {
  echo "is-cmd-exists <cmd>, check if command exists in device, return 1 if not"
}
function adb_is-cmd-exists {
  local adb_util_funcs=($(get_adbfunc_depend_funcs is_cmd_exists))
  $(adb_utilshell "if is_cmd_exists '${1}'; then echo true; else echo false; fi")
}

###**** Shell
# run adb raw shell
function _adb_usage_rawshell {
  echo "rawshell <commands..>, run adb shell in default permission"
}
function adb_rawshell {
  local cmdline="${*}"
  # if --verbose2 exists, print command that be executed, and redirect
  # to stderr to prevent influence of result
  [ 0"${adb_verbose}" -ge 2 ] && msg2 "${adb_cmd} shell %s" "$(quote "${cmdline}")" 1>&2
  ${adb_cmd} shell "${cmdline}"
}

# run waydroid raw shell
function _adb_usage_waydroid-rawshell {
  echo "waydroid-rawshell <commands..>, run waydroid shell, note that waydroid shell never returns error code"
}
function adb_waydroid-rawshell {
  local cmdline="${*}"
  # if --verbose2 exists, print command that be executed, and redirect
  # to stderr to prevent influence of result
  [ 0"${adb_verbose}" -ge 2 ] && msg2 "${waydroid_cmd} shell -- sh -c %s" "$(quote "${cmdline}")" 1>&2
  if [ "$(id -u)" = "0" ]; then
    ${waydroid_cmd} shell -- sh -c "${cmdline}"
  else
    sudo ${waydroid_cmd} shell -- sh -c "${cmdline}"
  fi
}

# run adb raw su shell
function _adb_usage_rawsushell {
  echo "rawsushell <commands..>, run adb su shell in root permission"
}
function adb_rawsushell {
  local quote_cmdline="$(quote "${*}")"
  # if --verbose2 exists, print command that be executed, and redirect
  # to stderr to prevent influence of result
  [ 0"${adb_verbose}" -ge 2 ] && msg2 "${adb_cmd} shell su -c %s" "$(quote "${quote_cmdline}")" 1>&2
  ${adb_cmd} shell su -c "${quote_cmdline}"
}

# run adb raw root shell
function _adb_usage_rawrootshell {
  echo "rawrootshell <commands..>, run adb shell in root permission"
}
function adb_rawrootshell {
  local cmdline="${*}"
  # FIXME: adb_has-root may cause pipeline input read failed, such as:
  #   echo 123 | adb_rawrootshell 'cat'
  if adb_has-root; then
    if [ "${adb_root_type}" = "su" ]; then
      adb_rawsushell "${cmdline}"
    elif [ "${adb_root_type}" = "adb-root" ]; then
      adb_rawshell "${cmdline}"
    elif [ "${adb_root_type}" = "waydroid" ]; then
      adb_waydroid-rawshell "${cmdline}"
    fi
  else
    warning "Missing root permission: adb rootshell %s" "${cmdline}"
  fi
}

function _adb_strip_output {
  tr -d '\r'
}

# run adb shell and strip the result
function _adb_usage_shell {
  echo "shell <commands..>, run adb shell and strip the result in default permission"
}
function adb_shell {
  local cmdline="${*}"
  # if not --verbose exists, always hide command error result
  if [ -z "${adb_verbose}" ]; then
    if ! [[ "${cmdline}" =~ ">/dev/null"$ ]]; then
      cmdline="${cmdline} 2>/dev/null"
    fi
  fi
  adb_rawshell "${cmdline}" | _adb_strip_output
}

# run adb root shell and strip the result
function _adb_usage_rootshell {
  echo "rootshell <commands..>, run adb shell and strip the result in root permission"
}
function adb_rootshell {
  local cmdline="${*}"
  # if not --verbose exists, always hide command error result
  if [ -z "${adb_verbose}" ]; then
    if ! [[ "${cmdline}" =~ ">/dev/null"$ ]]; then
      cmdline="${cmdline} 2>/dev/null"
    fi
  fi
  _fix_root_type # fix next line adb_rawrootshell with pipeline do not change adb_root_type variable
  adb_rawrootshell "${cmdline}" | _adb_strip_output
}

# opts: adb_root_type
function _adb_usage_autoshell {
  echo "autoshell <commands..>, run adb shell in root permission if possible, or fallback to default shell"
}
function adb_autoshell {
  local cmdline="${*}"
  if adb_has-root; then
    adb_rootshell "${cmdline}"
  else
    adb_shell "${cmdline}"
  fi
}

# arg1: user-id
function _adb_build_user_arg {
  local user="${1}"
  if [ -z "${user}" ] || ! adb_is-pm-support-multiuser; then
    echo ''
  else
    echo "--user '${user}'"
  fi
}

# arg1: command-format(use %s to replace with user arg), arg2: [user-id], default user-id: all users
function _adb_shell_with_user_arg {
  local cmdfmt="${1}"
  local user="${2}"
  if [ -z "${user}" ]; then
    for u in $(_adb_list-users-plain); do
      adb_shell "$(printf "${cmdfmt}" "$(_adb_build_user_arg ${u})")"
    done
  else
    adb_shell "$(printf "${cmdfmt}" "$(_adb_build_user_arg ${user})")"
  fi
}

# arg1: command-format(use %s to replace with user arg), arg2: [user-id], default user-id: all users
function _adb_rootshell_with_user_arg {
  local cmdfmt="${1}"
  local user="${2}"
  if [ -z "${user}" ]; then
    for u in $(_adb_list-users-plain); do
      adb_rootshell "$(printf "${cmdfmt}" "$(_adb_build_user_arg ${u})")"
    done
  else
    adb_rootshell "$(printf "${cmdfmt}" "$(_adb_build_user_arg ${user})")"
  fi
}

declare -a adb_util_vars=(adb_busybox ADB_CACHE_IS_BOOT_MODE ADB_CACHE_IS_SYSTEM_AS_ROOT)
function _adb_get_util_vars_str {
  # fill util variables for adb shell
  local adb_util_vars_str=
  for v in ${adb_util_vars[@]}; do
    local -n ref=${v}
    adb_util_vars_str="${adb_util_vars_str}${v}='${ref}'
"
  done
  echo -n "${adb_util_vars_str}"
}

declare -a adb_util_funcs=(msg msg2 warning error abort)
function _adb_get_util_funcs_str {
  # fill util functions for adb shell
  local adb_util_funcs_str=
  for f in ${adb_util_funcs[@]}; do
    adb_util_funcs_str="${adb_util_funcs_str}$(declare -f ${f})
"
  done
  echo -n "${adb_util_funcs_str}"
}

function _adb_usage_utilshell {
  echo "utilshell <commands..>, run adb autoshell with some builtin util functions like msg and warning
    could custom variables and functions to utilshell with array var 'adb_util_vars' and 'adb_util_funcs'"
}
function adb_utilshell {
  local cmdline="${*}"
  adb_autoshell "$(_adb_get_util_vars_str)
$(_adb_get_util_funcs_str)
${cmdline}"
}

function _get_adbfunc_depend_funcs {
  local func="${1}"
  echo "${func}"
  local -n def_depend_funcs="${func}_depend_funcs"
  local depfunc=
  for depfunc in "${def_depend_funcs[@]}"; do
    echo "${depfunc}"
    _get_adbfunc_depend_funcs "${depfunc}"
  done
}
function get_adbfunc_depend_funcs {
  _get_adbfunc_depend_funcs "${1}" | sort | uniq
}

function _adb_usage_run-script {
  echo "run-script [script-file-or-stdin] [script-args..], push and run script file in device
    default push dir: '${adb_device_tmpdir}', change it with argument '--device-tmpdir'"
}
function adb_run-script {
  local script_file= device_file=
  if has_pipe_input; then
    [ -n "${adb_verbose}" ] && msg2 "adb_run-script <stdin> $*" 1>&2
    script_file=$(mktemp --tmpdir run-script-stdin.XXXXXXXXXX)
    cat > "${script_file}"
    device_file="${adb_device_tmpdir}/$(basename "${script_file}")"
  else
    [ -n "${adb_verbose}" ] && msg2 "adb_run-script $*" 1>&2
    script_file="${1}"; shift
    device_file="${adb_device_tmpdir}/$(basename "${script_file}")"
  fi

  adb_push "${script_file}" "${device_file}"
  if [ 0"${adb_verbose}" -ge 2 ]; then
    if is_text_file "${script_file}"; then
      msg2 "script file content(${device_file}):"
      adb_autoshell "${adb_busybox} cat '${device_file}'"
    else
      msg2 "script file content(${device_file}): ignore for binary file"
    fi
  fi

  adb_autoshell "${adb_busybox} chmod +x '${device_file}'"
  if is_text_file "${script_file}"; then
    adb_utilshell "source '${device_file}' $@"
  else
    adb_utilshell "'${device_file}' $@"
  fi
  adb_autoshell "${adb_busybox} rm -f '${device_file}'"
}

###**** Check online
function _adb_usage_assert-online {
  echo "assert-online, abort if no device online"
}
function adb_assert-online {
  if ! adb_is-online; then
    local devices=$(adb_list-devices)
    local device_num=$(echo "${devices}" | wc -l)
    if [ "${device_num}" -gt 1 ]; then
      abort "There are multiple adb devices($(join_lines "${devices}")), please select one with argument '--serial' or environment variable \$ANDROID_SERIAL"
    else
      abort "Looks adb device(${adb_serial:-default}) is offline"
    fi
  fi
}

function _adb_usage_list-devices {
  echo "list-devices, list all devices"
}
function adb_list-devices {
  ${adb_cmd} devices | grep -E '(device|recovery|offline)$' | awk '{print $1}'
}

function _adb_usage_is-online {
  echo "is-online, check if there is device online, return 1 if not"
}
function adb_is-online {
  if ${adb_cmd} get-state &>/dev/null; then
    return 0
  else
    return 1
  fi
}

###**** Check root
function _adb_usage_assert-root {
  echo "assert-root, abort if there is no root permission"
}
function adb_assert-root {
  _fix_root_type
  if [ -z "${adb_root_type}" ] || [[ ! $(adb_rootshell "${adb_busybox} id") =~ "root" ]]; then
    abort "Looks there is no root permission in adb shell, please ensure 'adb root' works or 'su' command exists"
  fi
}

function _adb_usage_has-root {
  echo "has-root, check if there is root permission, return 1 if not"
}
function adb_has-root {
  _fix_root_type
  if [ -z "${adb_root_type}" ]; then
    return 1
  fi
  return 0
}

function _adb_usage_get-root-type {
  echo "get-root-type, get current root type"
}
function adb_get-root-type {
  _fix_root_type
  echo "${adb_root_type:-none}"
}

function _fix_root_type {
  # NOTE: All commands and functions will be executed in this function
  # will redirect stdin to /dev/null to avoid following command like
  # adb_autoshell could not read data from stdin.
  if [ "${adb_root_type}" = "auto" ]; then
    if [ "$(adb_rawshell "id -u" </dev/null)" = "0" ]; then
      adb_root_type='adb-root'
    elif [ "$(adb_rawshell "su -c 'id -u'" </dev/null 2>/dev/null)" = "0" ]; then
      adb_root_type='su'
    elif adb_is-waydroid-mode </dev/null; then
      adb_root_type='waydroid'
    else
      adb_root_type=
    fi
  elif [ "${adb_root_type}" = "none" ]; then
    adb_root_type=
  fi
  if [ -z "${_ignore_show_root_type_msg}" ]; then
    declare -g _ignore_show_root_type_msg=1
    [ -n "${adb_verbose}" ] && msg2 "root type: ${adb_root_type:-none}" 1>&2
    if [ "${adb_root_type}" = "waydroid" ]; then
      msg2 "need root permission for waydroid shell later" 1>&2
    fi
  fi
}

###**** System mode
function _adb_usage_check-mode {
  echo "check-mode <mode..>, check if current mode is one of target modes, mode could be boot, recovery, waydroid, fastboot"
}
function adb_check-mode {
  local mode
  for mode; do
    case "${mode}" in
      boot) adb_is-boot-mode &>/dev/null && return 0;;
      recovery) adb_is-recovery-mode &>/dev/null && return 0;;
      waydroid) adb_is-waydroid-mode &>/dev/null && return 0;;
      fastboot) fastboot_is-online &>/dev/null && return 0;;
    esac
  done
  return 1
}

function _adb_usage_assert-mode {
  echo "assert-mode <mode..>, abort if check mode failed"
}
function adb_assert-mode {
  adb_check-mode "$@" || abort "Looks current mode not under $@"
}

function _adb_usage_check-arch {
  echo "check-arch <arch..>, check if current arch is one of target architectures, arch could be arm64-v8a, armeabi-v7a, armeabi, x86, x86_64"
}
function adb_check-arch {
  local arch
  for arch in $(adb_get-android-abi-list); do
    if contains "${arch}" "$@"; then
      return 0
    fi
  done
  return 1
}

function _adb_usage_assert-arch {
  echo "assert-arch <arch..>, abort if check arch failed"
}
function adb_assert-arch {
  adb_check-arch "$@" || abort "Looks current arch not under $@"
}

function _adb_usage_check-device {
  echo "check-device <device..>, check if current device is one of target devices, device is the codename"
}
function adb_check-device {
  local device=$(adb_shell 'getprop ro.product.device 2>/dev/null')
  local model=$(adb_shell 'getprop ro.product.model 2>/dev/null')
  local product=$(adb_shell 'getprop ro.build.product 2>/dev/null')
  local vendor_device=$(adb_shell 'getprop ro.product.vendor.device 2>/dev/null')
  local vendor_model=$(adb_shell 'getprop ro.product.vendor.model 2>/dev/null')
  local vendor_product=$(adb_shell 'getprop ro.vendor.product.device 2>/dev/null')
  local d
  for d in "${device}" "${model}" "${product}" "${vendor_device}" "${vendor_model}" "${vendor_product}"; do
    if contains "${d}" "$@"; then
      return 0
    fi
  done
  return 1
}

function _adb_usage_assert-device {
  echo "assert-device <device..>, abort if check device failed"
}
function adb_assert-device {
  adb_check-device "$@" || abort "Looks current device codename not under $@"
}

function _adb_usage_is-boot-mode {
  echo "is-boot-mode, check if in normal boot mode"
}
function adb_is-boot-mode {
  if [ -n "${ADB_CACHE_IS_BOOT_MODE}" ]; then
    ${ADB_CACHE_IS_BOOT_MODE}
    return
  fi
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_is_boot_mode))
  ADB_CACHE_IS_BOOT_MODE=$(adb_utilshell "if adbfunc_is_boot_mode; then echo true; else echo false; fi")
  ${ADB_CACHE_IS_BOOT_MODE:-false}
}

function _adb_usage_is-recovery-mode {
  echo "is-recovery-mode, check if in recovery mode"
}
function adb_is-recovery-mode {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_is_recovery_mode))
  $(adb_utilshell "if adbfunc_is_recovery_mode; then echo true; else echo false; fi")
}

function _adb_usage_is-twrp-mode {
  echo "is-twrp-mode, check if in twrp recovery mode"
}
function adb_is-twrp-mode {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_is_twrp_mode))
  $(adb_utilshell "if adbfunc_is_twrp_mode; then echo true; else echo false; fi")
}

function _adb_usage_is-waydroid-mode {
  echo "is-waydroid-mode, check if in waydroid mode"
}
function adb_is-waydroid-mode {
  # Do not use adb_utilshell here or _fix_root_type will loop endlessly
  $(adb_shell 'if [ "$(getprop ro.board.platform)" = "waydroid" ]; then echo true; else echo false; fi')
}

function _adb_usage_reboot {
  echo "reboot, reboot android"
}
function adb_reboot {
  [ -n "${adb_verbose}" ] && msg2 "adb_reboot"
  [ -n "${adb_ignore_reboot}" ] && { msg2 "ignore reboot"; return; }
  if adb_is-waydroid-mode; then
    adb_waydroid-reboot
  else
    ${adb_cmd} reboot
  fi
}

###**** System information
function _adb_usage_get-current-slot {
  echo "get-current-slot"
}
function adb_get-current-slot {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_get_current_slot))
  adb_utilshell "adbfunc_get_current_slot"
}

function _adb_usage_get-kernel-version {
  echo "get-kernel-version"
}
function adb_get-kernel-version {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_get_kernel_version))
  adb_utilshell "adbfunc_get_kernel_version"
}

function _adb_usage_get-android-version {
  echo "get-android-version"
}
function adb_get-android-version {
  if [ -z "${ADB_CACHE_ANDROID_VERSION}" ]; then
    local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_get_android_version))
    ADB_CACHE_ANDROID_VERSION=$(adb_utilshell "adbfunc_get_android_version")
  fi
  echo "${ADB_CACHE_ANDROID_VERSION}"
}

function _adb_usage_get-android-api-level {
  echo "get-android-api-level"
}
function adb_get-android-api-level {
  if [ -z "${ADB_CACHE_ANDROID_API_LEVEL}" ]; then
    local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_get_android_api_level))
    ADB_CACHE_ANDROID_API_LEVEL=$(adb_utilshell "adbfunc_get_android_api_level")
    if [ -z "${ADB_CACHE_ANDROID_API_LEVEL}" ]; then
      # looks adb device is offline
      ADB_CACHE_ANDROID_API_LEVEL=0
    fi
  fi
  echo "${ADB_CACHE_ANDROID_API_LEVEL}"
}

function _adb_usage_get-android-abi-list {
  echo "get-android-abi-list"
}
function adb_get-android-abi-list {
  if [ -z "${ADB_CACHE_ANDROID_ABI_LIST}" ]; then
    local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_get_android_abi_list))
    ADB_CACHE_ANDROID_ABI_LIST=$(adb_utilshell "adbfunc_get_android_abi_list")
  fi
  echo "${ADB_CACHE_ANDROID_ABI_LIST}"
}

function _adb_usage_get-imei {
  echo "get-imei"
}
function adb_get-imei {
  # TODO: not working for all rom, test ok with lineageos 19.1
  adb_autoshell "imei=\$(input keyevent KEYCODE_CALL;sleep 1;input text '*#06#'; uiautomator dump --compressed /dev/stdout|sed s/\>\<\/\\n/g|grep -Eo 'text=\"[^\"]+[0-9]\"'|sed 's/^text=\"\(.*\)\"$/\1/'); echo \${imei}"
}

###**** Cache result
# fill CACHE_xxx variables to speed up
function _adb_fetch_cache {
  if [ -z "${ADB_CACHE_ANDROID_VERSION}" ]; then
    msg "Cache adb output"
  fi
  _fix_root_type
  adb_is-boot-mode || true
  adb_is-system-as-root || true
  adb_get-android-version 1>/dev/null
  adb_get-android-api-level 1>/dev/null
  adb_get-android-abi-list 1>/dev/null
  _adb_fetch_users
  _adb_fetch_pkgs_cache
}
function _adb_fetch_users {
  ADB_CACHE_USERS=
  adb_list-users 1>/dev/null
}
function _adb_fetch_pkgs_cache {
  ADB_CACHE_USER_PACKAGES=()
  ADB_CACHE_ALL_PACKAGES=
  ADB_CACHE_ALL_SYS_PACKAGES=
  ADB_CACHE_ALL_3RD_PACKAGES=
  local user=
  for user in $(_adb_list-users-plain); do
    adb_list-3rd-packages "${user}" 1>/dev/null
  done
  adb_list-sys-packages 1>/dev/null
}

###**** User
function _adb_usage_is-support-multiuser {
  echo "is-support-multiuser [apilevel]"
}
function adb_is-support-multiuser {
  local apilevel="${1:-$(adb_get-android-api-level)}"
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_ICE_CREAM_SANDWICH}" ]; then
    return 0
  else
    return 1
  fi
}

function _adb_usage_is-pm-support-multiuser {
  echo "is-pm-support-multiuser [apilevel]"
}
function adb_is-pm-support-multiuser {
  local apilevel="${1:-$(adb_get-android-api-level)}"
  # Android support multiple user since 4.0, but the am/pm command support "--user" option after 4.2
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_JELLY_BEAN_MR1}" ]; then
    return 0
  else
    return 1
  fi
}

function _adb_usage_list-users {
  echo "list-users"
}
function adb_list-users {
  if adb_is-support-multiuser; then
    if [ -z "${ADB_CACHE_USERS}" ]; then
      ADB_CACHE_USERS=$(adb_shell "pm list users 2>/dev/null" || true)
    fi
    if [ -z "${ADB_CACHE_USERS}" ]; then
      # some device need root permission to list users, just fallback here
      ADB_CACHE_USERS='UserInfo{0:Primary:3}'
    fi
    echo "${ADB_CACHE_USERS}" | grep -o '{.*}' | tr -d '{}' | awk -F: '{print $1,$2}'
  else
    echo '0 <none>'
  fi
}
function _adb_list-users-plain {
  adb_list-users | awk '{print $1}'
}

function _adb_usage_is-only-one-user {
  echo "is-only-one-user"
}
function adb_is-only-one-user {
  local user_num=$(adb_list-users | wc | awk '{print $1}')
  if [ "${user_num}" -le 1 ]; then
    return 0
  else
    return 1
  fi
}

function _adb_usage_is-user-exists {
  echo "is-user-exists <user-id>"
}
function adb_is-user-exists {
  local user=
  for user in $(_adb_list-users-plain); do
    if [ "${1}" = "${user}" ]; then
      return 0
    fi
  done
  return 1
}

function _adb_usage_create-user {
  echo "create-user <user-id> <user-name>"
}
function adb_create-user {
  adb_rootshell "pm create-user --profileOf '${1}' '${2}'" || true
}

function _adb_usage_ensure-user-exists {
  echo "ensure-user-exists <user-id> <user-name>, loop until create user success"
}
function adb_ensure-user-exists {
  local user="${1}"
  local user_name="${2:-test}"
  if ! adb_is-user-exists "${user}"; then
    adb_create-user "${user}" "${user_name}"
  fi
  # update cached users
  _adb_fetch_users
  if ! adb_is-user-exists "${user}"; then
    warning "Create user ${user} failed"
    read -n1 -p "Please create user manually and press any key to continue..."
    adb_ensure-user-exists "${user}" "${user_name}"
  fi
}

function _adb_usage_get-current-user {
  echo "get-current-user"
}
function adb_get-current-user {
  local user=
  if adb_is-pm-support-multiuser; then
    user=$(adb_shell "dumpsys activity" | ${adb_busybox} grep "mUserLru" | ${adb_busybox} grep '\[.*\]' | ${adb_busybox} grep -o '[0-9]\+' | tail -n 1)
  fi
  if [ -z "${user}" ]; then
    user='0'
  fi
  echo "${user}"
}

function _adb_usage_get-user-name {
  echo "get-user-name [user-id], default user-id: current user"
}
function adb_get-user-name {
  local user="${1:-$(adb_get-current-user)}"
  if adb_is-support-multiuser; then
    adb_list-users | grep "^${user} " | awk '{print $2}'
  else
    echo '<none>'
  fi
}

###**** Package
function _adb_usage_list-sys-packages {
  echo "list-sys-packages"
}
function adb_list-sys-packages {
  if [ -z "${ADB_CACHE_ALL_SYS_PACKAGES}" ]; then
    ADB_CACHE_ALL_SYS_PACKAGES=$(adb_shell "pm list packages -f -s" | sort | uniq || true)
    ADB_CACHE_ALL_PACKAGES=$(echo -e "${ADB_CACHE_ALL_PACKAGES}\n${ADB_CACHE_ALL_SYS_PACKAGES}" | sort | uniq)
  fi
  local pkg=
  for pkg in $(echo "${ADB_CACHE_ALL_SYS_PACKAGES}" | awk -F: '{print $2}'); do
    echo "${pkg##*=}"
  done
}

function _adb_usage_list-3rd-packages {
  echo "list-3rd-packages [user-id], default user-id: current user"
}
function adb_list-3rd-packages {
  local user="${1:-$(adb_get-current-user)}"
  if [ -z "${ADB_CACHE_USER_PACKAGES[${user}_3rd]}" ]; then
    ADB_CACHE_USER_PACKAGES+=([${user}_3rd]=$(adb_shell "pm list packages -f -3 $(_adb_build_user_arg ${user})" || true))
    ADB_CACHE_ALL_3RD_PACKAGES=$(echo -e "${ADB_CACHE_ALL_3RD_PACKAGES}\n${ADB_CACHE_USER_PACKAGES[${user}_3rd]}" | sort | uniq)
    ADB_CACHE_ALL_PACKAGES=$(echo -e "${ADB_CACHE_ALL_PACKAGES}\n${ADB_CACHE_USER_PACKAGES[${user}_3rd]}" | sort | uniq)
  fi
  # for android 8.0+, the format is: package:/data/app/org.fdroid.fdroid-nzG6JC-iGBiyPjMYNpFhfw==/base.apk=org.fdroid.fdroid
  # for older version, the format is: package:/data/app/org.fdroid.fdroid-1/base.apk=org.fdroid.fdroid
  local pkg=
  for pkg in $(echo "${ADB_CACHE_USER_PACKAGES[${user}_3rd]}" | awk -F: '{print $2}'); do
    echo "${pkg##*=}"
  done
}

function _adb_usage_list-packages {
  echo "list-packages [user-id], default user-id: current user"
}
function adb_list-packages {
  adb_list-3rd-packages "${1}"
  adb_list-sys-packages
}

function _adb_usage_is-sys-package {
  echo "is-sys-package <PKG>"
}
function adb_is-sys-package {
  local pkg="${1}"
  if [ -z "${ADB_CACHE_ALL_SYS_PACKAGES}" ]; then
    _adb_fetch_pkgs_cache
  fi
  adb_list-sys-packages | grep "^${pkg}$" &>/dev/null
}

function _adb_usage_is-3rd-package {
  echo "is-3rd-package <PKG> [user-id], default user-id: all users"
}
function adb_is-3rd-package {
  local pkg="${1}"
  local user="${2}"
  if [ -z "${ADB_CACHE_ALL_3RD_PACKAGES}" ]; then
    _adb_fetch_pkgs_cache
  fi
  if [ -z "${user}" ]; then
    if echo "${ADB_CACHE_ALL_3RD_PACKAGES}" | grep "=${pkg}$" &>/dev/null; then
      return 0
    else
      return 1
    fi
  else
    adb_list-3rd-packages "${user}" | grep "^${pkg}$" &>/dev/null
  fi
}

function _adb_usage_is-package-installed {
  echo "is-package-installed <PKG> [user-id], default user-id: all users"
}
function adb_is-package-installed {
  local pkg="${1}"
  local user="${2}"
  if [ -z "${ADB_CACHE_ALL_PACKAGES}" ]; then
    _adb_fetch_pkgs_cache
  fi
  if [ -z "${user}" ]; then
    if echo "${ADB_CACHE_ALL_PACKAGES}" | grep "=${pkg}$" &>/dev/null; then
      return 0
    else
      return 1
    fi
  else
    adb_is-sys-package "${pkg}" || adb_is-3rd-package "${pkg}" "${user}"
  fi
}

function _adb_usage_get-package-version-code {
  echo "get-package-version-code <PKG>"
}
function adb_get-package-version-code {
  local pkg="${1}"
  adb_shell "dumpsys package '${pkg}'" | ${adb_busybox} grep versionCode | awk '{print $1}' | awk -F= '{print $2}' | head -n 1
}

function _adb_usage_get-package-version-name {
  echo "get-package-version-name <PKG>"
}
function adb_get-package-version-name {
  local pkg="${1}"
  adb_shell "dumpsys package '${pkg}'" | ${adb_busybox} grep versionName | awk -F= '{print $2}' | head -n 1
}

function _adb_usage_get-package-path {
  echo "get-package-path <PKG>"
}
function adb_get-package-path {
  local pkg="${1}"
  local path=
  if [ -n "${ADB_CACHE_ALL_PACKAGES}" ]; then
    path=$(echo "${ADB_CACHE_ALL_PACKAGES}" | grep "=${pkg}$" | awk -F: '{print $2}')
    path="${path%=*}"
  fi
  if [ -z "${path}" ]; then
    local user
    for user in $(_adb_list-users-plain); do
      path=$(adb_shell "pm path --user "${user}" '${pkg}'" | awk -F: '{print $2}')
      if [ -n "${path}" ]; then
        break
      fi
    done
  fi
  echo "${path}"
}

function _adb_usage_download-package {
  echo "download-package <PKG> [DESTDIR]"
}
function adb_download-package {
  local pkg="${1}"
  local destdir="${2:-.}"
  local device_appfile=$(adb_get-package-path "${pkg}")
  local version_code=$(adb_get-package-version-code "${pkg}")
  local version_name=$(adb_get-package-version-name "${pkg}")
  local app_filename=$(get_app_filename "${pkg}" "${version_code}" "${version_name}")
  local host_appfile="${destdir}/${app_filename}"
  adb_pull "${device_appfile}" "${host_appfile}" 1
}

function _adb_usage_get-package-display-name {
  echo "get-package-display-name <PKG>"
}
function adb_get-package-display-name {
  if ! adb_is-cmd-exists "aapt"; then
    return
  fi
  local pkg="${1}"
  local path=$(adb_get-package-path "${pkg}")
  local display_name=
  display_name=$(adb_shell "aapt dump badging '${path}'" | ${adb_busybox} grep "application-label:" | ${adb_busybox} sed -e "s/application-label:'//" -e "s/'\$//")
  echo "${display_name}"
}

function _adb_usage_get-package-datadir {
  echo "get-package-datadir <PKG> [user-id] [apilevel], default user-id: current user"
}
function adb_get-package-datadir {
  local pkg="${1}"
  local user="${2:-$(adb_get-current-user)}"
  local apilevel="${3:-$(adb_get-android-api-level)}"
  if adb_is-support-multiuser "${apilevel}"; then
    echo "/data/user/${user}/${pkg}"
  else
    echo "/data/data/${pkg}"
  fi
}

function _adb_usage_get-package-datadir-de {
  echo "get-package-datadir-de <PKG> [user-id] [apilevel], default user-id: current user"
}
function adb_get-package-datadir-de {
  local pkg="${1}"
  local user="${2:-$(adb_get-current-user)}"
  local apilevel="${3:-$(adb_get-android-api-level)}"
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_N}" ]; then
    echo "/data/user_de/${user}/${pkg}"
  fi
}

function _adb_usage_fix-package-datadir-permission {
  echo "fix-package-datadir-permission <PKG> [user-id], default user-id: current user"
}
function adb_fix-package-datadir-permission {
  local pkg="${1}"
  local user="${2:-$(adb_get-current-user)}"
  local pkguid=$(adb_get-package-uid "${pkg}" "${user}")
  local device_datadir=
  for device_datadir in $(adb_get-package-datadir "${pkg}" "${user}") $(adb_get-package-datadir-de "${pkg}" "${user}"); do
    adb_rootshell "${adb_busybox} mkdir -p '${device_datadir}'"
    adb_rootshell "${adb_busybox} chmod 0700 '${device_datadir}'"
    adb_rootshell "${adb_busybox} find '${device_datadir}' -type d -exec chown '${pkguid}:${pkguid}' '{}' \; -exec chmod 0771 '{}' \;"
    adb_rootshell "${adb_busybox} find '${device_datadir}' ! -type d -exec chown '${pkguid}:${pkguid}' '{}' \; -exec chmod 0660 '{}' \;"
    adb_fix-selinux-permission "${device_datadir}"
  done
}

function _adb_usage_get-package-uid {
  echo "get-package-uid <PKG> <user-id>"
}
function adb_get-package-uid {
  local pkg="${1}"
  local user="${2}"
  local uid=$(adb_shell "dumpsys package '${pkg}'" | ${adb_busybox} grep -o 'userId=\w\+' | head -n 1 | awk -F= '{print $2}')
  if [ "${user}" -eq 0 ]; then
    echo "${uid}"
  else
    echo "${user}${uid}"
  fi
}

function _adb_usage_start-package {
  echo "start-package <PKG>"
}
function adb_start-package {
  local pkg="${1}"
  adb_shell "monkey -p '${pkg}' -c android.intent.category.LAUNCHER 1"
}

function _adb_usage_kill-package {
  echo "kill-package <PKG> [user-id], only kill background package, default user-id: all users"
}
function adb_kill-package {
  local pkg="${1}"
  local user="${2}"
  adb_shell "am kill $(_adb_build_user_arg ${user}) '${pkg}'"
}

function _adb_usage_stop-package {
  echo "stop-package <PKG> [user-id], default user-id: all users"
}
function adb_stop-package {
  local pkg="${1}"
  local user="${2}"
  adb_shell "am force-stop $(_adb_build_user_arg ${user}) '${pkg}'"
}

function _adb_usage_clear-package {
  echo "clear-package <PKG> [user-id], default user-id: all users"
}
function adb_clear-package {
  local pkg="${1}"
  local user="${2}"
  _adb_shell_with_user_arg "pm clear %s '${pkg}'" "${user}"
}

###***** Install package
# opts: adb_allow_downgrade
function _adb_usage_install {
  echo "install <host-appfile> [user-id], default user-id: all users"
}
function adb_install {
  local host_appfile=$(get_realpath "${1}")
  local user="${2}"
  local extra_arg=
  local result=
  if [ -n "${adb_allow_downgrade}" ]; then
    extra_arg='-d'
  fi
  if [ -z "${user}" ] || ! adb_is-pm-support-multiuser; then
    local cmd="${adb_cmd} install -r ${extra_arg} '${host_appfile}'"
    [ -n "${adb_verbose}" ] && msg2 "adb_install ${host_appfile}"
    [ -n "${adb_verbose}" ] && msg2 "eval: ${cmd}"
    result=$(eval "${cmd}")
  else
    [ -n "${adb_verbose}" ] && msg2 "adb_install ${host_appfile} for user ${user}"
    # install for special user
    _adb_ensure_tmpdir_exists
    local tmpdir="${adb_device_tmpdir}/$(basename "${host_appfile}")"
    adb_shell "${adb_busybox} mkdir -p '${adb_device_tmpdir}'"
    adb_shell "${adb_busybox} rm -rf '${tmpdir}'"
    adb_rawpush "${host_appfile}" "${adb_device_tmpdir}"
    result=$(adb_shell "pm install -r ${extra_arg} --user ${user} '${tmpdir}'")
    adb_shell "${adb_busybox} rm -rf '${tmpdir}'"
  fi
  if echo "${result}"| grep -q 'Success'; then
    return 0
  else
    local err=$(echo "${result}" | grep 'Failure')
    msg2 "adb_install failed: ${err}"
    return 1
  fi
}

# opts: adb_keep_data
function _adb_usage_uninstall {
  echo "uninstall <PKG> [user-id], default user-id: all users"
}
function adb_uninstall {
  local pkg="${1}"
  local user="${2}"
  local extra_arg=
  if [ -n "${adb_keep_data}" ]; then
    extra_arg='-k'
  fi
  if [ -z "${user}" ] || ! adb_is-pm-support-multiuser; then
    [ -n "${adb_verbose}" ] && msg2 "adb_uninstall ${pkg}"
    adb_shell "pm uninstall ${extra_arg} '${pkg}'"
  else
    [ -n "${adb_verbose}" ] && msg2 "adb_uninstall ${pkg} for user ${user}"
    adb_shell "pm uninstall ${extra_arg} --user '${user}' '${pkg}'"
  fi
}

function _adb_usage_install-sys {
  echo "install-sys <host-appfile> [device-install-dir]"
}
function adb_install-sys {
  # copy from oandbackup ShellCommands.restoreSystemApk()
  local host_appfile=$(get_realpath "${1}")
  local device_appdir="${2:-/system/app}"
  local filename=$(basename "${host_appfile}")
  local appname="${filename%.apk}"
  appname="${appname%-*}"

  adb_ensure-mount-rw "${device_appdir}" 1 || return 1
  device_appdir=$(adb_get-mount-rw-path "${device_appdir}")

  # locations of apks have been changed in android 5
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_LOLLIPOP}" ]; then
    device_appdir="${device_appdir}/${appname}"
  fi

  adb_rootshell "${adb_busybox} mkdir -p '${device_appdir}'"
  adb_rootshell "${adb_busybox} chmod -R 755 '${device_appdir}'"

  adb_push "${host_appfile}" "${device_appdir}/${filename}" 1
  adb_rootshell "${adb_busybox} chmod 644 '${device_appdir}/${filename}'"

  msg2 "maybe a reboot is necessary for system app"
}

# opts: adb_keep_data
function _adb_usage_uninstall-sys {
  echo "uninstall-sys <PKG>"
}
function adb_uninstall-sys {
  local pkg="${1}"
  local device_appfile=$(adb_get-package-path "${pkg}")
  if [ -z "${device_appfile}" ]; then
    warning "Could not detect package path: ${pkg}"
    return
  fi

  if [ -z "${adb_keep_data}" ]; then
    adb_clear-package "${pkg}"
  fi

  adb_ensure-mount-rw "${device_appfile}" 1 || return 1
  device_appfile=$(adb_get-mount-rw-path "${device_appfile}")

  adb_rootshell "${adb_busybox} rm -f '${device_appfile}'"

  # locations of apks has been changed since android 5
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_LOLLIPOP}" ]; then
    device_appfile=$(dirname "${device_appfile}")
  fi
  local device_app_parent=$(dirname "${device_appfile}")
  if [ "${device_app_parent}" = "/system/app" -o "${device_app_parent}" = "/vendor/app" \
      -o x"${device_app_parent}" = x"/system/priv-app" -o x"${device_app_parent}" = x"/vendor/priv-app" ]; then
    adb_rootshell "${adb_busybox} rm -rf '${device_appfile}'"
  fi

  msg2 "maybe a reboot is necessary for system app"
}

function _adb_usage_make-pkg-installed-for-user {
  echo "make-pkg-installed-for-user <PKG> <user-id>"
}
function adb_make-pkg-installed-for-user {
  adb_shell "pm install -r -d --user '${2}' '$(adb_get-package-path "${1}")'"
}

function _adb_usage_disable-package {
  echo "disable-package <PKG-OR-COMPONENT> [user-id], default user-id: all users"
}
function adb_disable-package {
  local pkg_or_comp="${1}"
  local user="${2}"
  [ -n "${adb_verbose}" ] && msg2 "adb_disable-package $(_adb_build_user_arg ${user}) ${pkg_or_comp}"
  if adb_has-root; then
    _adb_rootshell_with_user_arg "pm disable %s '${pkg_or_comp}'" "${user}"
  else
    local apilevel=$(adb_get-android-api-level)
    if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_LOLLIPOP}" ]; then
      _adb_shell_with_user_arg "pm disable-user %s '${pkg_or_comp}'" "${user}"
    elif [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_KITKAT}" ]; then
      _adb_shell_with_user_arg "pm block %s '${pkg_or_comp}'" "${user}"
    else
      warning "Could not disable package without root permission for api level ${apilevel}"
    fi
  fi
}

function _adb_usage_enable-package {
  echo "enable-package <PKG-OR-COMPONENT> [user-id], default user-id: all users"
}
function adb_enable-package {
  local pkg_or_comp="${1}"
  local user="${2}"
  [ -n "${adb_verbose}" ] && msg2 "adb_enable-package $(_adb_build_user_arg ${user}) ${pkg_or_comp}"
  if adb_has-root; then
    _adb_rootshell_with_user_arg "pm enable %s '${pkg_or_comp}'" "${user}"
  else
    local apilevel=$(adb_get-android-api-level)
    if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_LOLLIPOP}" ]; then
      _adb_shell_with_user_arg "pm enable %s '${pkg_or_comp}'" "${user}"
    elif [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_KITKAT}" ]; then
      _adb_shell_with_user_arg "pm unblock %s '${pkg_or_comp}'" "${user}"
    else
      warning "Could not enable package without root permission for api level ${apilevel}"
    fi
  fi
}

###***** Setup package
function _adb_usage_get-default-launcher {
  echo "get-default-launcher [user-id], default user-id: all users"
}
function adb_get-default-launcher {
  _adb_shell_with_user_arg "cmd shortcut get-default-launcher %s" "${1}"
}

function _adb_usage_clear-default-launcher {
  echo "clear-default-launcher [user-id], default user-id: all users"
}
function adb_clear-default-launcher {
  _adb_shell_with_user_arg "cmd shortcut clear-default-launcher %s" "${1}"
}

function _adb_usage_set-default-launcher {
  echo "set-default-launcher <activity> [user-id], default user-id: all users"
}
function adb_set-default-launcher {
  _adb_shell_with_user_arg "pm set-home-activity %s ${1}" "${2}"
}

function _adb_usage_list-autostart-components {
  echo "list-autostart-components [PKG]"
}
function adb_list-autostart-components {
  local pkg="${1}"
  if [ -z "${pkg}" ]; then
    adb_shell "dumpsys package resolvers receiver" | awk '/android.intent.action.BOOT_COMPLETED/{p=1;next}/:/{p=0}p {print $2}'
  else
    adb_shell "dumpsys package resolvers receiver" | awk '/android.intent.action.BOOT_COMPLETED/{p=1;next}/:/{p=0}p {print $2}' | ${adb_busybox} grep "^${pkg}/"
  fi
}

function _adb_usage_disable-autostart {
  echo "disable-autostart <PKG> [user-id], default user-id: all users"
}
function adb_disable-autostart {
  local pkg="${1}"
  local user="${2}"
  local comp=
  for comp in $(adb_list-autostart-components "${pkg}"); do
    adb_disable-package "${comp}" "${user}"
  done
}

function _adb_usage_enable-autostart {
  echo "enable-autostart <PKG> [user-id], default user-id: all users"
}
function adb_enable-autostart {
  local pkg="${1}"
  local user="${2}"
  local comp=
  for comp in $(adb_list-autostart-components "${pkg}"); do
    adb_enable-package "${comp}" "${user}"
  done
}

function _adb_usage_disable-run-background {
  echo "disable-run-background <PKG> [user-id], default user-id: all users"
}
function adb_disable-run-background {
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -lt "${ANDROID_VERSION_CODES_N}" ]; then
    warning "adb_diable-run-background only works since Android 7.0 (API level 24)"
    return
  fi
  local pkg="${1}"
  local user="${2}"
  _adb_shell_with_user_arg "cmd appops set %s '${pkg}' RUN_IN_BACKGROUND ignore" "${user}"
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_P}" ]; then
    _adb_shell_with_user_arg "cmd appops set %s '${pkg}' RUN_ANY_IN_BACKGROUND ignore" "${user}"
  fi
}

function _adb_usage_enable-run-background {
  echo "enable-run-background <PKG> [user-id], default user-id: all users"
}
function adb_enable-run-background {
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -lt "${ANDROID_VERSION_CODES_N}" ]; then
    warning "adb_enable-run-background only works since Android 7.0 (API level 24)"
    return
  fi
  local pkg="${1}"
  local user="${2}"
  _adb_shell_with_user_arg "cmd appops set %s '${pkg}' RUN_IN_BACKGROUND allow" "${user}"
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_P}" ]; then
    _adb_shell_with_user_arg "cmd appops set %s '${pkg}' RUN_ANY_IN_BACKGROUND allow" "${user}"
  fi
}

function _adb_usage_disable-battery-optimization {
  echo "disable-battery-optimization <PKG>"
}
function adb_disable-battery-optimization {
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -lt "${ANDROID_VERSION_CODES_M}" ]; then
    warning "adb_disable-battery-optimization only works since Android 6.0 (API level 23)"
    return
  fi
  adb_shell "dumpsys deviceidle whitelist +${1}"
}

function _adb_usage_enable-battery-optimization {
  echo "enable-battery-optimization <PKG>"
}
function adb_enable-battery-optimization {
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -lt "${ANDROID_VERSION_CODES_M}" ]; then
    warning "adb_enable-battery-optimization only works since Android 6.0 (API level 23)"
    return
  fi
  adb_shell "dumpsys deviceidle whitelist -${1}"
}

###***** Package permission
function _adb_usage_grant-permission {
  echo "grant-permission <PKG> <permission> [user-id], default user-id: all users"
}
function adb_grant-permission {
  local pkg="${1}"
  local perm="${2}"
  local user="${3}"

  # add permission prefix
  if ! [[ "${perm}" =~ \. ]]; then
    perm="android.permission.${perm}"
  fi
  _adb_shell_with_user_arg "pm grant %s '${pkg}' ${perm}" "${user}" || true
}

function _adb_usage_revoke-permission {
  echo "revoke-permission <PKG> <permission> [user-id], default user-id: all users"
}
function adb_revoke-permission {
  local pkg="${1}"
  local perm="${2}"
  local user="${3}"

  # add permission prefix
  if ! [[ "${perm}" =~ \. ]]; then
    perm="android.permission.${perm}"
  fi
  _adb_shell_with_user_arg "pm revoke %s '${pkg}' ${perm}" "${user}" || true
}

function _adb_usage_grant-storage-permission {
  echo "grant-storage-permission <PKG> [user-id], default user-id: all users"
}
function adb_grant-storage-permission {
  adb_grant-permission "${1}" "android.permission.READ_EXTERNAL_STORAGE" "${2}"
  adb_grant-permission "${1}" "android.permission.WRITE_EXTERNAL_STORAGE" "${2}"
}

function _adb_usage_revoke-storage-permission {
  echo "revoke-storage-permission <PKG> [user-id], default user-id: all users"
}
function adb_revoke-storage-permission {
  adb_revoke-permission "${1}" "android.permission.READ_EXTERNAL_STORAGE" "${2}"
  adb_revoke-permission "${1}" "android.permission.WRITE_EXTERNAL_STORAGE" "${2}"
}

function _adb_usage_grant-location-permission {
  echo "grant-location-permission <PKG> [user-id], default user-id: all users"
}
function adb_grant-location-permission {
  adb_grant-permission "${1}" "android.permission.ACCESS_FINE_LOCATION" "${2}"
  adb_grant-permission "${1}" "android.permission.ACCESS_COARSE_LOCATION" "${2}"
}

function _adb_usage_revoke-location-permission {
  echo "revoke-location-permission <PKG> [user-id], default user-id: all users"
}
function adb_revoke-location-permission {
  adb_revoke-permission "${1}" "android.permission.ACCESS_FINE_LOCATION" "${2}"
  adb_revoke-permission "${1}" "android.permission.ACCESS_COARSE_LOCATION" "${2}"
}

function _adb_usage_grant-background-location-permission {
  echo "grant-background-location-permission <PKG> [user-id], default user-id: all users"
}
function adb_grant-background-location-permission {
  adb_grant-permission "${1}" "android.permission.ACCESS_BACKGROUND_LOCATION" "${2}"
}

function _adb_usage_revoke-background-location-permission {
  echo "revoke-background-location-permission <PKG> [user-id], default user-id: all users"
}
function adb_revoke-background-location-permission {
  adb_revoke-permission "${1}" "android.permission.ACCESS_BACKGROUND_LOCATION" "${2}"
}

function _adb_usage_grant-notification-permission {
  echo "grant-notification-permission <PKG> [user-id], default user-id: all users"
}
function adb_grant-notification-permission {
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_TIRAMISU}" ]; then
    adb_grant-permission "${1}" "android.permission.POST_NOTIFICATIONS" "${2}"
  else
    warning "adb_grant-notification-permission only works since Android 13 (API level 33)"
  fi
}

function _adb_usage_revoke-notification-permission {
  echo "revoke-notification-permission <PKG> [user-id], default user-id: all users"
}
function adb_revoke-notification-permission {
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_TIRAMISU}" ]; then
    adb_revoke-permission "${1}" "android.permission.POST_NOTIFICATIONS" "${2}"
  else
    warning "adb_grant-notification-permission only works since Android 13 (API level 33)"
  fi
}

###**** File
function _adb_usage_chown-recursive {
  echo "chown-recursive <ownership> <path>"
}
function adb_chown-recursive {
  # user 'find' instead of 'chown -R' because chown not support '-R' argument in android
  adb_rootshell "${adb_busybox} find '${2}' -exec chown '${1}' '{}' \;"
}

function _adb_usage_fix-selinux-permission {
  echo "fix-selinux-permission <path>"
}
function adb_fix-selinux-permission {
  if [ $(adb_get-android-api-level) -ge "${ANDROID_VERSION_CODES_M}" ]; then
    adb_rootshell "restorecon -RF '${1}'"
  fi
}

function _adb_usage_is-same-file {
  echo "is-same-file <device-path> <host-path>, check if device file and host file are the same"
}
function adb_is-same-file {
  device_hash=$(adb_shell "${adb_busybox} sha1sum '${1}'" 2>/dev/null | awk '{print $1}')
  host_hash=$(sha1sum "${2}" | awk '{print $1}')
  if [ "${device_hash}" = "${host_hash}" ]; then
    return 0
  else
    return 1
  fi
}

function _adb_usage_check-file-hash {
  echo "check-file-hash <device-path> <hash>, hash format: sha256:xxxx, sha1:xxxx, md5:xxxx"
}
function adb_check-file-hash {
  local device_path="${1}"
  local hash="${2}"
  local hash_type="$(echo ${hash} | awk -F: '{print $1}')"
  local hash_value="$(echo ${hash} | awk -F: '{print $2}')"

  if ! adb_is-file-exists "${1}"; then
    msg2 "file not exists: ${1}"
    return 1
  fi

  if [ -z "${hash}" -o "${hash_type}" = "ignore" -o "${hash_value}" = "ignore" ]; then
    # ignore check hash
    return 0
  fi

  local real_hash=
  if [ "${hash_type}" = "sha256" ]; then
    real_hash=$(adb_shell "${adb_busybox} sha256sum '${device_path}'" | awk '{print $1}')
  elif [ "${hash_type}" = "sha1" ]; then
    real_hash=$(adb_shell "${adb_busybox} sha1sum '${device_path}'" | awk '{print $1}')
  elif [ "${hash_type}" = "md5" ]; then
    real_hash=$(adb_shell "${adb_busybox} md5sum '${device_path}'" | awk '{print $1}')
  else
    warning "Unknown hash type: ${2}"
    return 1
  fi

  if [ "${real_hash}" = "${hash_value}" ]; then
    return 0
  else
    warning "File hash incorrect: ${1} ${hash_type} is ${real_hash}, prefer is ${hash_value}"
    return 1
  fi
}

function _adb_usage_is-file-exists {
  echo "is-file-exists <device-path>"
}
function adb_is-file-exists {
  $(adb_autoshell "if [ -e '${1}' ]; then echo true; else  echo false; fi")
}

function _adb_usage_is-dir-exists {
  echo "is-dir-exists <device-path>"
}
function adb_is-dir-exists {
  $(adb_autoshell "if [ -d '${1}' ]; then echo true; else  echo false; fi")
}

function _adb_usage_get-file-timestamp {
  echo "get-file-timestamp <device-path>"
}
function adb_get-file-timestamp {
  local device_path="${1}"
  local device_timestamp
  if adb_is-dir-exists "${1}"; then
    device_timestamp=$(adb_autoshell "${adb_busybox} find '${device_path}' -print0 -maxdepth 2 | ${adb_busybox} xargs -0 ${adb_busybox} stat -c '%Y' | ${adb_busybox} sort -n | ${adb_busybox} tail -n 1")
  elif adb_is-file-exists "${1}"; then
    device_timestamp=$(adb_autoshell "${adb_busybox} stat -c '%Y' '${device_path}'")
  fi
  if [ -z "${device_timestamp}" ]; then
    device_timestamp=0
  fi
  echo "${device_timestamp}"
}

function _adb_usage_set-file-timestamp {
  echo "set-file-timestamp <device-path> <seconds>"
}
function adb_set-file-timestamp {
  if adb_is-file-exists "${1}" && [ "${adb_busybox}" != "toybox" ]; then
    # android default toybox touch command not support modify file time, please special '--busybox' arg
    local datafmt=$(date --utc "+%FT%T.%NZ" -d @${2})
    adb_autoshell "${adb_busybox} touch -c -m -d '${datafmt}' '${1}'"
  fi
}

###**** Push and pull
function _adb_usage_rawpush {
  echo "rawpush [args..]"
}
function adb_rawpush {
  [ 0"${adb_verbose}" -ge 2 ] && msg2 "${adb_cmd} push $*"
  ${adb_cmd} push "$@"
}

function _adb_usage_rawpull {
  echo "rawpull [args..]"
}
function adb_rawpull {
  [ 0"${adb_verbose}" -ge 2 ] && msg2 "${adb_cmd} pull $*"
  ${adb_cmd} pull "$@"
}

function _adb_usage_pull {
  echo "pull <device-path> <host-path> [use-tmpdir], pull file from device
    use tmpdir allow copy device file to tmpdir at first step to fix adb pull missing root permission issue"
}
function adb_pull {
  [ -n "${adb_verbose}" ] && msg2 "adb_pull $*"
  local device_path="${1}"
  local host_path="${2:-.}"
  local use_tmpdir="${3}"
  if [ -n "${use_tmpdir}" ] && is_child_dir "${adb_device_tmpdir}" "${device_path}"; then
    use_tmpdir=
    msg2 "adb_pull: ignore use tmpdir for target ${device_path} is the same or child dir of ${adb_device_tmpdir}"
  fi
  if ! adb_is-file-exists "${device_path}"; then
    msg2 "adb_pull: ${device_path} not found, ignore"
    return 1
  fi

  local device_is_dir= host_is_dir= host_dir_exists=
  if [[ "${device_path}" =~ /$ ]]; then
    device_is_dir=1
  fi
  if [[ "${host_path}" =~ /$ ]]; then
    host_is_dir=1
  fi
  if [ -d "${host_path}" ]; then
    host_dir_exists=1
  fi

  # ensure host parent dir exists
  host_path=$(get_realpath "${host_path}")
  if [ -n "$(dirname "${host_path}")" ]; then
    mkdir -p "$(dirname "${host_path}")"
  fi

  # if pull device file to host dir, and host dir not exists, ensure
  # it exists at first, such as "adb_pull device/file host/dir/"
  if [ -z "${device_is_dir}" -a -n "${host_is_dir}" -a -z "${host_dir_exists}" ]; then
    mkdir -p "${host_path}"
  fi

  _fix_root_type
  if [ -z "${use_tmpdir}" ] || [ "${adb_root_type}" = "adb-root" ] || [ -z "${adb_root_type}" ]; then
    adb_rawpull "${device_path}" "${host_path}"
  else
    _adb_ensure_tmpdir_exists
    local tmp_path="${adb_device_tmpdir}/$(basename "${device_path}")"
    local fix_device_path="${device_path%/}" # remove tailing slash or 'toybox cp -R' not works correctly
    adb_autoshell "${adb_busybox} rm -rf '${tmp_path}'"
    adb_autoshell "${adb_busybox} cp -R -L '${fix_device_path}' '${adb_device_tmpdir}'"
    adb_chown-recursive "shell:shell" "${tmp_path}"
    adb_rawpull "${tmp_path}" "${host_path}"
    adb_autoshell "${adb_busybox} rm -rf '${tmp_path}'"
  fi

  # if pull device dir to host dir, and host dir already exists, just
  # align the host dir , such as "adb_pull device/dir/ host/dir/"
  if [ -n "${host_dir_exists}" -a -n "${device_is_dir}" -a -n "${host_is_dir}" ]; then
    local device_dirname=$(basename "${device_path}")
    mv "${host_path}/${device_dirname}"/* "${host_path}"
    rmdir "${host_path}/${device_dirname}"
  fi
}

function _adb_usage_push {
  echo "push <host-path> <device-path> [use-tmpdir], push file to device
    use tmpdir allow push device file to tmpdir at first step to fix adb push missing root permission issue"
}
function adb_push {
  [ -n "${adb_verbose}" ] && msg2 "adb_push $*"
  local host_path="${1}"
  local device_path="${2}"
  local use_tmpdir="${3}"
  if [ -n "${use_tmpdir}" ] && is_child_dir "${adb_device_tmpdir}" "${device_path}"; then
    use_tmpdir=
    msg2 "adb_push: ignore use tmpdir for target ${device_path} is the same or child dir of ${adb_device_tmpdir}"
  fi

  local device_is_dir= host_is_dir= device_dir_exists=
  if [[ "${device_path}" =~ /$ ]]; then
    device_is_dir=1
  fi
  if [[ "${host_path}" =~ /$ ]]; then
    host_is_dir=1
  fi
  if adb_is-dir-exists "${device_path}"; then
    device_dir_exists=1
  fi

  host_path=$(get_realpath "${host_path}")
  if ! [ -e "${host_path}" ]; then
    msg2 "adb_push: ${host_path} not found, ignore"
    return 1
  fi

  _fix_root_type

  # ensure device parent dir exists
  if [ -n $(dirname "${device_path}") ]; then
    adb_autoshell "${adb_busybox} mkdir -p '$(dirname "${device_path}")'"
  fi

  # if push host file to device dir, and device dir not exists, ensure
  # it exists at first, such as "adb_push host/file device/dir/"
  if [ -z "${host_is_dir}" -a -n "${device_is_dir}" -a -z "${device_dir_exists}" ]; then
    adb_autoshell "${adb_busybox} mkdir -p '${device_path}'"
  fi

  if [ -z "${use_tmpdir}" ] || [ "${adb_root_type}" = "adb-root" ] || [ -z "${adb_root_type}" ]; then
    adb_rawpush "${host_path}" "${device_path}"
  else
    _adb_ensure_tmpdir_exists
    local tmp_path="${adb_device_tmpdir}/$(basename "${host_path}")"
    local fix_device_path="${device_path%/}" # remove tailing slash or 'toybox cp -R' not works correctly
    adb_autoshell "${adb_busybox} rm -rf '${tmp_path}'"
    adb_rawpush "${host_path}" "${adb_device_tmpdir}"
    adb_autoshell "${adb_busybox} cp -R -L '${tmp_path}' '${fix_device_path}'"
    adb_autoshell "${adb_busybox} rm -rf '${tmp_path}'"
  fi

  # if push host dir to device dir, and device dir already exists, just
  # align the device dir , such as "adb_push host/dir/ device/dir/"
  if [ -n "${device_dir_exists}" -a -n "${device_is_dir}" -a -n "${host_is_dir}" ]; then
    local host_dirname=$(basename "${host_path}")
    # workaround if target dir contains non-empty sub-dir, use cp instead of mv
    adb_autoshell "${adb_busybox} mv '${device_path}${host_dirname}'/* '${device_path}' 2>/dev/null || \
(${adb_busybox} cp -R -L '${device_path}${host_dirname}'/* '${device_path}' && ${adb_busybox} rm -rf '${device_path}${host_dirname}'/*)"
    adb_autoshell "${adb_busybox} rmdir '${device_path}${host_dirname}' || true"
  fi
}

###**** Mount
function _adb_usage_find-block {
  echo "find-block <partition-name-or-path>, find block path for target case-insensitive partition name"
}
function adb_find-block {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_find_block))
  local result=$(adb_utilshell "adbfunc_find_block '${1}'")
  if [ -n "${result}" ]; then
    echo "${result}"
    return 0
  else
    return 1
  fi
}

function _adb_usage_list-partition-names {
  echo "list-partition-names"
}
function adb_list-partition-names {
  local partition_parent_path=$(adb_autoshell "${adb_busybox} find /dev/block -type d -name by-name" | head -1)
  if adb_is-file-exists "${partition_parent_path}"; then
    adb_autoshell "ls -1 '${partition_parent_path}'"
  else
    warning "Partition device not found in /dev/block/by-name or /dev/block/bootdevice/by-name" 1>&2
    return 1
  fi
}

function _adb_usage_is-system-as-root {
  echo "is-system-as-root, check if partition layout is system-as-root"
}
function adb_is-system-as-root {
  if [ -n "${ADB_CACHE_IS_SYSTEM_AS_ROOT}" ]; then
    ${ADB_CACHE_IS_SYSTEM_AS_ROOT}
    return
  fi
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_is_system_as_root))
  ADB_CACHE_IS_SYSTEM_AS_ROOT=$(adb_utilshell "if adbfunc_is_system_as_root; then echo true; else echo false; fi")
  ${ADB_CACHE_IS_SYSTEM_AS_ROOT}
}

function _adb_usage_is-dynamic-partition {
  echo "is-dynamic-partition"
}
function adb_is-dynamic-partition {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_is_dynamic_partition))
  $(adb_utilshell "if adbfunc_is_dynamic_partition; then echo true; else echo false; fi")
}

function _adb_usage_is-mount-point {
  echo "is-mount-point <path>"
}
function adb_is-mount-point {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_is_mount_point))
  $(adb_utilshell "if adbfunc_is_mount_point '${1}'; then echo true; else echo false; fi")
}

function _adb_usage_find-mount-point {
  echo "find-mount-point <path>, find the mount point for target path"
}
function adb_find-mount-point {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_find_mount_point))
  adb_utilshell "adbfunc_find_mount_point '${1}'"
}

function _adb_usage_remount {
  echo "remount-path <path> <mode> [find-mount-point], remount the path with target mode like rw, ro
    find-mount-point is none empty means will find the parent if the path is not a mount point"
}
function adb_remount {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_remount))
  $(adb_utilshell "if adbfunc_remount '${1}' '${2}' '${3}'; then echo true; else echo false; fi")
}

function _adb_usage_remount-rw {
  echo "remount-rw <path> [find-mount-point], remount the path as read-write
    find-mount-point is none empty means will find the parent if the path is not a mount point"
}
function adb_remount-rw {
  adb_remount "${1}" "rw" "${2}"
}

function _adb_usage_remount-ro {
  echo "remount-ro <path> [find-mount-point], remount the path as read-only
    find-mount-point is none empty means will find the parent if the path is not a mount point"
}
function adb_remount-ro {
  adb_remount "${1}" "ro" "${2}"
}

function _adb_usage_check-mount-mode {
  echo "check-mount-mode <path> <mode> [find-mount-point], check the mount mode of path, mode could be rw, ro
    find-mount-point is none empty means will find the parent if the path is not a mount point"
}
function adb_check-mount-mode {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_check_mount_mode))
  $(adb_utilshell "if adbfunc_check_mount_mode '${1}' '${2}' '${3}'; then echo true; else echo false; fi")
}

function _adb_usage_is-mount-rw {
  echo "is-mount-rw <path> [find-mount-point], check if the path was mounted as read-write
    find-mount-point is none empty means will find the parent if the path is not a mount point"
}
function adb_is-mount-rw {
  adb_check-mount-mode "${1}" "rw" "${2}"
}

function _adb_usage_is-mount-ro {
  echo "is-mount-ro <path> [find-mount-point], check if the path was mounted as read-only
    find-mount-point is none empty means will find the parent if the path is not a mount point"
}
function adb_is-mount-ro {
  adb_check-mount-mode "${1}" "ro" "${2}"
}

function _adb_usage_get-mount-mirror {
  echo "get-mount-mirror <path> [mirror-dir], get mount mirror dir for the path, default mirror dir: ${adb_mount_mirror_dir}"
}
function adb_get-mount-mirror {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_get_mount_mirror))
  adb_utilshell "adbfunc_get_mount_mirror '${1}' '${2:-${adb_mount_mirror_dir}}'"
}

function _adb_usage_has-mount-mirror {
  echo "has-mount-mirror <path> [mirror-dir], check if the read-write mirror was mounted of the path
    default mirror dir: ${adb_mount_mirror_dir}"
}
function adb_has-mount-mirror {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_has_mount_mirror))
  $(adb_utilshell "if adbfunc_has_mount_mirror '${1}' '${2:-${adb_mount_mirror_dir}}'; then echo true; else echo false; fi")
}

function _adb_usage_mount-mirror {
  echo "mount-mirror <path> [mirror-dir], mount a read-write mirror for the path
    default mirror dir: ${adb_mount_mirror_dir}"
}
function adb_mount-mirror {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_mount_mirror))
  $(adb_utilshell "if adbfunc_mount_mirror '${1}' '${2:-${adb_mount_mirror_dir}}'; then echo true; else echo false; fi")
}

function _adb_usage_unmount-mirror {
  echo "unmount-mirror <path> [mirror-dir], unmount the read-write mirror of the path
    default mirror dir: ${adb_mount_mirror_dir}"
}
function adb_unmount-mirror {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_unmount_mirror))
  $(adb_utilshell "if adbfunc_unmount_mirror '${1}' '${2:-${adb_mount_mirror_dir}}'; then echo true; else echo false; fi")
}

function _adb_usage_ensure-mount-rw {
  echo "ensure-mount-rw <path> [find-mount-point] [mirror-dir], ensure target mount point was mounted as read-write
    default mirror dir: ${adb_mount_mirror_dir}"
}
function adb_ensure-mount-rw {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_ensure_mount_rw))
  $(adb_utilshell "if adbfunc_ensure_mount_rw '${1}' '${2}' '${3:-${adb_mount_mirror_dir}}'; then echo true; else echo false; fi")
}

function _adb_usage_get-mount-rw-path {
  echo "get-mount-rw-path <path> [mirror-dir], get read-write dir for target path
    mainly used to handle the situation when mirror was mounted
    default mirror dir: ${adb_mount_mirror_dir}"
}
function adb_get-mount-rw-path {
  local adb_util_funcs=($(get_adbfunc_depend_funcs adbfunc_get_mount_rw_path))
  adb_utilshell "adbfunc_get_mount_rw_path '${1}' '${2:-${adb_mount_mirror_dir}}'"
}

###**** TWRP
function _adb_usage_get-twrp-version {
  echo "get-twrp-version"
}
function adb_get-twrp-version {
  adb_shell getprop ro.twrp.version
}

function _adb_usage_twrp-wipe-cache {
  echo "twrp-wipe-cache, wipe dalvik-cache and cache partition"
}
function adb_twrp-wipe-cache {
  adb_shell "twrp mount /data; sleep 1"
  adb_shell "${adb_busybox} rm -rf /data/dalvik-cache"
  adb_shell "twrp wipe cache; sleep 1"
}

function _adb_usage_twrp-factory-reset {
  echo "twrp-factory-reset"
}
function adb_twrp-factory-reset {
  adb_shell "twrp wipe data; sleep 1"
}

function _adb_usage_twrp-format-data {
  echo "twrp-format-data"
}
function adb_twrp-format-data {
  adb_shell "twrp format data; sleep 1"
}

function _adb_usage_twrp-flash-zip {
  echo "twrp-flash-zip <zip-file> [push-dir], push and flash zip file to device
    only work in twrp recovery mode, default push-dir: /sdcard"
}
function adb_twrp-flash-zip {
  [ -n "${adb_verbose}" ] && msg2 "adb_twrp-flash-zip $*"
  if ! adb_is-twrp-mode; then
    abort "Only works in twrp recovery mode"
  fi

  local zip_file="${1}"
  local push_dir="${2:-/sdcard}"
  local device_zip_file="${push_dir}/$(basename "${zip_file}")"

  # push file if not exists
  if adb_is-same-file "${device_zip_file}" "${zip_file}"; then
    msg2 "ignore push file to device: ${device_zip_file} already exists"
  else
    adb_push "${zip_file}" "${device_zip_file}"
  fi

  adb_rootshell "twrp install '${device_zip_file}'; sleep 1"
  adb_rootshell "${adb_busybox} rm -f '${device_zip_file}'"
}

function _adb_usage_twrp-flash-image {
  echo "twrp-flash-image <partition-name-or-path> <image-file[.7z|.gz|.bz2|.xz|.zip]> [push-dir] [is-sparse-image]
    push and flash image file to parition with twrp flash_image command, default push-dir: /sdcard"
}
function adb_twrp-flash-image {
  if ! adb_is-twrp-mode; then
    abort "Only works in twrp recovery mode"
  fi

  local partition="${1}"
  local image_file="${2}"
  local push_dir="${3:-/sdcard}"
  local is_sparse_image="${4}"

  local block=
  if ! block=$(adb_find-block "${partition}"); then
    return 1
  fi

  local device_image_file="${push_dir}/$(basename "${image_file}")"
  if is_archive_file "${image_file}"; then
    # unpack and push with pipeline
    device_image_file="${push_dir}/$(get_prefer_unpacked_file "$(basename "${image_file}")")"
    adb_rootshell "${adb_busybox} mkdir -p '${push_dir}'"
    pipe_unpack_file "${image_file}" | adb_rawshell "cat > '${device_image_file}'"
  else
    # push directly
    if adb_is-same-file "${device_image_file}" "${image_file}"; then
      msg "Ignore push file to device: ${device_image_file} already exists"
    else
      adb_push "${image_file}" "${device_image_file}"
    fi
  fi

  # flash image
  if [ -n "${is_sparse_image}" ]; then
    adb_rootshell "simg2img '${device_image_file}' '${block}'"
  else
	# sometimes flash image doesn't like to flash due to the first 2KB
	# matching, so we erase first to ensure that it flashes
    adb_rootshell "erase_image '${block}'"
    adb_rootshell "flash_image '${block}' '${device_image_file}'"
  fi

  # fix partition size
  adb_rootshell "(e2fsck -fy '${block}' && resize2fs '${block}') > /dev/null" || true

  # remove tmp image file
  adb_rootshell "${adb_busybox} rm -f '${device_image_file}'"
}

function _adb_usage_twrp-flash-sparse-image {
  echo "twrp-flash-sparse-image <partition-name-or-path> <image-file[.7z|.gz|.bz2|.xz|.zip]> [push-dir]
    push and flash sparse image file to partition with twrp simg2img command, default push-dir: /sdcard"
}
function adb_twrp-flash-sparse-image {
  adb_twrp-flash-image "${1}" "${2}" "${3}" "1"
}

function _adb_usage_twrp-dump-image {
  echo "twrp-dump-image <partition-name-or-path> [pull-file], dump partition and pull image file with twrp dump_image command
    default pull-file: ./<partition-name>.img[.gz]"
}
function adb_twrp-dump-image {
  if ! adb_is-twrp-mode; then
    abort "Only works in twrp recovery mode"
  fi

  local partition="${1}"
  local partition_name="$(basename "${partition}")"
  local pull_file="${2:-./${partition_name}.img}"
  local stdout='/proc/self/fd/1'

  local block=
  if ! block=$(adb_find-block "${partition}"); then
    return 1
  fi

  # dump image to stdout, then pull and pack it to archive image with pipeline
  if is_archive_file "${pull_file}"; then
    adb_rawrootshell "dump_image '${block}' ${stdout} | cat" | pipe_pack_file "${pull_file}"
  else
    adb_rawrootshell "dump_image '${block}' ${stdout} | cat" > "${pull_file}"
  fi
}

function _adb_usage_twrp-erase-partition {
  echo "twrp-erase-partition <partition-name-or-path>, erase partition with twrp erase_image command"
}
function adb_twrp-erase-partition {
  if ! adb_is-twrp-mode; then
    abort "Only works in twrp recovery mode"
  fi

  local partition="${1}"
  local partition_name="$(basename "${partition}")"

  local block=
  if ! block=$(adb_find-block "${partition}"); then
    return 1
  fi

  adb_rootshell "erase_image '${block}'"
}

###**** Waydroid
function _adb_usage_waydroid-reboot {
  echo "waydroid-reboot, reboot waydroid ui"
}
function adb_waydroid-reboot {
  [ -n "${adb_verbose}" ] && msg2 "adb_waydroid-reboot: need sudo permission"
  sudo lxc-attach -P "${waydroid_dir}"/lxc/ -n waydroid -- service call waydroidhardware 4 || true
}

###**** Flash image
function _adb_usage_flash-image {
  echo "flash-image <partition-name-or-path> <image-file[.7z|.bz2|.gz|.xz]>, push and flash image file to partition with dd command
    suggest work in recovery mode"
}
function adb_flash-image {
  if ! adb_is-recovery-mode; then
    warning "Flash image may broken if not in recovery mode"
  fi

  local partition="${1}"
  local image_file="${2}"

  local block=
  if ! block=$(adb_find-block "${partition}"); then
    return 1
  fi

  if is_pipe_archive_file "${image_file}"; then
    pipe_unpack_file "${image_file}" | adb_rawrootshell "${adb_busybox} dd bs=4k of='${block}'; sync"
  else
    cat "${image_file}" | adb_rawrootshell "${adb_busybox} dd bs=4k of='${block}'; sync"
  fi

  # fix partition size
  adb_rootshell "(e2fsck -fy '${block}' && resize2fs '${block}') > /dev/null" || true
}

function _adb_usage_dump-image {
  echo "dump-image <partition-name-or-path> [pull-file], dump partition and pull image file with dd command
    default pull-file: ./<partition-name>.img[.7z|.bz2|.gz|.xz]"
}
function adb_dump-image {
  local partition="${1}"
  local partition_name="$(basename "${partition}")"
  local pull_file="${2:-./${partition_name}.img}"

  local block=
  if ! block=$(adb_find-block "${partition}"); then
    return 1
  fi

  if is_pipe_archive_file "${pull_file}"; then
    adb_rawrootshell "${adb_busybox} dd bs=4k if='${block}'" | pipe_pack_file "${pull_file}"
  else
    adb_rawrootshell "${adb_busybox} dd bs=4k if='${block}'" > "${pull_file}"
  fi
}

function _adb_usage_erase-partition {
  echo "erase-partition <partition-name-or-path>, erase partition with dd command"
}
function adb_erase-partition {
  local partition="${1}"
  local partition_name="$(basename "${partition}")"

  local block=
  if ! block=$(adb_find-block "${partition}"); then
    return 1
  fi

  adb_rootshell "${adb_busybox} dd bs=4k if=/dev/zero of='${block}'"
}

function _adb_usage_flash-zip {
  echo "flash-zip <zip-file> [pre-hook] [pre-code] [push-dir], push and flash zip file to device
    pre-hook arg1 is update-binary file in host, arg2 is the zip file in device
    pre-code will be insert as header in update-binary
    default push-dir: /sdcard"
}
function adb_flash-zip {
  [ -n "${adb_verbose}" ] && msg2 "adb_flash-zip $*"
  local zip_file="${1}"
  local pre_hook="${2}"
  local pre_code="${3}"
  local push_dir="${4:-/sdcard}"
  local adb_device_tmpdir="${push_dir}"
  local host_tmpdir=$(mktemp -d)
  local device_zip_file="${push_dir}/$(basename "${zip_file}")"

  local unpdate_binary_file=META-INF/com/google/android/update-binary
  unpack_file "${zip_file}" "${host_tmpdir}" "${unpdate_binary_file}"

  # push file if not exists
  if adb_is-same-file "${device_zip_file}" "${zip_file}"; then
    msg2 "ignore push file to device: ${device_zip_file} already exists"
  else
    adb_push "${zip_file}" "${device_zip_file}"
  fi

  # unpack update-binary from zip and run it in device
  if is_func_exists "${pre_hook}"; then
    "${pre_hook}" "${host_tmpdir}/${unpdate_binary_file}" "${device_zip_file}"
  fi
  local retry_count=1
  local outfd=1
  ([ -n "${pre_code}" ] && echo "${pre_code}"; cat "${host_tmpdir}/${unpdate_binary_file}") | adb_run-script "${retry_count}" "${outfd}" "${device_zip_file}"

  adb_autoshell "${adb_busybox} rm -rf '${device_zip_file}'"
  rm -rf "${host_tmpdir}"
}
