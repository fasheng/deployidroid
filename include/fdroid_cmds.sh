# Depends: utils.sh, adb_utils.sh(optional), backup_cmds.sh

declare -A fdroid_depends=(
  [basename]=''
  [grep]=''
  [tr]=''
  [awk]=''
  [xmlstarlet]=''
)

# Configurable variables, could override in host script or config file
fdroid_cache_dir="${HOME}/.cache/${app_name}"
fdroid_allow_unstable=

declare -A fdroid_repos=(
  ["fdroid"]="https://f-droid.org/repo"
)

###*** F-Droid help functions

# return all available repo names, if fdroid_arg_repos not empty, will
# ignore repo that not in fdroid_arg_repos
function fdroid_get_repo_names {
  for r in "${!fdroid_repos[@]}"; do
    if [ ${#fdroid_arg_repos[@]} -gt 0 ]; then
      # limit operation only in specialized repos
      if contains "${r}" "${fdroid_arg_repos[@]}"; then
        echo "${r}"
      fi
    else
      echo "${r}"
    fi
  done
}

# arg1: index-file
function fdroid_check_index_file {
  local index_xml="${1}"
  if [ ! -f "${index_xml}" ]; then
    warning "${index_xml} not exists, please run 'update' first"
    return 1
  fi
  if ! xmlstarlet validate -q "${index_xml}"; then
    warning "${index_xml} is invalide, please run 'update' again to fix"
    return 1
  fi
  return 0
}

# Get suggest application version code
#   opts: fdroid_allow_unstable
#   arg1: appid, arg2: apilevel, arg3: abilist
function fdroid_get_app_suggest_version_code {
  local appid="${1}"
  local apilevel="${2}"
  if [ -z "${apilevel}" ] && adb_is-online; then
    apilevel=$(adb_get-android-api-level)
  fi
  local abilist="${3}"
  if [ -z "${abilist}" ] && adb_is-online; then
    abilist=$(adb_get-android-abi-list)
  fi

  local versions=
  if [ -n "${fdroid_allow_unstable}" ]; then
    versions=$(fdroid_get_app_versions "${1}")
  else
    versions=$(fdroid_get_app_versions "${1}" | \
      grep -i -v "version=.*beta.\?';" | \
      grep -i -v "version=.*alpha.\?';" | \
      grep -i -v "version=.*pre.\?';" | \
      grep -i -v "version=.*rc.\?';" | \
      grep -i -v "version=.*[-_]git';")
  fi
  local IFS=$'\n'
  local suggest_version= suggest_version_code=
  local version= version_code= sdkver= native_code=
  for v in ${versions}; do
    if [ -z "${v}" ]; then
      continue
    fi
    eval "${v}"

    # check sdk api level
    if [ -n "${apilevel}" -a -n "${sdkver}" ]; then
      if [ "${apilevel}" -lt "${sdkver}" ]; then
        continue
      fi
    fi

    # check cpu abi list
    if [ -n "${abilist}" -a -n "${native_code}" ]; then
      local arch
      for arch in $(echo "${native_code}" | tr ',' '\n'); do
        if contains "${arch}" ${abilist}; then
          if [ -z "${suggest_version}" ] || [ "${suggest_version_code}" -lt "${version_code}" ]; then
            suggest_version_code="${version_code}"
            suggest_version="${version}"
          fi
        fi
      done
    else
      if [ -z "${suggest_version}" ] || [ "${suggest_version_code}" -lt "${version_code}" ]; then
        suggest_version_code="${version_code}"
        suggest_version="${version}"
      fi
    fi
  done
  echo "${suggest_version_code}"
}

# output-format: version=xxx;version_code=xxx;sdkver=xxx;native_code=xxx
function fdroid_get_app_versions {
  local appid="${1}"
  for r in $(fdroid_get_repo_names); do
    local index_xml="${fdroid_cache_dir}/repo/${r}/index.xml"
    if ! fdroid_check_index_file "${index_xml}"; then
      continue
    fi
    xmlstarlet select -t -m "//fdroid/application[@id=\"${appid}\"]/package/version" --nl -o "version='" -v . -o "';version_code=" -m '../versioncode' -v . -o ';sdkver=' -m '../sdkver' -v . -o ';native_code=' -m '../nativecode' -v . "${index_xml}" || true
  done
}
function fdroid_get_app_version {
  local appid="${1}"
  local version_code="${2}"
  local version=
  for r in $(fdroid_get_repo_names); do
    local index_xml="${fdroid_cache_dir}/repo/${r}/index.xml"
    if ! fdroid_check_index_file "${index_xml}"; then
      continue
    fi
    local cmd="xmlstarlet select -t -m '//fdroid/application[@id=\"${appid}\"]/package/versioncode[.=\"${version_code}\"]/../version' -v . --nl ${index_xml}"
    version=$(eval "${cmd}" | head -n 1)
    if [ -n "${version}" ]; then
      break
    fi
  done
  echo "${version}"
}

function fdroid_get_app_native_code {
  local appid="${1}"
  local version="${2}"
  local native_code=
  for r in $(fdroid_get_repo_names); do
    local index_xml="${fdroid_cache_dir}/repo/${r}/index.xml"
    if ! fdroid_check_index_file "${index_xml}"; then
      continue
    fi
    local cmd="xmlstarlet select -t -m '//fdroid/application[@id=\"${appid}\"]/package/version[.=\"${version}\"]/../nativecode' -v . --nl ${index_xml}"
    native_code=$(eval "${cmd}" | head -n 1)
    if [ -n "${native_code}" ]; then
      break
    fi
  done
  echo "${native_code}" | tr ',' '\n'
}

# arg1: appid, arg2: version_code
function fdroid_get_app_repo {
  local appid="${1}"
  local version_code="${2}"
  local apkname
  local repo
  for r in $(fdroid_get_repo_names); do
    local index_xml="${fdroid_cache_dir}/repo/${r}/index.xml"
    if ! fdroid_check_index_file "${index_xml}"; then
      continue
    fi
    local cmd="xmlstarlet select -t -m '//fdroid/application[@id=\"${appid}\"]/package/versioncode[.=\"${version_code}\"]/../apkname' -v . --nl ${index_xml}"
    apkname=$(eval "${cmd}" | head -n 1)
    if [ -n "${apkname}" ]; then
      repo="${r}"
      break
    fi
  done
  echo "${repo}"
}

# arg1: appid, arg2: version_code
function fdroid_get_app_url {
  local appid="${1}"
  local version_code="${2}"
  local apkname
  local url
  for r in $(fdroid_get_repo_names); do
    local index_xml="${fdroid_cache_dir}/repo/${r}/index.xml"
    if ! fdroid_check_index_file "${index_xml}"; then
      continue
    fi
    local cmd="xmlstarlet select -t -m '//fdroid/application[@id=\"${appid}\"]/package/versioncode[.=\"${version_code}\"]/../apkname' -v . --nl ${index_xml}"
    apkname=$(eval "${cmd}" | head -n 1)
    if [ -n "${apkname}" ]; then
      url="${fdroid_repos[${r}]}/${apkname}"
      break
    fi
  done
  echo "${url}"
}

# arg1: appid, arg2: version_code
function fdroid_get_app_path {
  local appid="${1}"
  local version_code="${2}"
  local apkname= appdir= appfile=
  for r in $(fdroid_get_repo_names); do
    local index_xml="${fdroid_cache_dir}/repo/${r}/index.xml"
    if ! fdroid_check_index_file "${index_xml}"; then
      continue
    fi
    local cmd="xmlstarlet select -t -m '//fdroid/application[@id=\"${appid}\"]/package/versioncode[.=\"${version_code}\"]/../apkname' -v . --nl ${index_xml}"
    apkname=$(eval "${cmd}" | head -n 1)
    if [ -n "${apkname}" ]; then
      appdir="${fdroid_cache_dir}/app/${r}"
      appfile="${appdir}/$(basename "${apkname}")"
      break
    fi
  done
  echo "${appfile}"
}

# Return format: sha256:37203e6764fc0525cdf3b2555884dfbece45585ddd072022ea563563fe2149be
function fdroid_get_app_hash {
  local appid="${1}"
  local version_code="${2}"
  local hash
  for r in $(fdroid_get_repo_names); do
    local index_xml="${fdroid_cache_dir}/repo/${r}/index.xml"
    if ! fdroid_check_index_file "${index_xml}"; then
      continue
    fi
    local cmd="xmlstarlet select -t -m '//fdroid/application[@id=\"${appid}\"]/package/versioncode[.=\"${version_code}\"]/../hash' -v '@type' -o ':' -v '.' --nl ${index_xml}"
    hash=$(eval "${cmd}" | head -n 1)
    if [ -n "${hash}" ]; then
      break
    fi
  done
  echo "${hash}"
}

function fdroid_get_app_categories {
  local appid="${1}"
  local categories
  for r in $(fdroid_get_repo_names); do
    local index_xml="${fdroid_cache_dir}/repo/${r}/index.xml"
    if ! fdroid_check_index_file "${index_xml}"; then
      continue
    fi
    local cmd="xmlstarlet select -t -m '//fdroid/application[@id=\"${appid}\"]/categories' -v '.' --nl ${index_xml}"
    categories=$(eval "${cmd}" | head -n 1)
    if [ -n "${categories}" ]; then
      break
    fi
  done
  echo "${categories}"
}

function fdroid_get_app_display_name {
  local appid="${1}"
  local display_name=
  for r in $(fdroid_get_repo_names); do
    local index_xml="${fdroid_cache_dir}/repo/${r}/index.xml"
    if ! fdroid_check_index_file "${index_xml}"; then
      continue
    fi
    local cmd="xmlstarlet select -t -m '//fdroid/application[@id=\"${appid}\"]/name' -v '.' --nl ${index_xml}"
    display_name=$(eval "${cmd}" | head -n 1)
    if [ -n "${display_name}" ]; then
      break
    fi
  done
  echo "${display_name}"
}

# arg1: appid, arg2: [version_code]
function fdroid_is_app_exists {
  local appid="${1}"
  local version_code="${2}"
  for r in $(fdroid_get_repo_names); do
    local index_xml="${fdroid_cache_dir}/repo/${r}/index.xml"
    if ! fdroid_check_index_file "${index_xml}"; then
      continue
    fi
    local cmd=
    if [ -n "${version_code}" ]; then
      cmd="xmlstarlet select -t -m '//fdroid/application[@id=\"${appid}\"]/package/versioncode[.=\"${version_code}\"]' -v . ${index_xml}"
    else
      cmd="xmlstarlet select -t -m '//fdroid/application[@id=\"${appid}\"]/package/versioncode' -v . ${index_xml}"
    fi
    if eval "${cmd} &>/dev/null"; then
      return 0
    fi
  done
  return 1
}

###*** F-Droid commands

function _cmd_usage_update {
  echo "update [-r|--repo name]
    download fdroid repo index.xml file to cache direcotry"
}
function cmd_update {
  msg "Update F-Droid repository index.xml files.."
  for r in $(fdroid_get_repo_names); do
    local url="${fdroid_repos[${r}]}/index.xml"
    local repodir="${fdroid_cache_dir}/repo/${r}"
    msg2 "${r}: download ${url} to ${repodir}"

    local index_file="${repodir}/index.xml"
    if [ -f "${index_file}" ]; then
      mv -f "${index_file}" "${index_file}.old"
    fi
    if download_file "${url}" "${repodir}"; then
      if [ -f "${index_file}.old" ]; then
        rm -f "${index_file}.old"
      fi
    else
      warning "${r}: download ${url} failed"
      if [ -f "${index_file}.old" ]; then
        mv -f "${index_file}.old" "${index_file}"
      fi
    fi
  done
}

function _cmd_usage_list {
  echo "list [-r|--repo name] [--cat] [--desc] [--ver] [--sug] [--only-ver] [--allow-unstable] [--appid] [keyword|APP..]
    list matched applications
    --appid, use application id instead keyword
      the keyword support awk regexp syntax
    --cat, show and match for categories
    --desc, show and match for application description
    --ver, show all application version details
    --sug, show suggest version
      only works for keyword or APP specialized
    --only-ver, show application versions only"
}
function cmd_list {
  if [ ${#} -eq 0 ]; then
    _fdroid_show_app_info
    return
  fi

  local matched_apps=()
  local appid=
  if [ -n "${fdroid_arg_list_appid}" ]; then
    matched_apps=("${@}")
  else
    for appid in $(_fdroid_list_matched_apps "${1}" | sort | uniq); do
      matched_apps+=("${appid}")
    done
  fi
  for appid in "${matched_apps[@]}"; do
    _fdroid_show_app_info "${appid}" | xmlstarlet unesc
  done
}
# arg1: keyword
function _fdroid_list_matched_apps {
  local keyword="${1}"
  for r in $(fdroid_get_repo_names); do
    local index_xml="${fdroid_cache_dir}/repo/${r}/index.xml"
    if ! fdroid_check_index_file "${index_xml}"; then
      continue
    fi
    local cmd="xmlstarlet select -t -m '//fdroid/application' -o '====appid:' -v '@id' --nl $(_fdroid_build_show_app_xml_cmd) ${index_xml}"
    eval "${cmd}" | awk -v keyword="${keyword}" 'BEGIN{IGNORECASE=1;RS="===="} $0 ~ keyword{print}' | grep 'appid:' | awk -F: '{print $2}'
  done
}
# arg1: appid
function _fdroid_show_app_info {
  local appid="${1}"
  local found=
  for r in $(fdroid_get_repo_names); do
    local index_xml="${fdroid_cache_dir}/repo/${r}/index.xml"
    if ! fdroid_check_index_file "${index_xml}"; then
      continue
    fi
    local url_prefix="${fdroid_repos[${r}]}/"
    if [ -z "${appid}" ]; then
      eval "xmlstarlet select -t -m '//fdroid/application' $(_fdroid_build_show_app_xml_cmd "${url_prefix}") ${index_xml}" || true
      found=1
    else
      if eval "xmlstarlet select -t -m '//fdroid/application[@id=\"${appid}\"]' $(_fdroid_build_show_app_xml_cmd "${url_prefix}") ${index_xml}"; then
        if [ -n "${fdroid_arg_list_sug}" ]; then
          local suggest_version=
          local suggest_version_code=$(fdroid_get_app_suggest_version_code "${appid}")
          if [ -z "${suggest_version_code}" ]; then
            suggest_version="<none>"
            suggest_version_code="<none>"
          else
            suggest_version=$(fdroid_get_app_version "${appid}" "${suggest_version_code}")
          fi
          echo "  ${BOLD}suggest version: ${suggest_version} (${suggest_version_code})${ALL_OFF}"
        fi
        found=1
      fi
    fi
  done
  if [ -z "${found}" ]; then
    warning "Application ${appid} not found in repo"
  fi
}
# arg1: [url_prefix]
function _fdroid_build_show_app_xml_cmd {
  if [ -n "${fdroid_arg_list_only_ver}" ]; then
    echo "-m './package/versioncode' -v '../../@id' -o '=' -v '.' -o '	' -v '../../name' -o ' ' -v '../version' --nl"
    return
  fi

  local url_prefix="${1}"
  local cmd="-o '${YELLOW}' -v '@id' -o ' - ' -m './name' -v . -o '${ALL_OFF}' --nl"
  if [ -n "${fdroid_arg_list_cat}" ]; then
    cmd="${cmd} -o '  ${BOLD}categories:${ALL_OFF} ' -m '../categories' -v . --nl"
  fi
  if [ -n "${fdroid_arg_list_desc}" ]; then
    cmd="${cmd} -o '  ${BOLD}description:${ALL_OFF} ' -m '../desc' -v . --nl"
  fi
  if [ -n "${fdroid_arg_list_ver}" ]; then
    cmd="${cmd} -o '${BOLD}  versions:${ALL_OFF}' --nl -m '../package/version' -o '    ${BOLD}' -v . -o '${ALL_OFF}: date=' -m '../added' -v . -o ' versioncode=' -m '../versioncode' -v . -o ' sdkver=' --if '../sdkver' -v '../sdkver' --break -o ' nativecode=' --if '../nativecode' -v '../nativecode' --break -o ' url=${url_prefix}' -m '../apkname' -v . --nl"
  fi
  echo "${cmd}"
}

function _cmd_usage_download {
  echo "download [-r|--repo name] [--allow-unstable] [--force-download] <VERAPP..>
    download applications
    if already exists, check the hash
    --force-download, always download application even through file exists"
}
function cmd_download {
  local error_apps=()
  for verapp in "${@}"; do
    local appid="$(echo ${verapp} | awk -F= '{print $1}')"
    local version="$(echo ${verapp} | awk -F= '{print $2}')"
    if [ -z "${version}" ]; then
      version=$(fdroid_get_app_suggest_version_code "${appid}")
      if [ -z "${version}" ]; then
        warning "Ignore ${appid}: suggest version not found"
        error_apps+=("${appid}")
        continue
      fi
    fi
    if ! fdroid_is_app_exists "${appid}" "${version}"; then
      warning "Ignore ${appid}=${version}: not found in repo"
      error_apps+=("${appid}")
      continue
    fi
    local url="$(fdroid_get_app_url "${appid}" "${version}")"
    local repo="$(fdroid_get_app_repo "${appid}" "${version}")"
    local appdir="${fdroid_cache_dir}/app/${repo}"
    if [ "${arg_cmd}" = "download" ]; then
      msg "Download app ${appid}=${version} ${url}"
    else
      msg2 "download app ${appid}=${version} ${url}"
    fi
    local appfile="${appdir}/$(basename "${url}")" # the url from fdroid is decoded
    local hash="$(fdroid_get_app_hash "${appid}" "${version}")"
    if [ -n "${fdroid_arg_force_download}" ] || ! check_file_hash "${appfile}" "${hash}"; then
      download_file "${url}" "${appdir}"
    fi
    if ! check_file_hash "${appfile}" "${hash}"; then
      error_apps+=("${appid}")
    fi
  done
  if [ ${#error_apps[@]} -ne 0 ]; then
    warning "Download applications failed: ${error_apps[*]}"
    return 1
  else
    return 0
  fi
}

function _cmd_usage_install {
  echo "install [-r|--repo name] [-u|--user id] [--allow-unstable] [--allow-downgrade] [--install-path dir] <VERAPP|APKFILE..>
    download and install applications
    --install-path, install system application to special directory [default: '/system/app']
    --allow-downgrade, allow downgrade application"
}
function cmd_install {
  local error_apps=()
  for verapp in "${@}"; do
    local appfile= installed_sys= installed_3rd= appid=
    if [ -f "${verapp}" ]; then
      appfile="${verapp}"
      appid="${appfile}" # just set appid a value for error message
      if [ "${arg_cmd}" = "install" ]; then
        msg "Install app file ${appfile}"
      else
        msg2 "install app file ${appfile}"
      fi
    else
      appid="$(echo ${verapp} | awk -F= '{print $1}')"
      local version="$(echo ${verapp} | awk -F= '{print $2}')"
      if [ -z "${version}" ]; then
        version=$(fdroid_get_app_suggest_version_code "${appid}")
        if [ -z "${version}" ]; then
          warning "Ignore ${appid}: suggest version not found"
          error_apps+=("${appid}")
          continue
        fi
      fi
      if [ "${arg_cmd}" = "install" ]; then
        msg "Install app ${appid}=${version}"
      else
        msg2 "install app ${appid}=${version}"
      fi

      # check if already installed
      if adb_is-sys-package "${appid}"; then
        local installed_version=$(adb_get-package-version-code "${appid}")
        if [ "${installed_version}" = "${version}" ]; then
          installed_sys=1
        fi
      fi
      if adb_is-3rd-package "${appid}"; then
        local installed_version=$(adb_get-package-version-code "${appid}")
        if [ "${installed_version}" = "${version}" ]; then
          installed_3rd=1
        fi
      fi

      # check if application exists
      if [ -z "${installed_sys}" -a -z "${installed_3rd}" ]; then
        if ! fdroid_is_app_exists "${appid}" "${version}"; then
          warning "Ignore ${appid}=${version}: not found in repo"
          error_apps+=("${appid}")
          continue
        fi
      fi

      appfile=$(fdroid_get_app_path "${appid}" "${version}")
    fi

    if contains "system" "${fdroid_arg_users[@]}"; then
      msg2 "install ${appid} as system app"
      if [ -n "${installed_sys}" ]; then
        msg2 "ignore app ${appid}: already installed"
      else
        adb_assert-root
        if ! { cmd_download "${verapp}" && adb_install-sys "${appfile}" "${fdroid_arg_install_path}"; }; then
          error_apps+=("${appid}")
        fi
      fi
    elif [ ${#fdroid_arg_users[@]} -eq 0 ] || adb_is-only-one-user; then
      if [ -n "${installed_3rd}" ]; then
        msg2 "ignore app ${appid}: already installed"
        continue
      else
        msg2 "install app ${appid}"
        if ! { { [ -f "${appid}" ] || cmd_download "${appid}=${version}"; } && adb_install "${appfile}"; }; then
          error_apps+=("${appid}")
        fi
      fi
    else
      local user=
      for user in "${fdroid_arg_users[@]}"; do
        if [ "${user}" = "system" ]; then
          continue
        fi
        if ! adb_is-user-exists "${user}"; then
          msg2 "ignore user ${user}: user not exists"
        fi
        if [ -n "${installed_3rd}" ] && adb_is-3rd-package "${appid}" "${user}"; then
          msg2 "ignore app ${appid}: already installed for user ${user}"
        else
          msg2 "install app ${appid} for user ${user}"
          if ! { { [ -f "${appid}" ] || cmd_download "${verapp}"; } && adb_install "${appfile}" "${user}"; };then
            error_apps+=("${appid}")
          fi
        fi
      done
    fi
  done
  if [ ${#error_apps[@]} -ne 0 ]; then
    if [ "${arg_cmd}" = "install" ]; then
      warning "Install applications failed: ${error_apps[*]}"
    else
      warning "Install app failed: ${error_apps[*]}"
    fi
    return 1
  else
    return 0
  fi
}

function _cmd_usage_uninstall {
  echo "uninstall [-u|--user id] [--keep-data] <DEVICEAPP..>
    uninstall applications
    --keep-data, keep the data and cache directories after removal"
}
function cmd_uninstall {
  local error_apps=()
  local appid=
  for appid in "${@}"; do
    if [ "${arg_cmd}" = "uninstall" ]; then
      msg "Uninstall app ${appid}"
    else
      msg2 "uninstall app ${appid}"
    fi
    if ! adb_is-package-installed "${appid}"; then
      msg2 "ignore app ${appid}: not installed"
      continue
    fi

    if adb_is-sys-package "${appid}"; then
      msg2 "uninstall system app ${appid}"
      local device_appfile=$(adb_get-package-path "${appid}")
      if [[ "${device_appfile}" =~ ^/data/app ]]; then
        # shadowed system app
        if ! adb_uninstall "${appid}"; then
          error_apps+=("${appid}")
        fi
      else
        adb_assert-root
        if ! adb_uninstall-sys "${appid}"; then
          error_apps+=("${appid}")
        fi
      fi
      continue
    fi

    if [ ${#fdroid_arg_users[@]} -eq 0 ] || adb_is-only-one-user; then
      msg2 "uninstall app ${appid}"
      if ! adb_uninstall "${appid}"; then
        error_apps+=("${appid}")
      fi
    else
      local user=
      for user in "${fdroid_arg_users[@]}"; do
        if ! adb_is-user-exists "${user}"; then
          msg2 "ignore user ${user}: user not exists"
        fi
        if ! adb_is-package-installed "${appid}" "${user}"; then
          msg2 "ignore app ${appid}: not installed for user ${user}"
          continue
        fi
        msg2 "uninstall app ${appid} for user ${user}"
        if ! adb_uninstall "${appid}" "${user}"; then
          error_apps+=("${appid}")
        fi
      done
    fi
  done

  if [ ${#error_apps[@]} -ne 0 ]; then
    if [ "${arg_cmd}" = "uninstall" ]; then
      warning "Uninstall applications failed: ${error_apps[*]}"
    else
      warning "uninstall app failed: ${error_apps[*]}"
    fi
    return 1
  else
    return 0
  fi
}

function _cmd_usage_upgrade {
  echo "upgrade [-r|--repo name] [-u|--user id] [--allow-unstable] [-l|--list] [DEVICEAPP..]
    upgrade applications
    if no application id specialized, will upgrade all applications
    -l, --list, only list all upgradeable applications"
}
function cmd_upgrade {
  local users=
  if [ ${#fdroid_arg_users[@]} -gt 0 ]; then
    users="${fdroid_arg_users[@]}"
  else
    users="$(adb_list-users | awk '{print $1}')"
  fi

  local apps=()
  local error_apps=()
  for user in ${users}; do
    if [ ${#} -eq 0 ]; then
      if [ "${user}" = "system" ]; then
        eval "apps=( $(adb_list-sys-packages) )"
      else
        eval "apps=( $(adb_list-3rd-packages ${user}) )"
      fi
    else
      apps=("${@}")
    fi
    if [ "${user}" = "system" ]; then
      msg "Upgrade system applications"
    elif adb_is-only-one-user; then
      msg "Upgrade applications"
    else
      msg "Upgrade applications for user ${user}"
    fi
    local appid=
    for appid in "${apps[@]}"; do
      if ([ "${user}" = "system" ] && adb_is-sys-package "${appid}") || ([ "${user}" != "system" ] && adb_is-3rd-package "${appid}" "${user}"); then
        local suggest_version_code=$(fdroid_get_app_suggest_version_code "${appid}")
        if [ -n "${suggest_version_code}" ]; then
          local display_name=$(fdroid_get_app_display_name "${appid}")
          local suggest_version=$(fdroid_get_app_version "${appid}" "${suggest_version_code}")
          local installed_version=$(adb_get-package-version-name "${appid}")
          local installed_version_code=$(adb_get-package-version-code "${appid}")
          if [ "${suggest_version_code}" -gt "${installed_version_code}" ]; then
            msg2 "${appid}(${display_name}): ${installed_version}(${installed_version_code}) -> ${YELLOW}${suggest_version}(${suggest_version_code})${ALL_OFF}"
            if [ -z "${fdroid_arg_upgrade_list}" ]; then
              local fdroid_arg_users=("${user}")
              if ! cmd_install "${appid}=${suggest_version_code}"; then
                error_apps+=("${appid}")
              fi
            fi
          elif [ -n "${fdroid_verbose}" ]; then
            msg2 "ignore app ${appid}(${display_name}): up to date"
          fi
        elif [ -n "${fdroid_verbose}" ]; then
          if fdroid_is_app_exists "${appid}"; then
            msg2 "ignore app ${appid}: suggest version not found"
          else
            msg2 "ignore app ${appid}: not found in repo"
          fi
        fi
      else
        if [ "${user}" = "system" ]; then
          msg2 "ignore system app ${appid}: not installed"
        elif adb_is-only-one-user; then
          msg2 "ignore app ${appid}: not installed"
        else
          msg2 "ignore app ${appid}: not installed for user ${user}"
        fi
      fi
    done
  done
  if [ ${#error_apps[@]} -ne 0 ]; then
    warning "Upgrade applications failed:  ${error_apps[*]}"
    return 1
  else
    return 0
  fi
}

###*** Dispatch F-Droid arguments

fdroid_arg_list_cat=
fdroid_arg_list_desc=
fdroid_arg_list_ver=
fdroid_arg_list_only_ver=
fdroid_arg_list_sug=
fdroid_arg_list_appid=
fdroid_arg_force_download=
fdroid_arg_install_path=
fdroid_arg_upgrade_list=
declare -a fdroid_arg_users
declare -a fdroid_arg_repos

declare -a fdroid_left_args=()
function fdroid_dispatch_args {
  while [ ${#} -gt 0 ]; do
    case "${1}" in
      --cache-dir) fdroid_cache_dir="${2}"; shift;shift;;
      --allow-downgrade) adb_allow_downgrade=1; shift;;
      --keep-data) adb_keep_data=1; shift;;
      --allow-unstable) fdroid_allow_unstable=1; shift;;
      --cat) fdroid_arg_list_cat=1; shift;;
      --desc) fdroid_arg_list_desc=1; shift;;
      --ver) fdroid_arg_list_ver=1; shift;;
      --only-ver) fdroid_arg_list_only_ver=1; shift;;
      --sug) fdroid_arg_list_sug=1; shift;;
      --appid) fdroid_arg_list_appid=1; shift;;
      # ignore --user here, just accept backup_arg_users's value
      # -u|--user) shift;shift;;
      -r|--repo)
        fdroid_arg_repos+=("${2}")
        shift;shift;;
      --force-download) fdroid_arg_force_download=1; shift;;
      --install-path) fdroid_arg_install_path="${2}"; shift;shift;;
      -l|--list) fdroid_arg_upgrade_list=1; shift;;
      *) fdroid_left_args+=("${1}"); shift;;
    esac
  done
}
