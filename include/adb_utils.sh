###*** ADB functions that could be load in adb_utilshell

###**** System mode
function adbfunc_is_boot_mode {
  if [ -n "${ADB_CACHE_IS_BOOT_MODE}" ]; then
    ${ADB_CACHE_IS_BOOT_MODE}
    return
  fi
  ${adb_busybox} ps | ${adb_busybox} grep zygote | ${adb_busybox} grep -qv grep && return 0
  ${adb_busybox} ps -A 2>/dev/null | ${adb_busybox} grep zygote | ${adb_busybox} grep -qv grep && return 0
  return 1
}

function adbfunc_is_recovery_mode {
  [ "$(getprop init.svc.recovery)" = "running" ]
}

adbfunc_is_twrp_mode_depend_funcs=(is_cmd_exists)
function adbfunc_is_twrp_mode {
  [ "$(getprop ro.twrp.boot)" = "1" ] || [ -n "$(getprop ro.twrp.sar)" ] || is_cmd_exists twrp
}

function adbfunc_is_waydroid_mode {
  [ "$(getprop ro.board.platform)" = "waydroid" ]
}

###**** System information
function adbfunc_get_current_slot {
  getprop ro.boot.slot_suffix | tr -d '_'
}

function adbfunc_get_kernel_version {
  ${adb_busybox} uname -r
}

function adbfunc_get_android_version {
  getprop ro.build.version.release
}

function adbfunc_get_android_api_level {
  getprop ro.build.version.sdk
}

function adbfunc_get_android_abi_list {
  local cpu_abilist=$(getprop ro.product.cpu.abilist)
  local cpu_abi=$(getprop ro.product.cpu.abi)
  if [ -n "${cpu_abilist}" ]; then
    echo "${cpu_abilist}" | ${adb_busybox} tr ',' '\n'
  else
    echo "${cpu_abi}" | ${adb_busybox} tr ',' '\n'
  fi
}

###**** Mount
function adbfunc_find_block {
  local partition="${1}"
  case "${partition}" in
    /dev*)
      if [ -b "${partition}" -o -c "${partition}" -o -L "${partition}" ]; then
        echo "${partition}"
        return
      else
        return 1
      fi
      ;;
  esac

  local device=$(${adb_busybox} find /dev/block \( -type b -o -type c -o -type l \) -iname "${partition}" | head -n 1) 2>/dev/null
  if [ -n "${device}" ]; then
    echo "${device}"
    return 0
  fi
  return 1
}

adbfunc_is_system_as_root_depend_funcs=(adbfunc_is_boot_mode)
function adbfunc_is_system_as_root {
  if [ -n "${ADB_CACHE_IS_SYSTEM_AS_ROOT}" ]; then
    ${ADB_CACHE_IS_SYSTEM_AS_ROOT}
    return
  fi

  if [ -f /system/init -o -L /system/init ]; then
    return 0
  elif adbfunc_is_boot_mode; then
    local mnt_type=$(${adb_busybox} head -1 /proc/self/mountinfo | awk '{ printf $9 }')
    if [ "${mnt_type}" != "rootfs" ]; then
      return 0
    fi
  else
    # reovery mode
    if ${adb_busybox} grep -v '#' /etc/*fstab* | ${adb_busybox} grep -q ' /system_root '; then
      return 0
    fi
  fi
  return 1
}

adbfunc_is_dynamic_partition_depend_funcs=(adbfunc_find_block)
function adbfunc_is_dynamic_partition {
  if ${adb_busybox} grep -q 'androidboot.super_partition' /proc/cmdline; then
    return 0
  elif [ -n "$(adbfunc_find_block super)" ]; then
    return 0
  fi
  return 1
}

function adbfunc_is_mount_point {
  ${adb_busybox} mountpoint -q "${1}"
}

# ignore error when path not exists
function adbfunc_find_mount_point {
  local path="${1}"
  while true; do
    path=$(realpath "${path}" || echo "${path}")
    ${adb_busybox} mountpoint -q "${path}" 2>/dev/null && break
    path=$(dirname "${path}")
    [ "${path}" = "/" ] && break
  done
  if ${adb_busybox} mountpoint -q "${path}" 2>/dev/null; then
    echo "${path}"
    return 0
  else
    return 1
  fi
}

# arg1: <path>, arg2: <mode>, arg3: [find-mount-point]
adbfunc_remount_depend_funcs=(adbfunc_find_mount_point adbfunc_check_mount_rw_warning)
function adbfunc_remount {
  if [ -z "${3}" ]; then
    local mount_point=$(realpath "${1}") || return 1
  else
    local mount_point=$(adbfunc_find_mount_point "${1}") || return 1
  fi
  ${adb_busybox} mount -o remount,"${2}" "${mount_point}"

  # NOTE: kernelsu overlayfs may not allow remount as read-write but
  # never return error code, so always
  if [ "${2}" = "rw" ]; then
    adbfunc_check_mount_rw_warning "${mount_point}"
  fi
}

# arg1: <path>, arg2: <mode>, arg3: [find-mount-point]
adbfunc_check_mount_mode_depend_funcs=(adbfunc_find_mount_point)
function adbfunc_check_mount_mode {
  if [ -z "${3}" ]; then
    local mount_point=$(realpath "${1}") || return 1
  else
    local mount_point=$(adbfunc_find_mount_point "${1}") || return 1
  fi
  local mode="${2}"
  local IFS=$'\t\n ,'
  for i in $(${adb_busybox} cat /proc/mounts | ${adb_busybox} grep " ${mount_point} " | awk '{print $4}'); do
    [ "${i}" = "${mode}" ] && return 0
  done
  return 1
}

# arg1: <path>, arg2: [find-mount-point]
adbfunc_check_mount_rw_warning_depend_funcs=(adbfunc_check_mount_mode warning)
function adbfunc_check_mount_rw_warning {
  # redirect output to stderr to to prevent influence of result
  if ! adbfunc_check_mount_mode "${1}" "rw" "${2}"; then
    case "${1}" in
      /system*|/vendor*)
        warning "Mount failed: ${1} is still read-only. If there is Magisk/KernelSU/APatch installed, please disable all modules and retry after reboot" 1>&2 ;;
      *)
        warning "Mount failed: ${1} is still read-only" 1>&2 ;;
    esac
    return 1
  fi
}

adbfunc_bind_mount_rw_depend_funcs=(adbfunc_check_mount_rw_warning)
function adbfunc_bind_mount_rw {
  ${adb_busybox} mount -o bind,private "${1}" "${2}"
  ${adb_busybox} mount -o remount,rw "${2}"
  adbfunc_check_mount_rw_warning "${2}"
}

# arg1: <path>, arg2: [mirror-dir]
adbfunc_get_mount_mirror_depend_funcs=(adbfunc_is_system_as_root)
function adbfunc_get_mount_mirror {
  local path="${1}"
  local mirror_dir="${2:-/dev/mount_mirror}"
  local mp_mirror_dir="${mirror_dir}${path}"

  if [ "${path}" = "/" ]; then
    # special for root
    mp_mirror_dir="${mirror_dir}/system_root"
  elif adbfunc_is_system_as_root; then
    # special for /system
    case "${path}" in
      /system*)
        mp_mirror_dir="${mirror_dir}/system_root${path}" ;;
    esac
  fi

  echo "${mp_mirror_dir}"
}

# arg1: <path>, arg2: [mirror-dir]
adbfunc_has_mount_mirror_depend_funcs=(adbfunc_get_mount_mirror adbfunc_find_mount_point is_child_dir adbfunc_check_mount_mode)
function adbfunc_has_mount_mirror {
  local path="${1}"
  local mirror_dir="${2:-/dev/mount_mirror}"
  local mp_mirror_dir=$(adbfunc_get_mount_mirror "${path}" "${mirror_dir}")

  local real_mp_dir=$(adbfunc_find_mount_point "${mp_mirror_dir}" 2>/dev/null) || return 1
  # real mount point not under base mirror_dir means it's not a mirror mount
  is_child_dir "${mirror_dir}" "${real_mp_dir}" || return 1

  adbfunc_check_mount_mode "${real_mp_dir}" "rw" 2>/dev/null
}

# arg1: <path>, arg2: [mirror-dir]
adbfunc_mount_mirror_depend_funcs=(adbfunc_has_mount_mirror adbfunc_get_mount_mirror adbfunc_find_mount_point adbfunc_bind_mount_rw)
function adbfunc_mount_mirror {
  local path="${1}"
  local mirror_dir="${2:-/dev/mount_mirror}"

  if adbfunc_has_mount_mirror "${path}" "${mirror_dir}"; then
    msg2 "ignore, there is already a read-write mirror for ${path}" 1>&2
    return 0
  fi

  local mp_mirror_dir=$(adbfunc_get_mount_mirror "${path}" "${mirror_dir}")
  ${adb_busybox} mkdir -p "${mp_mirror_dir}"

  local real_mp_dir=$(adbfunc_find_mount_point "${mp_mirror_dir}") || return 1
  if is_child_dir "${mirror_dir}" "${real_mp_dir}" && [ "${real_mp_dir}" != "${mp_mirror_dir}" ]; then
    # real mount point not under base mirror_dir means it's not a mirror mount
    # real mount point not equal with prefer means it's a mirror mount of parent
    msg2 "ignore, there is a parent mirror mount" 1>&2
    return 0
  fi

  ${adb_busybox} umount "${mp_mirror_dir}" 2>/dev/null || true
  adbfunc_bind_mount_rw "${path}" "${mp_mirror_dir}"
}

# arg1: <path>, arg2: [mirror-dir]
adbfunc_unmount_mirror_depend_funcs=(adbfunc_get_mount_mirror adbfunc_find_mount_point is_child_dir msg2)
function adbfunc_unmount_mirror {
  local path="${1}"
  local mirror_dir="${2:-/dev/mount_mirror}"
  local mp_mirror_dir=$(adbfunc_get_mount_mirror "${path}" "${mirror_dir}")

  local real_mp_dir=$(adbfunc_find_mount_point "${mp_mirror_dir}") || return 1
  # real mount point not under base mirror_dir means it's not a mirror mount
  is_child_dir "${mirror_dir}" "${real_mp_dir}" || return 0
  # real mount point not equal with prefer means it's a mirror mount of parent
  if [ "${real_mp_dir}" != "${mp_mirror_dir}" ]; then
    msg2 "ignore, there is a parent mirror mount" 1>&2
    return 0
  fi

  ${adb_busybox} umount "${mp_mirror_dir}" || return 1
  ${adb_busybox} rmdir "${mp_mirror_dir}" || true
}

# ensure /system was mounted as read-write, mirror will be mounted if
# necessary to fix magisk/kernelsu/apatch overlayfs read-only issue
# arg1: [mirror-dir]
adbfunc_ensure_mount_system_rw_depend_funcs=(adbfunc_check_mount_mode adbfunc_is_boot_mode adbfunc_remount adbfunc_is_system_as_root msg2)
function adbfunc_ensure_mount_system_rw {
  local mirror_dir="${1:-/dev/mount_mirror}"
  if adbfunc_check_mount_mode "/system" "rw"; then
    msg2 "ignore, ${mount_point} is already mounted as read-write" 1>&2
    return 0
  fi
  if adbfunc_is_boot_mode; then
    # normal boot mode
    if adbfunc_remount "/system" "rw" 1; then
      msg2 "normal boot mode, remount /system as read-write directly" 1>&2
    elif adbfunc_is_system_as_root && adbfunc_mount_mirror "/" "${mirror_dir}"; then
      msg2 "normal boot mode, mount / as read-write mirror for remount ${mount_point} failed" 1>&2
    else
      return 1
    fi
  else
    # recovery mode
    if ${adb_busybox} mount "/system_root" 2>/dev/null || ${adb_busybox} mount -o remount,rw "/system_root" 2>/dev/null; then
      ${adb_busybox} mount -o bind "/system_root/system" "/system"
      ${adb_busybox} mount -o remount,rw "/system"
      msg2 "recovery mode, remount /system_root /system as read-write directly" 1>&2
    elif ${adb_busybox} mount "/system" 2>/dev/null || ${adb_busybox} mount -o remount,rw "/system"; then
      msg2 "recovery mode, remount /system as read-write directly" 1>&2
    fi
    adbfunc_check_mount_mode "/system" "rw" || return 1
  fi
}

# ensure target mount point was mounted as read-write
# arg1: <path>, arg2: [find-mount-point], arg3: [mirror-dir]
adbfunc_ensure_mount_rw_depend_funcs=(adbfunc_find_mount_point adbfunc_ensure_mount_system_rw adbfunc_is_boot_mode adbfunc_remount adbfunc_mount_mirror adbfunc_check_mount_mode msg2)
function adbfunc_ensure_mount_rw {
  if [ -z "${2}" ]; then
    local mount_point=$(realpath "${1}") || return 1
  else
    local mount_point=$(adbfunc_find_mount_point "${1}") || return 1
  fi
  local mirror_dir="${3:-/dev/mount_mirror}"

  # special for /system
  if [ "${mount_point}" = "/system" ]; then
    adbfunc_ensure_mount_system_rw "${mirror_dir}"
    return
  fi

  if adbfunc_check_mount_mode "${mount_point}" "rw"; then
    msg2 "ignore, ${mount_point} is already mounted as read-write" 1>&2
    return 0
  fi

  # ensure already mounted
  ${adb_busybox} mountpoint -q "${mount_point}" 2>/dev/null || ${adb_busybox} mount "${mount_point}" || return 1

  if adbfunc_is_boot_mode; then
    # normal boot mode, try remount directly, if failed then will mount as mirror
    if adbfunc_remount "${mount_point}" "rw"; then
      msg2 "normal boot mode, remount ${mount_point} as read-write directly" 1>&2
    elif adbfunc_mount_mirror "${mount_point}" "${mirror_dir}"; then
      msg2 "normal boot mode, mount ${mount_point} as read-write mirror for remount failed" 1>&2
    else
      return 1
    fi
  else
    # recovery mode, try remount directly
    if adbfunc_remount "${mount_point}" "rw"; then
      msg2 "recovery mode, remount ${mount_point} as read-write directly" 1>&2
    else
      return 1
    fi
  fi
}

# get read-write dir for target path, mainly used to handle the
# situation when mirror was mounted, should be called after
# adbfunc_ensure_mount_rw
# arg1: <path>, arg2: [mirror-dir]
adbfunc_get_mount_rw_path_depend_funcs=(adbfunc_has_mount_mirror adbfunc_get_mount_mirror)
function adbfunc_get_mount_rw_path {
  local path="${1}"
  local mirror_dir="${2:-/dev/mount_mirror}"
  if adbfunc_has_mount_mirror "${path}" "${mirror_dir}"; then
    adbfunc_get_mount_mirror "${path}" "${mirror_dir}"
  else
    echo "${path}"
  fi
}
