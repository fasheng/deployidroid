# Depends: utils.sh

declare -A adb_depends=(
  [tr]=''
  [basename]=''
  [dirname]=''
  [grep]=''
  [sed]=''
  [awk]=''
  [sha1sum]=''
  [adb]='please install android-sdk or android-platform-tools firstly'
)

adb_cmd="adb"

# Constant variables

ANDROID_VERSION_CODES_P=28
ANDROID_VERSION_CODES_O=26
ANDROID_VERSION_CODES_N=24
ANDROID_VERSION_CODES_M=23
ANDROID_VERSION_CODES_LOLLIPOP=21
ANDROID_VERSION_CODES_JELLY_BEAN_MR1=17
ANDROID_VERSION_CODES_ICE_CREAM_SANDWICH=14
ANDROID_FIRST_APPLICATION_UID=10000

ADB_DEFAULT_DEVICE_TMPDIR='/data/local/tmp'
ADB_DEFAULT_ROOT_TYPE='auto'

# Cache variables

declare -A ADB_CACHE_USER_PACKAGES
ADB_CACHE_ALL_PACKAGES=
ADB_CACHE_ALL_SYS_PACKAGES=
ADB_CACHE_ALL_3RD_PACKAGES=
ADB_CACHE_ANDROID_VERSION=
ADB_CACHE_ANDROID_API_LEVEL=
ADB_CACHE_USERS=
ADB_CACHE_ANDROID_ABI_LIST=

# Optional variables, could override in host script or config file
adb_opt_verbose=
adb_opt_root_type="${ADB_DEFAULT_ROOT_TYPE}"
adb_opt_device_tmpdir="${ADB_DEFAULT_DEVICE_TMPDIR}"
adb_opt_allow_downgrade=
adb_opt_keep_data=
adb_opt_busybox=
adb_opt_serial=

###*** Adb functions

function _adb_strip_output {
  tr -d '\r'
}

# arg1: user-id
function _adb_build_user_arg {
  local user="${1}"
  if [ -z "${user}" ] || ! adb_is-pm-support-multiuser; then
    echo ''
  else
    echo "--user '${user}'"
  fi
}

function _adb_ensure_tmpdir_exists {
  adb_autoshell "${adb_opt_busybox} mkdir -p '${adb_opt_device_tmpdir}'"
  adb_autoshell "${adb_opt_busybox} chmod 777 '${adb_opt_device_tmpdir}'"
}

# run adb shell in root permission if possible, or will fallback to normal shell
# opts: adb_opt_root_type
function _adb_usage_autoshell {
  echo "autoshell <command>"
}
function adb_autoshell {
  local cmdline="${*}"
  if adb_has-root; then
    adb_rootshell "${cmdline}"
  else
    adb_shell "${cmdline}"
  fi
}

# run adb shell and trip the result
# opts: adb_opt_verbose
function _adb_usage_shell {
  echo "shell <command>"
}
function adb_shell {
  local cmdline="${*}"
  if [ -z "${adb_opt_verbose}" -o 0"${adb_opt_verbose}" -lt 2 ]; then
    if ! [[ "${cmdline}" =~ ">/dev/null"$ ]]; then
      cmdline="${cmdline} 2>/dev/null"
    fi
  fi
  if [ -n "${adb_opt_verbose}" ]; then
    msg2 "adb shell %s" "${cmdline}"
  fi
  ${adb_cmd} shell "${cmdline}" | _adb_strip_output
}

function _adb_usage_rootshell {
  echo "rootshell <command>"
}
function adb_rootshell {
  local cmdline="${*}"
  if [ -z "${adb_opt_verbose}" -o 0"${adb_opt_verbose}" -lt 2 ]; then
    if ! [[ "${cmdline}" =~ ">/dev/null"$ ]]; then
      cmdline="${cmdline} 2>/dev/null"
    fi
  fi

  if adb_has-root; then
    if [ x"${adb_opt_root_type}" = x"su" ]; then
      if [ -n "${adb_opt_verbose}" ]; then
        msg2 "adb shell su -c %s" "${cmdline}"
      fi
      ${adb_cmd} shell su -c "'${cmdline}'" | _adb_strip_output
    elif [ x"${adb_opt_root_type}" = x"adb-root" ]; then
      if [ -n "${adb_opt_verbose}" ]; then
        msg2 "adb rootshell %s" "${cmdline}"
      fi
      ${adb_cmd} shell "${cmdline}" | _adb_strip_output
    fi
  else
    warning "Missing root permission: adb rootshell %s" "${cmdline}"
  fi
}

# arg1: command-format(use %s to replace with user arg), arg2: [user-id] (user-id default: all users)
function _adb_shell_with_user_arg {
  local cmdfmt="${1}"
  local user="${2}"
  if [ -z "${user}" ]; then
    for u in $(_adb_list-users-plain); do
      adb_shell "$(printf "${cmdfmt}" "$(_adb_build_user_arg ${u})")"
    done
  else
    adb_shell "$(printf "${cmdfmt}" "$(_adb_build_user_arg ${user})")"
  fi
}

# arg1: command-format(use %s to replace with user arg), arg2: [user-id] (user-id default: all users)
function _adb_rootshell_with_user_arg {
  local cmdfmt="${1}"
  local user="${2}"
  if [ -z "${user}" ]; then
    for u in $(_adb_list-users-plain); do
      adb_rootshell "$(printf "${cmdfmt}" "$(_adb_build_user_arg ${u})")"
    done
  else
    adb_rootshell "$(printf "${cmdfmt}" "$(_adb_build_user_arg ${user})")"
  fi
}

function _adb_usage_check-online {
  echo "check-online"
}
function adb_check-online {
  ${adb_cmd} shell "echo ''" &>/dev/null || abort "Looks adb device(${adb_opt_serial:-default}) is offline"
}

function _adb_usage_is-online {
  echo "is-online"
}
function adb_is-online {
  if ! is_cmd_exists "${adb_cmd}"; then
    return 1
  fi
  if ${adb_cmd} shell "echo ''" &>/dev/null; then
    return 0
  else
    return 1
  fi
}

function _adb_usage_check-root {
  echo "check-root"
}
function adb_check-root {
  _fix_root_type
  if [ -z "${adb_opt_root_type}" ] || [[ ! $(adb_rootshell '${adb_opt_busybox} id') =~ "root" ]]; then
    abort "Looks there is no root permission in adb shell, please ensure 'adb root' works or 'su' command exists"
  fi
}

function _adb_usage_has-root {
  echo "has-root"
}
function adb_has-root {
  _fix_root_type
  if [ -z "${adb_opt_root_type}" ]; then
    return 1
  fi
  return 0
}

function _fix_root_type {
  if [ x"${adb_opt_root_type}" = x"auto" ]; then
    if [[ $(${adb_cmd} shell 'id') =~ "root" ]]; then
      adb_opt_root_type='adb-root'
    elif [[ $(${adb_cmd} shell su -c 'id' 2>/dev/null) =~ "root" ]]; then
      adb_opt_root_type='su'
    else
      adb_opt_root_type=
    fi
  elif [ x"${adb_opt_root_type}" = x"none" ]; then
    adb_opt_root_type=
  fi
}

function _adb_usage_is-cmd-exists {
  echo "is-cmd-exists <cmd>"
}
function adb_is-cmd-exists {
  local result=$(adb_shell "if type '${1}' &>/dev/null; then ${adb_opt_busybox} echo 1; fi")
  if [ -z "${result}" ]; then
    return 1
  else
    return 0
  fi
}

function _adb_usage_get-kernel-version {
  echo "get-kernel-version"
}
function adb_get-kernel-version {
  adb_shell uname -r
}

# fill CACHE_xxx variables to speed up
function _adb_fetch_cache {
  if [ -z "${ADB_CACHE_ANDROID_VERSION}" ]; then
    msg "Cache adb output"
  fi
  adb_get-android-version 1>/dev/null
  adb_get-android-api-level 1>/dev/null
  adb_get-android-abi-list 1>/dev/null
  _adb_fetch_users
  _adb_fetch_pkgs_cache
}
function _adb_fetch_users {
  ADB_CACHE_USERS=
  adb_list-users 1>/dev/null
}
function _adb_fetch_pkgs_cache {
  ADB_CACHE_USER_PACKAGES=()
  ADB_CACHE_ALL_PACKAGES=
  ADB_CACHE_ALL_SYS_PACKAGES=
  ADB_CACHE_ALL_3RD_PACKAGES=
  local user=
  for user in $(_adb_list-users-plain); do
    adb_list-3rd-packages "${user}" 1>/dev/null
  done
  adb_list-sys-packages 1>/dev/null
}

function _adb_usage_get-android-version {
  echo "get-android-version"
}
function adb_get-android-version {
  if [ -z "${ADB_CACHE_ANDROID_VERSION}" ]; then
    ADB_CACHE_ANDROID_VERSION=$(adb_shell "getprop ro.build.version.release")
  fi
  echo "${ADB_CACHE_ANDROID_VERSION}"
}

function _adb_usage_get-android-api-level {
  echo "get-android-api-level"
}
function adb_get-android-api-level {
  if [ -z "${ADB_CACHE_ANDROID_API_LEVEL}" ]; then
    ADB_CACHE_ANDROID_API_LEVEL=$(adb_shell "getprop ro.build.version.sdk")
    if [ -z "${ADB_CACHE_ANDROID_API_LEVEL}" ]; then
      # looks adb device is offline
      ADB_CACHE_ANDROID_API_LEVEL=0
    fi
  fi
  echo "${ADB_CACHE_ANDROID_API_LEVEL}"
}

function _adb_usage_get-android-abi-list {
  echo "get-android-abi-list"
}
function adb_get-android-abi-list {
  if [ -z "${ADB_CACHE_ANDROID_ABI_LIST}" ]; then
    local cpu_abilist=$(adb_shell "getprop ro.product.cpu.abilist")
    local cpu_abi=$(adb_shell "getprop ro.product.cpu.abi")
    if [ -n "${cpu_abilist}" ]; then
      ADB_CACHE_ANDROID_ABI_LIST="${cpu_abilist}"
    else
      ADB_CACHE_ANDROID_ABI_LIST="${cpu_abi}"
    fi
  fi
  echo "${ADB_CACHE_ANDROID_ABI_LIST}" | tr ',' '\n'
}

function _adb_usage_is-support-multiuser {
  echo "is-support-multiuser [apilevel]"
}
function adb_is-support-multiuser {
  local apilevel="${1:-$(adb_get-android-api-level)}"
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_ICE_CREAM_SANDWICH}" ]; then
    return 0
  else
    return 1
  fi
}

function _adb_usage_is-pm-support-multiuser {
  echo "is-pm-support-multiuser [apilevel]"
}
function adb_is-pm-support-multiuser {
  local apilevel="${1:-$(adb_get-android-api-level)}"
  # Android support multiple user since 4.0, but the am/pm command support "--user" option after 4.2
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_JELLY_BEAN_MR1}" ]; then
    return 0
  else
    return 1
  fi
}

function _adb_usage_list-users {
  echo "list-users"
}
function adb_list-users {
  if adb_is-support-multiuser; then
    if [ -z "${ADB_CACHE_USERS}" ]; then
      ADB_CACHE_USERS=$(adb_shell "pm list users 2>/dev/null")
    fi
    if [ -z "${ADB_CACHE_USERS}" ]; then
      # some device need root permission to list users, just fallback here
      ADB_CACHE_USERS='UserInfo{0:Primary:3}'
    fi
    echo "${ADB_CACHE_USERS}" | grep -o '{.*}' | tr -d '{}' | awk -F: '{print $1,$2}'
  else
    echo '0 <none>'
  fi
}
function _adb_list-users-plain {
  adb_list-users | awk '{print $1}'
}

function _adb_usage_is-only-one-user {
  echo "is-only-one-user"
}
function adb_is-only-one-user {
  local user_num=$(adb_list-users | wc | awk '{print $1}')
  if [ "${user_num}" -eq 0 ]; then
    return 0
  else
    return 1
  fi
}

function _adb_usage_is-user-exists {
  echo "is-user-exists <user-id>"
}
function adb_is-user-exists {
  local user=
  for user in $(_adb_list-users-plain); do
    if [ x"${1}" = x"${user}" ]; then
      return 0
    fi
  done
  return 1
}

function _adb_usage_create-user {
  echo "create-user <user-id> <user-name>"
}
function adb_create-user {
  adb_rootshell "pm create-user --profileOf '${1}' '${2}'"
}

function _adb_usage_ensure-user-exists {
  echo "ensure-user-exists <user-id> <user-name>"
}
function adb_ensure-user-exists {
  local user="${1}"
  local user_name="${2:-test}"
  if ! adb_is-user-exists "${user}"; then
    adb_create-user "${user}" "${user_name}"
  fi
  # update cached users
  _adb_fetch_users
  if ! adb_is-user-exists "${user}"; then
    warning "Create user ${user} failed"
    read -n1 -p "Please create user manually and press any key to continue..."
    adb_ensure-user-exists "${user}" "${user_name}"
  fi
}

function _adb_usage_get-current-user {
  echo "get-current-user"
}
function adb_get-current-user {
  local user=
  if adb_is-pm-support-multiuser; then
    user=$(adb_shell "dumpsys activity" | grep "mUserLru" | grep '\[.*\]' | grep -o '[0-9]\+' | tail -n 1)
  fi
  if [ -z "${user}" ]; then
    user='0'
  fi
  echo "${user}"
}

function _adb_usage_get-user-name {
  echo "get-user-name [user-id] (user-id default: current user)"
}
function adb_get-user-name {
  local user="${1:-$(adb_get-current-user)}"
  if adb_is-support-multiuser; then
    adb_list-users | grep "^${user} " | awk '{print $2}'
  else
    echo '<none>'
  fi
}

function _adb_usage_list-sys-packages {
  echo "list-sys-packages"
}
function adb_list-sys-packages {
  if [ -z "${ADB_CACHE_ALL_SYS_PACKAGES}" ]; then
    ADB_CACHE_ALL_SYS_PACKAGES=$(adb_shell "pm list packages -f -s" | sort | uniq)
    ADB_CACHE_ALL_PACKAGES=$(echo -e "${ADB_CACHE_ALL_PACKAGES}\n${ADB_CACHE_ALL_SYS_PACKAGES}" | sort | uniq)
  fi
  local pkg=
  for pkg in $(echo "${ADB_CACHE_ALL_SYS_PACKAGES}" | awk -F: '{print $2}'); do
    echo "${pkg##*=}"
  done
}

function _adb_usage_list-3rd-packages {
  echo "list-3rd-packages [user-id] (user-id default: current user)"
}
function adb_list-3rd-packages {
  local user="${1:-$(adb_get-current-user)}"
  if [ -z "${ADB_CACHE_USER_PACKAGES[${user}_3rd]}" ]; then
    ADB_CACHE_USER_PACKAGES+=([${user}_3rd]=$(adb_shell "pm list packages -f -3 $(_adb_build_user_arg ${user})"))
    ADB_CACHE_ALL_3RD_PACKAGES=$(echo -e "${ADB_CACHE_ALL_3RD_PACKAGES}\n${ADB_CACHE_USER_PACKAGES[${user}_3rd]}" | sort | uniq)
    ADB_CACHE_ALL_PACKAGES=$(echo -e "${ADB_CACHE_ALL_PACKAGES}\n${ADB_CACHE_USER_PACKAGES[${user}_3rd]}" | sort | uniq)
  fi
  # for android 8.0+, the format is: package:/data/app/org.fdroid.fdroid-nzG6JC-iGBiyPjMYNpFhfw==/base.apk=org.fdroid.fdroid
  # for older version, the format is: package:/data/app/org.fdroid.fdroid-1/base.apk=org.fdroid.fdroid
  local pkg=
  for pkg in $(echo "${ADB_CACHE_USER_PACKAGES[${user}_3rd]}" | awk -F: '{print $2}'); do
    echo "${pkg##*=}"
  done
}

function _adb_usage_list-packages {
  echo "list-packages [user-id] (user-id default: current user)"
}
function adb_list-packages {
  adb_list-3rd-packages "${1}"
  adb_list-sys-packages
}

function _adb_usage_is-sys-package {
  echo "is-sys-package <PKG>"
}
function adb_is-sys-package {
  local pkg="${1}"
  if [ -z "${ADB_CACHE_ALL_SYS_PACKAGES}" ]; then
    _adb_fetch_pkgs_cache
  fi
  adb_list-sys-packages | grep "^${pkg}$" &>/dev/null
}

function _adb_usage_is-3rd-package {
  echo "is-3rd-package <PKG> [user-id] (user-id default: all users)"
}
function adb_is-3rd-package {
  local pkg="${1}"
  local user="${2}"
  if [ -z "${ADB_CACHE_ALL_3RD_PACKAGES}" ]; then
    _adb_fetch_pkgs_cache
  fi
  if [ -z "${user}" ]; then
    if echo "${ADB_CACHE_ALL_3RD_PACKAGES}" | grep "=${pkg}$" &>/dev/null; then
      return 0
    else
      return 1
    fi
  else
    adb_list-3rd-packages "${user}" | grep "^${pkg}$" &>/dev/null
  fi
}

function _adb_usage_is-package-installed {
  echo "is-package-installed <PKG> [user-id] (user-id default: all users)"
}
function adb_is-package-installed {
  local pkg="${1}"
  local user="${2}"
  if [ -z "${ADB_CACHE_ALL_PACKAGES}" ]; then
    _adb_fetch_pkgs_cache
  fi
  if [ -z "${user}" ]; then
    if echo "${ADB_CACHE_ALL_PACKAGES}" | grep "=${pkg}$" &>/dev/null; then
      return 0
    else
      return 1
    fi
  else
    adb_is-sys-package "${pkg}" || adb_is-3rd-package "${pkg}" "${user}"
  fi
}

function _adb_usage_get-package-version-code {
  echo "get-package-version-code <PKG>"
}
function adb_get-package-version-code {
  local pkg="${1}"
  adb_shell "dumpsys package '${pkg}'" | grep versionCode | awk '{print $1}' | awk -F= '{print $2}' | head -n 1
}

function _adb_usage_get-package-version-name {
  echo "get-package-version-name <PKG>"
}
function adb_get-package-version-name {
  local pkg="${1}"
  adb_shell "dumpsys package '${pkg}'" | grep versionName | awk -F= '{print $2}' | head -n 1
}

function _adb_usage_get-package-path {
  echo "get-package-path <PKG>"
}
function adb_get-package-path {
  local pkg="${1}"
  local path=
  if [ -n "${ADB_CACHE_ALL_PACKAGES}" ]; then
    path=$(echo "${ADB_CACHE_ALL_PACKAGES}" | grep "=${pkg}$" | awk -F: '{print $2}')
    path="${path%=*}"
  fi
  if [ -z "${path}" ]; then
    path=$(adb_shell "pm path '${pkg}'" | awk -F: '{print $2}')
  fi
  echo "${path}"
}

function _adb_usage_get-package-display-name {
  echo "get-package-display-name <PKG>"
}
function adb_get-package-display-name {
  if ! adb_is-cmd-exists "aapt"; then
    return
  fi
  local pkg="${1}"
  local path=$(adb_get-package-path "${pkg}")
  local display_name=
  display_name=$(adb_shell "aapt dump badging '${path}'" | grep "application-label:" | sed -e "s/application-label:'//" -e "s/'\$//")
  echo "${display_name}"
}

function _adb_usage_get-package-datadir {
  echo "get-package-datadir <PKG> [user-id] [apilevel] (user-id default: all users)"
}
function adb_get-package-datadir {
  local pkg="${1}"
  local user="${2:-$(adb_get-current-user)}"
  local apilevel="${3:-$(adb_get-android-api-level)}"
  if adb_is-support-multiuser "${apilevel}"; then
    echo "/data/user/${user}/${pkg}"
  else
    echo "/data/data/${pkg}"
  fi
}

function _adb_usage_fix-package-datadir-permission {
  echo "fix-package-datadir-permission <PKG> [user-id] (user-id default: current user)"
}
function adb_fix-package-datadir-permission {
  local pkg="${1}"
  local user="${2:-$(adb_get-current-user)}"
  local pkguid=$(adb_get-package-uid "${pkg}" "${user}")
  local device_datadir=$(adb_get-package-datadir "${pkg}" "${user}")
  adb_rootshell "${adb_opt_busybox} mkdir -p '${device_datadir}'"
  adb_chown-recursive "${pkguid}:${pkguid}" "${device_datadir}"
  adb_rootshell "${adb_opt_busybox} chmod -R 0751 '${device_datadir}'"
  adb_rootshell "${adb_opt_busybox} chmod -R 0771 '${device_datadir}'/*"
  adb_fix-selinux-permission "${device_datadir}"
}

function _adb_usage_chown-recursive {
  echo "chown-recursive <ownership> <path>"
}
function adb_chown-recursive {
  # user 'find' instead of 'chown -R' because chown not support '-R' argument in android
  adb_rootshell "${adb_opt_busybox} find '${2}' -exec chown '${1}' '{}' \;"
}

function _adb_usage_fix-selinux-permission {
  echo "fix-selinux-permission <path>"
}
function adb_fix-selinux-permission {
  if [ $(adb_get-android-api-level) -ge "${ANDROID_VERSION_CODES_M}" ]; then
    adb_rootshell "restorecon -R '${1}'"
  fi
}

function _adb_usage_get-package-uid {
  echo "get-package-uid <PKG> <user-id>"
}
function adb_get-package-uid {
  local pkg="${1}"
  local user="${2}"
  local uid=$(adb_shell "dumpsys package '${pkg}'" | grep -o 'userId=\w\+' | head -n 1 | awk -F= '{print $2}')
  if [ "${user}" -eq 0 ]; then
    echo "${uid}"
  else
    echo "${user}${uid}"
  fi
}

function _adb_usage_start-package {
  echo "start-package <PKG>"
}
function adb_start-package {
  local pkg="${1}"
  adb_shell "monkey -p '${pkg}' -c android.intent.category.LAUNCHER 1"
}

function _adb_usage_kill-package {
  echo "kill-package <PKG> [user-id] (only kill background package, user-id default: all users)"
}
function adb_kill-package {
  local pkg="${1}"
  local user="${2}"
  adb_shell "am kill $(_adb_build_user_arg ${user}) '${pkg}'"
}

function _adb_usage_stop-package {
  echo "stop-package <PKG> [user-id] (user-id default: all users)"
}
function adb_stop-package {
  local pkg="${1}"
  local user="${2}"
  adb_shell "am force-stop $(_adb_build_user_arg ${user}) '${pkg}'"
}

function _adb_usage_clear-package {
  echo "clear-package <PKG> [user-id] (user-id default: all users)"
}
function adb_clear-package {
  local pkg="${1}"
  local user="${2}"
  _adb_shell_with_user_arg "pm clear %s '${pkg}'" "${user}"
}

# arg1: device-path, arg2: host-path, return 0 if is same file
function _adb_is-same-file {
  device_hash=$(adb_shell "${adb_opt_busybox} sha1sum '${1}'" | awk '{print $1}')
  host_hash=$(sha1sum "${2}" | awk '{print $1}')
  if [ x"${device_hash}" = x"${host_hash}" ]; then
    return 0
  else
    return 1
  fi
}

function _adb_usage_is-file-exists {
  echo "is-file-exists <device-path>"
}
function adb_is-file-exists {
  local result=$(adb_autoshell "[ -e '${1}' ] && ${adb_opt_busybox} echo 1")
  if [ -z "${result}" ]; then
    return 1
  else
    return 0
  fi
}

function _adb_usage_is-dir-exists {
  echo "is-dir-exists <device-path>"
}
function adb_is-dir-exists {
  local result=$(adb_autoshell "[ -d '${1}' ] && ${adb_opt_busybox} echo 1")
  if [ -z "${result}" ]; then
    return 1
  else
    return 0
  fi
}

function _adb_usage_get-file-timestamp {
  echo "get-file-timestamp <device-path>"
}
function adb_get-file-timestamp {
  local device_timestamp
  if adb_is-dir-exists "${1}"; then
    device_timestamp=$(adb_autoshell "${adb_opt_busybox} find '${1}' -print0 -maxdepth 2 | ${adb_opt_busybox} xargs -0 ${adb_opt_busybox} stat -c '%Y' | ${adb_opt_busybox} sort -n | ${adb_opt_busybox} tail -n 1")
  elif adb_is-file-exists "${1}"; then
    device_timestamp=$(adb_autoshell "${adb_opt_busybox} stat -c '%Y' '${1}'")
  fi
  if [ -z "${device_timestamp}" ]; then
    device_timestamp=0
  fi
  echo "${device_timestamp}"
}

function _adb_usage_set-file-timestamp {
  echo "set-file-timestamp <device-path> <seconds>"
}
function adb_set-file-timestamp {
  if adb_is-file-exists "${1}" && [ x"${adb_opt_busybox}" != x"toybox" ]; then
    # android default toybox touch command not support modify file time, please special '--busybox' arg
    local datafmt=$(date --utc "+%FT%T.%NZ" -d @${2})
    adb_autoshell "${adb_opt_busybox} touch -c -m -d '${datafmt}' '${fixed_device_path}'"
  fi
}

function _adb_usage_pull {
  echo "pull <device-path> <host-path> [use-tmpdir]"
}
function adb_pull {
  local device_path="${1}"
  local host_path="${2:-.}"
  local use_tmpdir="${3}"
  if ! adb_is-file-exists "${device_path}"; then
    msg2 "adb_pull: ${device_path} not found, ignore"
    return 1
  fi

  local device_is_dir= host_is_dir= host_dir_exists=
  if [[ "${device_path}" =~ /$ ]]; then
    device_is_dir=1
  fi
  if [[ "${host_path}" =~ /$ ]]; then
    host_is_dir=1
  fi
  if [ -d "${host_path}" ]; then
    host_dir_exists=1
  fi

  # ensure host parent dir exists
  host_path=$(get_realpath "${host_path}")
  if [ -n "$(dirname ${host_path})" ]; then
    mkdir -p "$(dirname ${host_path})"
  fi

  # if pull device file to host dir, and host dir not exists, ensure
  # it exists at first, such as "adb_pull device/file host/dir/"
  if [ -z "${device_is_dir}" -a -n "${host_is_dir}" -a -z "${host_dir_exists}" ]; then
    mkdir -p "${host_path}"
  fi

  _fix_root_type
  if [ -z "${use_tmpdir}" ] || [ x"${adb_opt_root_type}" = x"adb-root" ] || [ -z "${adb_opt_root_type}" ]; then
    ${adb_cmd} pull "${device_path}" "${host_path}"
  else
    _adb_ensure_tmpdir_exists
    local tmpdir="${adb_opt_device_tmpdir}/$(basename "${device_path}")"
    adb_autoshell "${adb_opt_busybox} rm -r '${tmpdir}'"
    local fix_device_path="${device_path%/}" # remove tailing slash or 'toybox cp -R' not works correct
    adb_autoshell "${adb_opt_busybox} cp -R -L '${fix_device_path}' '${adb_opt_device_tmpdir}'"
    adb_chown-recursive "shell:shell" "${tmpdir}"
    ${adb_cmd} pull "${tmpdir}" "${host_path}"
    adb_autoshell "${adb_opt_busybox} rm -r '${tmpdir}'"
  fi

  # if pull device dir to host dir, and host dir already exists, just
  # align the host dir , such as "adb_pull device/dir/ host/dir/"
  if [ -n "${host_dir_exists}" -a -n "${device_is_dir}" -a -n "${host_is_dir}" ]; then
    local device_dirname=$(basename "${device_path}")
    mv "${host_path}/${device_dirname}"/* "${host_path}"
    rmdir "${host_path}/${device_dirname}"
  fi
}

function _adb_usage_push {
  echo "push <host-path> <device-path> [use-tmpdir]"
}
function adb_push {
  local host_path="${1}"
  local device_path="${2}"
  local use_tmpdir="${3}"

  local device_is_dir= host_is_dir= device_dir_exists=
  if [[ "${device_path}" =~ /$ ]]; then
    device_is_dir=1
  fi
  if [[ "${host_path}" =~ /$ ]]; then
    host_is_dir=1
  fi
  if adb_is-dir-exists "${device_path}"; then
    device_dir_exists=1
  fi

  host_path=$(get_realpath "${host_path}")
  if ! [ -e "${host_path}" ]; then
    msg2 "adb_push: ${host_path} not found, ignore"
    return 1
  fi

  _fix_root_type

  # ensure device parent dir exists
  if [ -n $(dirname "${device_path}") ]; then
    adb_autoshell "${adb_opt_busybox} mkdir -p '$(dirname ${device_path})'"
  fi

  # if push host file to device dir, and device dir not exists, ensure
  # it exists at first, such as "adb_push host/file device/dir/"
  if [ -z "${host_is_dir}" -a -n "${device_is_dir}" -a -z "${device_dir_exists}" ]; then
    adb_autoshell "${adb_opt_busybox} mkdir -p '${device_path}'"
  fi

  if [ -z "${use_tmpdir}" ] || [ x"${adb_opt_root_type}" = x"adb-root" ] || [ -z "${adb_opt_root_type}" ]; then
    ${adb_cmd} push "${host_path}" "${device_path}"
  else
    _adb_ensure_tmpdir_exists
    local tmpdir="${adb_opt_device_tmpdir}/$(basename "${host_path}")"
    adb_autoshell "${adb_opt_busybox} rm -r '${tmpdir}'"
    ${adb_cmd} push "${host_path}" "${adb_opt_device_tmpdir}"
    adb_autoshell "${adb_opt_busybox} cp -R -L '${tmpdir}' '${device_path}'"
    adb_autoshell "${adb_opt_busybox} rm -r '${tmpdir}'"
  fi

  # if push host dir to device dir, and device dir already exists, just
  # align the device dir , such as "adb_push host/dir/ device/dir/"
  if [ -n "${device_dir_exists}" -a -n "${device_is_dir}" -a -n "${host_is_dir}" ]; then
    local host_dirname=$(basename "${host_path}")
    # TODO: not work if contains non-empty sub-dir
    adb_autoshell "${adb_opt_busybox} mv '${device_path}${host_dirname}'/* '${device_path}'"
    adb_autoshell "${adb_opt_busybox} rmdir '${device_path}${host_dirname}'"
  fi
}

# opts: adb_opt_allow_downgrade
function _adb_usage_install {
  echo "install <host-appfile> [user-id] (user-id default: all users)"
}
function adb_install {
  local host_appfile=$(get_realpath "${1}")
  local user="${2}"
  local extra_arg=
  local result=
  if [ -n "${adb_opt_allow_downgrade}" ]; then
    extra_arg='-d'
  fi
  if [ -z "${user}" ] || ! adb_is-pm-support-multiuser; then
    local cmd="${adb_cmd} install -r ${extra_arg} '${host_appfile}'"
    if [ -n "${adb_opt_verbose}" ]; then
      msg2 "adb_install ${host_appfile}"
      msg2 "adb_install cmd: ${cmd}"
    fi
    result=$(eval "${cmd}")
  else
    if [ -n "${adb_opt_verbose}" ]; then
      msg2 "adb_install ${host_appfile} for user ${user}"
    fi
    # install for special user
    _adb_ensure_tmpdir_exists
    local tmpdir="${adb_opt_device_tmpdir}/$(basename "${host_appfile}")"
    adb_shell "${adb_opt_busybox} mkdir -p '${adb_opt_device_tmpdir}'"
    adb_shell "${adb_opt_busybox} rm -r '${tmpdir}'"
    ${adb_cmd} push "${host_appfile}" "${adb_opt_device_tmpdir}"
    result=$(adb_shell "pm install -r ${extra_arg} --user ${user} '${tmpdir}'")
    adb_shell "${adb_opt_busybox} rm -r '${tmpdir}'"
  fi
  if echo "${result}"| grep -q 'Success'; then
    return 0
  else
    local err=$(echo "${result}" | grep 'Failure')
    msg2 "adb_install failed: ${err}"
    return 1
  fi
}

# opts: adb_opt_keep_data
function _adb_usage_uninstall {
  echo "uninstall <PKG> [user-id] (user-id default: all users)"
}
function adb_uninstall {
  local pkg="${1}"
  local user="${2}"
  local extra_arg=
  if [ -n "${adb_opt_keep_data}" ]; then
    extra_arg='-k'
  fi
  if [ -z "${user}" ] || ! adb_is-pm-support-multiuser; then
    if [ -n "${adb_opt_verbose}" ]; then
      msg2 "adb_uninstall ${pkg}"
    fi
    adb_shell "pm uninstall ${extra_arg} '${pkg}'"
  else
    if [ -n "${adb_opt_verbose}" ]; then
      msg2 "adb_uninstall ${pkg} for user ${user}"
    fi
    adb_shell "pm uninstall ${extra_arg} --user '${user}' '${pkg}'"
  fi
}

function _adb_usage_disable {
  echo "disable <PKG-OR-COMPONENT> [user-id] (user-id default: all users)"
}
function adb_disable {
  local pkg_or_comp="${1}"
  local user="${2}"
  if [ -n "${adb_opt_verbose}" ]; then
    msg2 "adb_disable ${pkg_or_comp}"
  fi
  _adb_rootshell_with_user_arg "pm disable %s '${pkg_or_comp}'" "${user}"
}

function _adb_usage_enable {
  echo "enable <PKG-OR-COMPONENT> [user-id] (user-id default: all users)"
}
function adb_enable {
  local pkg_or_comp="${1}"
  local user="${2}"
  if [ -n "${adb_opt_verbose}" ]; then
    msg2 "adb_enable $(_adb_build_user_arg ${user}) ${pkg_or_comp}"
  fi
  _adb_rootshell_with_user_arg "pm enable %s '${pkg_or_comp}'" "${user}"
}

function _adb_usage_remount-rw {
  echo "remount-rw <path> remount /system or /vendor as read and write"
}
function adb_remount-rw {
  if [[ "${1}" == /vendor/* ]]; then
    adb_rootshell "${adb_opt_busybox} mount -o remount,rw /vendor"
  else
    adb_remount-system-rw
  fi
}

function _adb_usage_remount-ro {
  echo "remount-ro <path> remount /system or /vendor as read and write"
}
function adb_remount-ro {
  if [[ "${1}" == /vendor/* ]]; then
    adb_rootshell "${adb_opt_busybox} mount -o remount,ro /vendor"
  else
    adb_remount-system-ro
  fi
}

function _adb_usage_remount-system-rw {
  echo "remount-system-rw remount /system as read and write"
}
function adb_remount-system-rw {
  if adb_is-mountpoint "/system"; then
    if ! adb_is-mountpoint-rw "/system"; then
      adb_rootshell "${adb_opt_busybox} mount -o remount,rw /system"
      if ! adb_is-mountpoint-rw "/system"; then
        warning "Could not remount /system, please try \`adb root; adb disable-verity; adb remount\` manually and try again"
      fi
    fi
  elif adb_is-mountpoint "/"; then
    adb_rootshell "mount -o remount,rw /"
  fi
  # it seems unnecessary to remount /system, it's not a mount point
}

function _adb_usage_remount-system-ro {
  echo "remount-system-ro remount /system as read only"
}
function adb_remount-system-ro {
  if adb_is-mountpoint "/system"; then
    if ! adb_is-mountpoint-ro "/system"; then
      adb_rootshell "${adb_opt_busybox} mount -o remount,ro /system"
    fi
  elif adb_is-mountpoint "/"; then
    adb_rootshell "mount -o remount,ro /"
  fi
}

function _adb_usage_is-mountpoint {
  echo "is-mountpoint <path>"
}
function adb_is-mountpoint {
  local path="${1}"
  local result=$(adb_shell "${adb_opt_busybox} mountpoint -q ${1} && ${adb_opt_busybox} echo 1")
  if [ -z "${result}" ]; then
    return 1
  else
    return 0
  fi
}

function _adb_usage_is-mountpoint-rw {
  echo "is-mountpoint-rw <path>"
}
function adb_is-mountpoint-rw {
  local path="${1}"
  local mounts=$(adb_shell "cat /proc/mounts")
  echo "${mounts}" | grep -q "\s${path}\s.*\srw[[:space:],]"
}

function _adb_usage_is-mountpoint-ro {
  echo "is-mountpoint-ro <path>"
}
function adb_is-mountpoint-ro {
  local path="${1}"
  local mounts=$(adb_shell "cat /proc/mounts")
  echo "${mounts}" | grep -q "\s${path}\s.*\sro[[:space:],]"
}

function _adb_usage_install-sys {
  echo "install-sys <host-appfile> [device-install-dir]"
}
function adb_install-sys {
  # copy from oandbackup ShellCommands.restoreSystemApk()
  local host_appfile=$(get_realpath "${1}")
  local device_appdir="${2:-/system/app}"
  local filename=$(basename "${host_appfile}")
  local appname="${filename%.apk}"
  appname="${appname%-*}"

  adb_remount-rw "${device_appdir}"

  # locations of apks have been changed in android 5
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_LOLLIPOP}" ]; then
    device_appdir="${device_appdir}/${appname}"
  fi
  adb_rootshell "${adb_opt_busybox} mkdir -p '${device_appdir}'"
  adb_rootshell "${adb_opt_busybox} chmod -R 755 '${device_appdir}'"

  adb_push "${host_appfile}" "${device_appdir}/${filename}" 1
  adb_rootshell "${adb_opt_busybox} chmod 644 '${device_appdir}/${filename}'"

  adb_remount-ro "${device_appdir}"

  msg2 "maybe a reboot is necessary for system app"
}

# opts: adb_opt_keep_data
function _adb_usage_uninstall-sys {
  echo "uninstall-sys <PKG>"
}
function adb_uninstall-sys {
  local pkg="${1}"
  local device_appfile=$(adb_get-package-path "${pkg}")
  if [ -z "${device_appfile}" ]; then
    warning "Could not detect package path: ${pkg}"
    return
  fi

  if [ -z "${adb_opt_keep_data}" ]; then
    adb_clear-package "${pkg}"
  fi
  adb_remount-rw "${device_appfile}"
  adb_rootshell "${adb_opt_busybox} rm '${device_appfile}'"

  # locations of apks have been changed in android 5
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_LOLLIPOP}" ]; then
    device_appfile=$(dirname "${device_appfile}")
  fi
  local device_app_parent=$(dirname "${device_appfile}")
  if [ x"${device_app_parent}" = x"/system/app" -o x"${device_app_parent}" = x"/vendor/app" \
      -o x"${device_app_parent}" = x"/system/priv-app" -o x"${device_app_parent}" = x"/vendor/priv-app" ]; then
    adb_rootshell "${adb_opt_busybox} rm -r '${device_appfile}'"
  fi

  adb_remount-ro "${device_appfile}"

  msg2 "maybe a reboot is necessary for system app"
}

function _adb_usage_make-pkg-installed-for-user {
  echo "make-pkg-installed-for-user <PKG> <user-id>"
}
function adb_make-pkg-installed-for-user {
  adb_shell "pm install -r -d --user '${2}' '$(adb_get-package-path "${1}")'"
}

function _adb_usage_get-default-launcher {
  echo "get-default-launcher [user-id] (user-id default: all users)"
}
function adb_get-default-launcher {
  _adb_shell_with_user_arg "cmd shortcut get-default-launcher %s" "${1}"
}

function _adb_usage_clear-default-launcher {
  echo "clear-default-launcher [user-id] (user-id default: all users)"
}
function adb_clear-default-launcher {
  _adb_shell_with_user_arg "cmd shortcut clear-default-launcher %s" "${1}"
}

function _adb_usage_set-default-launcher {
  echo "set-default-launcher <activity> [user-id] (user-id default: all users)"
}
function adb_set-default-launcher {
  _adb_shell_with_user_arg "pm set-home-activity %s ${1}" "${2}"
}

function _adb_usage_disable-run-background {
  echo "disable-run-background <PKG> [user-id] (user-id default: all users)"
}
function adb_disable-run-background {
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -lt "${ANDROID_VERSION_CODES_N}" ]; then
    warning "adb_diable-run-background only works since Android 7.0 (API level 24)"
    return
  fi
  local pkg="${1}"
  local user="${2}"
  _adb_shell_with_user_arg "cmd appops set %s '${pkg}' RUN_IN_BACKGROUND ignore" "${user}"
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_P}" ]; then
    _adb_shell_with_user_arg "cmd appops set %s '${pkg}' RUN_ANY_IN_BACKGROUND ignore" "${user}"
  fi
}

function _adb_usage_enable-run-background {
  echo "enable-run-background <PKG> [user-id] (user-id default: all users)"
}
function adb_enable-run-background {
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -lt "${ANDROID_VERSION_CODES_N}" ]; then
    warning "adb_enable-run-background only works since Android 7.0 (API level 24)"
    return
  fi
  local pkg="${1}"
  local user="${2}"
  _adb_shell_with_user_arg "cmd appops set %s '${pkg}' RUN_IN_BACKGROUND allow" "${user}"
  if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_P}" ]; then
    _adb_shell_with_user_arg "cmd appops set %s '${pkg}' RUN_ANY_IN_BACKGROUND allow" "${user}"
  fi
}

function _adb_usage_list-autostart-components {
  echo "list-autostart-components [PKG]"
}
function adb_list-autostart-components {
  local pkg="${1}"
  if [ -z "${pkg}" ]; then
    adb_shell "dumpsys package resolvers receiver" | awk '/android.intent.action.BOOT_COMPLETED/{p=1;next}/:/{p=0}p {print $2}'
  else
    adb_shell "dumpsys package resolvers receiver" | awk '/android.intent.action.BOOT_COMPLETED/{p=1;next}/:/{p=0}p {print $2}' | grep "^${pkg}/"
  fi
}

function _adb_usage_disable-autostart {
  echo "disable-autostart <PKG> [user-id] (user-id default: all users)"
}
function adb_disable-autostart {
  local pkg="${1}"
  local user="${2}"
  local comp=
  for comp in $(adb_list-autostart-components "${pkg}"); do
    adb_disable "${comp}" "${user}"
  done
}

function _adb_usage_enable-autostart {
  echo "enable-autostart <PKG> [user-id] (user-id default: all users)"
}
function adb_enable-autostart {
  local pkg="${1}"
  local user="${2}"
  local comp=
  for comp in $(adb_list-autostart-components "${pkg}"); do
    adb_enable "${comp}" "${user}"
  done
}

function _adb_usage_grant-permission {
  echo "grant-permission <PKG> <permission> [user-id] (user-id default: all users)"
}
function adb_grant-permission {
  local pkg="${1}"
  local perm="${2}"
  local user="${3}"

  # add permission prefix
  if ! [[ "${perm}" =~ \. ]]; then
    perm="android.permission.${perm}"
  fi
  _adb_shell_with_user_arg "pm grant %s '${pkg}' ${perm}" "${user}"
}

function _adb_usage_revoke-permission {
  echo "revoke-permission <PKG> <permission> [user-id] (user-id default: all users)"
}
function adb_revoke-permission {
  local pkg="${1}"
  local perm="${2}"
  local user="${3}"

  # add permission prefix
  if ! [[ "${perm}" =~ \. ]]; then
    perm="android.permission.${perm}"
  fi
  _adb_shell_with_user_arg "pm revoke %s '${pkg}' ${perm}" "${user}"
}

function _adb_usage_grant-storage-permission {
  echo "grant-storage-permission <PKG> [user-id] (user-id default: all users)"
}
function adb_grant-storage-permission {
  adb_grant-permission "${1}" "android.permission.READ_EXTERNAL_STORAGE" "${2}"
  adb_grant-permission "${1}" "android.permission.WRITE_EXTERNAL_STORAGE" "${2}"
}

function _adb_usage_revoke-storage-permission {
  echo "revoke-storage-permission <PKG> [user-id] (user-id default: all users)"
}
function adb_revoke-storage-permission {
  adb_revoke-permission "${1}" "android.permission.READ_EXTERNAL_STORAGE" "${2}"
  adb_revoke-permission "${1}" "android.permission.WRITE_EXTERNAL_STORAGE" "${2}"
}

function _adb_usage_grant-location-permission {
  echo "grant-location-permission <PKG> [user-id] (user-id default: all users)"
}
function adb_grant-location-permission {
  adb_grant-permission "${1}" "android.permission.ACCESS_FINE_LOCATION" "${2}"
  adb_grant-permission "${1}" "android.permission.ACCESS_COARSE_LOCATION" "${2}"
}

function _adb_usage_revoke-location-permission {
  echo "revoke-location-permission <PKG> [user-id] (user-id default: all users)"
}
function adb_revoke-location-permission {
  adb_revoke-permission "${1}" "android.permission.ACCESS_FINE_LOCATION" "${2}"
  adb_revoke-permission "${1}" "android.permission.ACCESS_COARSE_LOCATION" "${2}"
}

function _adb_usage_disable-battery-optimization {
  echo "disable-battery-optimization <PKG>"
}
function adb_disable-battery-optimization {
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -lt "${ANDROID_VERSION_CODES_M}" ]; then
    warning "adb_disable-battery-optimization only works since Android 6.0 (API level 23)"
    return
  fi
  adb_shell "dumpsys deviceidle whitelist +${1}"
}

function _adb_usage_enable-battery-optimization {
  echo "enable-battery-optimization <PKG>"
}
function adb_enable-battery-optimization {
  local apilevel=$(adb_get-android-api-level)
  if [ "${apilevel}" -lt "${ANDROID_VERSION_CODES_M}" ]; then
    warning "adb_enable-battery-optimization only works since Android 6.0 (API level 23)"
    return
  fi
  adb_shell "dumpsys deviceidle whitelist -${1}"
}
