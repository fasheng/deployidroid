# Depends: utils.sh, adb_utils.sh

declare -A backup_depends=(
  [basename]=''
  [dirname]=''
  [awk]=''
  [tar]=''
  [gzip]=''
  [find]=''
)
declare -A backup_depends_optional=(
)

# Constant variables
BACKUP_DEFAULT_PROFILE_NAME='default'
BACKUP_DEFAULT_APPDIR='./app'
BACKUP_DEFAULT_DATADIR='./data'
BACKUP_DEFAULT_TYPE='app+data'
BACKUP_DEFAULT_ARCHIVE_SIDE='auto'
BACKUP_DEFAULT_ARCHIVE_TYPE='tar.gz'

# Configurable variables, could override in host script or config file
backup_verbose=
backup_profile_name="${BACKUP_DEFAULT_PROFILE_NAME}"
backup_appdir="${BACKUP_DEFAULT_APPDIR}"
backup_datadir="${BACKUP_DEFAULT_DATADIR}"
backup_type="${BACKUP_DEFAULT_TYPE}"
backup_archive_side="${BACKUP_DEFAULT_ARCHIVE_SIDE}"
backup_archive_type="${BACKUP_DEFAULT_ARCHIVE_TYPE}"

declare -a backup_freq_apps=(
  "com.android.providers.contacts"
  "com.android.providers.settings"
  "com.android.providers.telephony"
  "com.android.messaging"
  "com.android.settings"
  "com.cyanogenmod.trebuchet"
  "org.cyanogenmod.cmsettings"
  "org.lineageos.lineagesettings"
  "org.lineageos.trebuchet"
)

declare -a backup_spec_apps=(
  "@bluetooth"
  "@datausage"
  "@wifi"
  "@apatch"
  "@kernelsu"
  "@user.accounts"
  "@user.appwidgets"
  "@user.icon"
  "@user.wallpaper"
)

backup_appinfo_name="appinfo.sh"
backup_appinfo_name_fallback="pkginfo.sh"

###*** Backup help functions

# arg1: appline, arg2: default user

# for example: 'user_0:com.android.providers.contacts:type=app+data;version_code=123' -> appuser=0, appid=com.android.providers.contacts, appline_options[type]=app+data;appline_options[version_code]=123
function backup_parse_appline {
  local appline="${1}"
  local default_user="${2}"
  local options_str=
  appuser= appid= appline_options=()
  appid=$(echo "${appline}" | awk -F: '{print $1}')
  if [[ "${appid}" =~ ^user_[^.]*$ ]]; then
    # user prefix exists
    appuser="${appid#user_}"
    appid=$(echo "${appline}" | awk -F: '{print $2}')
    options_str=$(echo "${appline}" | awk -F: '{print $3}')
  else
    appuser="${default_user}"
    options_str=$(echo "${appline}" | awk -F: '{print $2}')
  fi
  if [ -n "${options_str}" -o -n "${backup_arg_options}" ]; then
    local type= repo= version_code= install_path= allow_unstable= appdir= datadir= datauser= appfile= datafile= archive_type=
    if [ -n "${options_str}" ]; then
      safe_eval "${options_str}"
    fi
    if [ -n "${backup_arg_options}" ]; then
      safe_eval "${backup_arg_options}"
    fi
    if [ -n "${type}" ]; then appline_options[type]="${type}"; fi
    if [ -n "${repo}" ]; then appline_options[repo]="${repo}"; fi
    if [ -n "${version_code}" ]; then appline_options[version_code]="${version_code}"; fi
    if [ -n "${install_path}" ]; then appline_options[install_path]="${install_path}"; fi
    if [ -n "${allow_unstable}" ]; then appline_options[allow_unstable]="${allow_unstable}"; fi
    if [ -n "${appdir}" ]; then appline_options[appdir]="${appdir}"; fi
    if [ -n "${datadir}" ]; then appline_options[datadir]="${datadir}"; fi
    if [ -n "${appfile}" ]; then appline_options[appfile]="${appfile}"; fi
    if [ -n "${datafile}" ]; then appline_options[datafile]="${datafile}"; fi
    if [ -n "${datauser}" ]; then appline_options[datauser]="${datauser}"; fi
    if [ -n "${archive_type}" ]; then appline_options[archive_type]="${archive_type#.}"; fi
  fi
  if [ -z "${appline_options[type]}" ]; then
    if backup_is_spec_app "${appid}"; then
      appline_options[type]="data"
    elif [ "${appuser}" = 'system' ]; then
      appline_options[type]="app"
    elif backup_is_app_in_system_user "${appid}"; then
      appline_options[type]="data"
    else
      appline_options[type]="${backup_type}"
    fi
  fi
}

# arg1: app line, arg2: user
# for example: 'com.android.providers.contacts:app+data:version_code=123' -> appid='com.android.providers.contacts', type='app+data', appline_options='version_code=123'
function _backup_parse_appline_old {
  local pkgline="${1}"
  local user="${2}"
  appid= type= appline_options=
  appid=$(echo "${pkgline}" | awk -F: '{print $1}')
  type=$(echo "${pkgline}" | awk -F: '{print $2}')
  appline_options=$(echo "${pkgline}" | awk -F: '{print $3}')
  if [ -z "${type}" ]; then
    if backup_is_spec_app "${appid}"; then
      type="data"
    elif [ "${user}" = 'system' ]; then
      type="app"
    elif backup_is_app_in_system_user "${appid}"; then
      type="data"
    else
      type="${backup_type}"
    fi
  fi
  if [ -n "${backup_arg_type}" ]; then
    type="${backup_arg_type}"
  fi
}

# arg1: app id
function backup_is_app_in_system_user {
  local check_appid="${1}"
  local appid= appline=
  for appline in "${system_apps[@]}"; do
    if [[ "${appline}" =~ ^user_[^.]*: ]]; then
      appid="${appline#*:}"
      appid="${appid%:*}"
    else
      appid="${appline%:*}"
    fi
    if [ "${check_appid}" = "${appid}" ]; then
      return
    fi
  done
  return 1
}

# arg1: app id
function backup_get_comment {
  local display_name=
  if adb_is-online; then
    display_name=$(adb_get-package-display-name "${appid}")
  fi
  local comment=
  if [ -n "${display_name}" ]; then
    comment="  # ${display_name}"
  fi
  echo "${comment}"
}

# arg1: backup_app, arg2: backup_data
function backup_get_backup_type {
  local backup_app="${1}"
  local backup_data="${2}"
  if [ -n "${backup_app}" -a -n "${backup_data}" ]; then
    echo "app+data"
  elif [ -n "${backup_app}" ]; then
    echo "app"
  elif [ -n "${backup_data}" ]; then
    echo "data"
  fi
}

# arg1: app id
function backup_is_spec_app {
  if [[ "${1}" == @* ]]; then
    return 0
  else
    return 1
  fi
}

# arg1: app id
function backup_is_spec_app_support_multiuser {
  if [[ "${1}" == @user.* ]]; then
    return 0
  else
    return 1
  fi
}

function backup_fix_archive_side {
  local old="${backup_archive_side}"
  if [ "${backup_archive_side}" = "auto" ]; then
    if backup_is_device_archive_cmd_exists; then
      backup_archive_side="device"
    else
      backup_archive_side="host"
    fi
  fi
  if [ "${old}" != "${backup_archive_side}" ]; then
    msg2 "fix archive side: ${old} -> ${backup_archive_side}"
  fi
  if [ "${backup_archive_side}" = "device" ] && ! backup_is_device_archive_cmd_exists; then
    abort "User special --archive-side to device, but there is no tar and gzip commands in it, please install busybox and retry"
  fi
}
function backup_is_device_archive_cmd_exists {
  if adb_is-cmd-exists "tar" && adb_is-cmd-exists "gzip"; then
    return 0
  else
    return 1
  fi
}
function backup_prepare_args {
  if [ -n "${backup_arg_profile}" ]; then
    if [ ! -f "${backup_arg_profile}" ]; then
      abort "Profile file not found: ${backup_arg_profile}"
    fi
    local profile_path=$(get_realpath "${backup_arg_profile}")
    profile_dir=$(dirname "${backup_arg_profile}")
    source "${backup_arg_profile}"

    # run 'all' hook as soon as profile is loaded
    run_cmd_hook "all" "pre"
    run_cmd_hook "all"

    # check profile version
    if [ -z "${backup_arg_ignore_check_profile}" ] && [ "${arg_cmd}" != "check" ] && compver_lt "${version}" "${profile_version}"; then
      abort "Profile format not support: please use 'check --fix' to upgrade version to ${profile_version} or use '--ignore-check-profile' to ignore format checking"
    fi

    # check profile compatibility
    [ "${#support_modes[@]}" -gt 0 ] && adb_assert-mode "${support_modes[@]}"
    [ "${#support_arches[@]}" -gt 0 ] && adb_assert-arch "${support_arches[@]}"
    [ "${#support_devices[@]}" -gt 0 ] && adb_assert-device "${support_devices[@]}"

    # fallback variables to default if empty
    name="${name:-${backup_profile_name}}"
    if [ -z "${appdir}" ]; then
      appdir="${backup_appdir}"
    fi
    if [ -z "${datadir}" ]; then
      datadir="${backup_datadir}/${name}"
    fi

    # fix relative path
    if [ "${PWD}" != "${profile_dir}" ]; then
      appdir=$(get_realpath "${appdir}" "${profile_dir}")
      datadir=$(get_realpath "${datadir}" "${profile_dir}")
    fi

  else
    backup_build_default_profile "${@}"
  fi

  # override variables that defined in argument
  name="${backup_arg_name:-${name}}"
  if [ -n "${backup_arg_appdir}" ]; then
    appdir=$(get_realpath "${backup_arg_appdir}")
  fi
  if [ -n "${backup_arg_datadir}" ]; then
    datadir=$(get_realpath "${backup_arg_datadir}/${name}")
  fi

  # add user prefix for system_apps
  local i= appline=
  for i in "${!system_apps[@]}"; do
    appline="${system_apps[i]}"
    if [[ "${appline}" =~ ^user_[^.]*: ]]; then
      system_apps[i]="user_system:${appline#user_*:}"
    else
      system_apps[i]="user_system:${appline}"
    fi
    if ! contains "system" "${users[@]}"; then
      users=("system" "${users[@]}")
    fi
  done

  # expand user aliases: cur, current, sys, all
  for i in "${!user_apps[@]}"; do
    appline="${user_apps[i]}"
    if [[ "${appline}" =~ ^user_[^.]*: ]]; then
      local appuser="${appline%%:*}"
      appuser="${appuser#user_}"
      if [ "${appuser}" = 'cur' -o "${appuser}" = 'current' ]; then
        local cur=$(adb_get-current-user)
        user_apps[i]="user_${cur}:${appline#user_*:}"
        if ! contains "${cur}" "${users[@]}"; then
          users+=("${cur}")
        fi
      elif [ "${appuser}" = 'all' ]; then
        user_apps[i]="user_0:${appline#user_*:}"
        if ! contains "0" "${users[@]}"; then
          users+=("0")
        fi
        local u=
        for u in $(adb_list-users | awk '{print $1}'); do
          if [ "${u}" != "0" ]; then
            user_apps+=("user_${u}:${appline#user_*:}")
            if ! contains "${u}" "${users[@]}"; then
              users+=("${u}")
            fi
          fi
        done
      elif ! contains "${appuser}" "${users[@]}"; then
        users+=("${appuser}")
      fi
    else
      # no user special, expand to user 0
      user_apps[i]="user_0:${appline}"
      if ! contains "0" "${users[@]}"; then
        users+=("0")
      fi
    fi
  done

  # filter applines
  remove_array_unmatch system_apps "${backup_arg_filter}" "ignore appline by filter ${backup_arg_filter}: %s"
  remove_array_unmatch user_apps   "${backup_arg_filter}" "ignore appline by filter ${backup_arg_filter}: %s"

  msg2 "profile dir: ${profile_dir}"
  msg2 "profile name: ${name}"
  msg2 "app dir: ${appdir}"
  msg2 "data dir: ${datadir}"
  msg2 "user ids: ${users[*]}"
}
function backup_build_default_profile {
  if [ ${#} -eq 0 ]; then
    abort "Must special --profile or BACKUPAPPLINE|${backup_appinfo_name}.."
  fi

  profile_dir=$(get_realpath ".")
  name="${backup_profile_name}"
  appdir=$(get_realpath "${backup_appdir}")
  local user_datadir=
  local appline=
  for appline in "${@}"; do
    if [[ "${appline}" =~ /"${backup_appinfo_name}"$ ]]; then
      # appline is a appinfo.sh file
      if [ ! -f "${appline}" ]; then
        abort "${backup_appinfo_name} file not found: ${appline}"
      fi

      # fix appline name, profile dir and profile name
      local appinfo_file=$(get_realpath "${appline}")
      local appinfo_dir=$(dirname "${appinfo_file}")
      appline=$(basename "${appinfo_dir}")
      user_datadir=$(dirname "${appinfo_dir}")
      appline="$(basename "${user_datadir}"):${appline}"
      if [ -z "${datadir}" ]; then
        datadir=$(dirname "${user_datadir}")
      elif [ x${datadir} != x$(dirname "${user_datadir}") ]; then
        abort "Could not detect the correct datadir, there are multiple ${backup_appinfo_name} but with different datadir"
      fi
      profile_dir=$(dirname "${datadir}")
      name=$(basename "${datadir}")
    elif [ -z "${datadir}" ]; then
      datadir=$(get_realpath "${backup_datadir}/${name}")
    fi

    # parse appline
    local appuser= appid=; unset appline_options; local -A appline_options
    backup_parse_appline "${appline}"
    if backup_is_spec_app "${appid}"; then
      if backup_is_spec_app_support_multiuser "${appid}"; then
        eval "user_apps+=('${appline}')"
      else
        eval "system_apps+=('${appline}')"
      fi
    else
      if [ "${appuser}" = 'sys' -o "${appuser}" = 'system' ]; then
        eval "system_apps+=('${appline}')"
      else
        eval "user_apps+=('${appline}')"
      fi
    fi
  done

  if [ -z "${profile_dir}" -o -z "${name}" ]; then
    abort "Could not detect profile dir(${profile_dir}) or profile name(${name}), a correct dir tree is: profildir/profilename/user_x/com.app.name/${backup_appinfo_name}"
  fi
}

# arg1: <appid>, arg2: <user-id> arg3: [apilevel]
function backup_get_backup_files {
  local appid="${1}"
  local user="${2}"

  if ! backup_is_spec_app "${appid}"; then
    # data file only exists for normal users
    if [ "${user}" != "system" ]; then
      device_backup_files+=($(adb_get-package-datadir "${appid}" "${user}"))
      device_backup_files+=($(adb_get-package-datadir-de "${appid}" "${user}"))
    fi
    return
  fi

  local apilevel="${3:-$(adb_get-android-api-level)}"
  case "${appid}" in
    "@user.icon")
      if adb_is-support-multiuser "${apilevel}"; then
        device_backup_files+=("/data/system/users/${user}/photo.png")
      fi
      ;;
    "@user.accounts")
      if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_N}" ]; then
        device_backup_files+=("/data/system_ce/${user}/accounts_ce.db")
        device_backup_files+=("/data/system_de/${user}/accounts_de.db")
      elif adb_is-support-multiuser "${apilevel}"; then
        device_backup_files+=("/data/system/users/${user}/accounts.db")
      else
        device_backup_files+=("/data/system/accounts.db")
      fi
      ;;
    "@user.appwidgets")
      if adb_is-support-multiuser "${apilevel}"; then
        device_backup_files+=("/data/system/users/${user}/appwidgets.xml")
      else
        device_backup_files+=("/data/system/appwidgets.xml")
      fi
      ;;
    "@user.wallpaper")
      if adb_is-support-multiuser "${apilevel}"; then
        device_backup_files+=("/data/system/users/${user}/wallpaper")
        device_backup_files+=("/data/system/users/${user}/wallpaper_info.xml")
      else
        device_backup_files+=("/data/system/wallpaper")
        device_backup_files+=("/data/system/wallpaper_info.xml")
      fi
      ;;
    "@datausage")
      if adb_is-support-multiuser "${apilevel}"; then
        device_backup_files+=("/data/system/netpolicy.xml")
        device_backup_files+=("/data/system/netstats")
      fi
      ;;
    "@bluetooth")
      if adb_is-support-multiuser "${apilevel}"; then
        device_backup_files+=("/data/misc/bluedroid")
      else
        device_backup_files+=("/data/misc/bluetooth")
        device_backup_files+=("/data/misc/bluetoothd")
      fi
      ;;
    "@wifi")
      if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_O}" ]; then
        device_backup_files+=("/data/misc/wifi/WifiConfigStore.xml" "/data/misc/apexdata/com.android.wifi/WifiConfigStore.xml" "/data/misc/wifi/wpa_supplicant.conf")
      else
        device_backup_files+=("/data/misc/wifi/wpa_supplicant.conf")
      fi
      ;;
    "@keystore")
      # NOTE: keystore could not be backed up
      if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_ICE_CREAM_SANDWICH}" ]; then
        device_backup_files+=("/data/misc/keystore" "/data/misc/systemkeys" "/data/system/recoverablekeystore.db")
      fi
      ;;
    "@fbekey")
      if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_N}" ]; then
        device_backup_files+=("/data/unencrypted" "/data/misc/vold/user_keys")
      fi
      ;;
    "@apatch")
      device_backup_files+=("/data/adb/ap/package_config")
      ;;
    "@kernelsu")
      device_backup_files+=("/data/adb/ksu/.allowlist")
      ;;
  esac
}

# arg1: <appid>, arg2: <backup-apilevel>, arg3: <current-apilevel>, arg4: <user>, arg5: <path>
function backup_fix_restore_path_for_compatibility {
  local appid="${1}"
  local backup_apilevel="${2}"
  local current_apilevel="${3}"
  local user="${4}"
  local path="${5}"
  local newpath="${path}"
  case "${path}" in
    "/data/system/accounts.db" |\
    "/data/system/users/${user}/accounts.db" |\
    "/data/system_ce/${user}/accounts_ce.db")
      if [ "${current_apilevel}" -ge "${ANDROID_VERSION_CODES_N}" ]; then
        newpath="/data/system_ce/${user}/accounts_ce.db"
      elif adb_is-support-multiuser "${current_apilevel}"; then
        newpath="/data/system/users/${user}/accounts.db"
      else
        newpath="/data/system/accounts.db"
      fi
      ;;
  esac
  echo "${newpath}"
}

# arg1: <app-id>, arg2: <backup-apilevel>, arg3: <current-apilevel>
function backup_check_app_data_compatibility {
  local appid="${1}"
  local backup_apilevel="${2}"
  local current_apilevel="${3}"

  case "${appid}" in
    "@user.accounts")
      if [ "${backup_apilevel}" -lt "${ANDROID_VERSION_CODES_N}" ] &&
         [ "${current_apilevel}" -ge "${ANDROID_VERSION_CODES_N}" ]; then
        return 1
      fi
      if [ "${backup_apilevel}" -ge "${ANDROID_VERSION_CODES_N}" ] &&
         [ "${current_apilevel}" -lt "${ANDROID_VERSION_CODES_N}" ]; then
        return 1
      fi
      ;;
    "@bluetooth")
      if adb_is-support-multiuser "${backup_apilevel}" && ! adb_is-support-multiuser "${current_apilevel}"; then
        return 1
      fi
      if adb_is-support-multiuser "${current_apilevel}" && ! adb_is-support-multiuser "${backup_apilevel}"; then
        return 1
      fi
      ;;
    com.android.*)
      if [ "${current_apilevel}" -lt "${backup_apilevel}" ]; then
        return 1
      fi
      ;;
  esac

  return 0
}

# arg1: <appid>, arg2: [user-id], arg3: [apilevel]
function backup_adb_fix-spec-datafile-permission {
  local appid="${1}"
  local user="${2:-$(adb_get-current-user)}"
  local apilevel="${3:-$(adb_get-android-api-level)}"
  local device_backup_files=()
  backup_get_backup_files "${appid}" "${user}" "${apilevel}"
  local f
  for f in "${device_backup_files[@]}"; do
    if ! adb_is-file-exists "${f}"; then
      msg2 "ignore fix permission: ${f} not found"
      continue
    fi
    case "${f}" in
      "/data/misc/wifi/WifiConfigStore.xml" |\
      "/data/misc/apexdata/com.android.wifi/WifiConfigStore.xml" |\
      "/data/system/users/${user}/photo.png" |\
      "/data/system_ce/${user}/accounts_ce.db" |\
      "/data/system/users/${user}/accounts.db" |\
      "/data/system/accounts.db" |\
      "/data/system/users/${user}/appwidgets.xml" |\
      "/data/system/appwidgets.xml" |\
      "/data/system/users/${user}/wallpaper" |\
      "/data/system/users/${user}/wallpaper_info.xml" |\
      "/data/system/wallpaper" |\
      "/data/system/wallpaper_info.xml" |\
      "/data/system/netpolicy.xml" |\
      "/data/system/netstats")
        adb_chown-recursive 'system:system' "${f}"
        adb_rootshell "${adb_busybox} chmod -R 0600 '${f}'"
        ;;
      "/data/system_de/${user}/accounts_de.db" |\
      "/data/system/recoverablekeystore.db")
        adb_chown-recursive 'system:system' "${f}"
        adb_rootshell "${adb_busybox} chmod -R 0660 '${f}'"
        ;;
      "/data/misc/bluedroid" | \
      "/data/misc/bluetooth" | \
      "/data/misc/bluetoothd")
        adb_chown-recursive 'bluetooth:bluetooth' "${f}"
        adb_rootshell "${adb_busybox} chmod -R 0771 '${f}'"
        ;;
      "/data/misc/wifi/wpa_supplicant.conf")
        adb_rootshell "${adb_busybox} chown wifi:wifi '${f}'"
        adb_rootshell "${adb_busybox} chmod 0660 '${f}'"
        ;;
      "/data/misc/keystore")
        adb_chown-recursive 'keystore:keystore' "${f}"
        adb_rootshell "${adb_busybox} chmod -R 0700 '${f}'"
        ;;
      "/data/misc/systemkeys")
        adb_chown-recursive 'system:system' "${f}"
        adb_rootshell "${adb_busybox} chmod -R 0700 '${f}'"
        ;;
      "/data/unencrypted" |\
      "/data/misc/vold/user_keys" |\
      "/data/adb/ap/package_config" |\
      "/data/adb/ksu/.allowlist")
        adb_chown-recursive 'root:root' "${f}"
        adb_rootshell "${adb_busybox} chmod -R 0700 '${f}'"
        ;;
    esac
    adb_fix-selinux-permission "${f}"
  done
}

function _backup_convert_appinfo_vars {
  appinfo_options[id]="${id}"
  appinfo_options[version_name]="${version_name}"
  appinfo_options[version_code]="${version_code}"
  appinfo_options[android_api_level]="${android_api_level}"
  appinfo_options[is_system]="${is_system}"
  appinfo_options[is_encrypted]="${is_encrypted}"
  appinfo_options[install_path]="${install_path}"
  appinfo_options[archive_type]="${archive_type}"
  appinfo_options[data_timestamp]="${data_timestamp}"
}

# arg1: appinfo-file
function backup_generate_appinfo_file {
  echo "id='${appinfo_options[id]}'
version_name='${appinfo_options[version_name]}'
version_code='${appinfo_options[version_code]}'
android_api_level='${appinfo_options[android_api_level]}'
is_system='${appinfo_options[is_system]}'
is_encrypted='${appinfo_options[is_encrypted]}'
install_path='${appinfo_options[install_path]}'
archive_type='${appinfo_options[archive_type]}'
data_timestamp='${appinfo_options[data_timestamp]}'" > "${1}"
}

# arg1: appinfo-file
function backup_parse_appinfo_file {
  local appinfo_file="${1}"
  local id= version_name= version_code= android_api_level= is_system= is_encrypted= install_path= archive_type= data_timestamp=
  appinfo_options=()
  safe_source "${appinfo_file}"
  if [ -n "${id}" ]; then appinfo_options[id]="${id}"; fi
  if [ -n "${version_name}" ]; then appinfo_options[version_name]="${version_name}"; fi
  if [ -n "${version_code}" ]; then appinfo_options[version_code]="${version_code}"; fi
  if [ -n "${android_api_level}" ]; then appinfo_options[android_api_level]="${android_api_level}"; fi
  if [ -n "${is_system}" ]; then appinfo_options[is_system]="${is_system}"; fi
  if [ -n "${is_encrypted}" ]; then appinfo_options[is_encrypted]="${is_encrypted}"; fi
  if [ -n "${install_path}" ]; then appinfo_options[install_path]="${install_path}"; fi
  if [ -n "${data_timestamp}" ]; then appinfo_options[data_timestamp]="${data_timestamp}"; fi
  if [ -n "${archive_type}" ]; then
    appinfo_options[archive_type]="${archive_type}"
  else
    appinfo_options[archive_type]="${backup_archive_type}"
  fi
}

# arg1: appinfo-file, arg2: new-varname, arg3: old-varname
function backup_need_fallback_var {
  local file="${1}"
  local new_varname="${2}"
  local old_varname="${3}"
  local old_var= new_var=
  local -n old_var="${old_varname}"
  local -n new_var="${new_varname}"
  if [ -z "${new_var}" -a -n "${old_var}" ]; then
    msg2 "old style: variable \$${old_varname} not found, fallback to \$${new_varname} in ${file}"
    return
  fi
  return 1
}

# arg1: new-file-varname, arg2: old-file-varname
function backup_need_fallback_file {
  local new_varname="${1}"
  local old_varname="${2}"
  local old_var= new_var=
  local -n old_var="${old_varname}"
  local -n new_var="${new_varname}"
  if [ ! -f "${new_var}" -a -f "${old_var}" ]; then
    msg2 "old style: file ${new_var} not found, fallback to ${old_var}"
    return
  fi
  return 1
}

###*** Backup commands

function _cmd_usage_new {
  echo "new [--profile-type type] [--3rd-app] [--3rd-data] [--sys-app] [--sys-data] [--freq-app] [--freq-data] [--spec-data] [--name name] [-u|--user id] [--appdir dir] [--datadir dir] [--device-tmpdir dir] [--profile file.sh]
    create new template profile for current device
    --profile-type, append different template code for different types, could set multiple times
      the value could be 'backup', 'restore', 'deploy', 'cleanup', 'disable' and 'enable' [default: 'backup' 'restore' 'deploy' 'cleanup' 'disable' 'enable']
    --3rd-app, backup 3rd application
    --3rd-data, backup 3rd application data
    --sys-app, backup system application
    --sys-data, backup system application data
    --freq-app, backup frequently used application which defined in \$backup_freq_apps
      such as contacts, call logs
      (conflict with --sys-app and --sys-data)
    --freq-data, backup frequently used application data
    --spec-data, backup special data which defined in \$backup_spec_apps
      such as user accounts, saved wifi access points"
}
function cmd_new {
  msg "Create new profile ${backup_arg_profile}"
  if [ -n "${backup_arg_profile}" ]; then
    do_cmd_new > "${backup_arg_profile}"
  else
    do_cmd_new
  fi
}
function do_cmd_new {
  # fix args
  if [ ${#backup_arg_profile_types[@]} -eq 0 ]; then
    backup_arg_profile_types+=(backup restore deploy cleanup disable enable)
  fi
  if [ -n "${backup_arg_sys_app}" -a -n "${backup_arg_freq_app}" ]; then
    backup_arg_freq_app=
    msg2 "ignore --freq-app for --sys-app exists" 1>&2
  fi
  if [ -n "${backup_arg_sys_data}" -a -n "${backup_arg_freq_data}" ]; then
    backup_arg_freq_data=
    msg2 "ignore --freq-data for --sys-data exists" 1>&2
  fi

  local content=
  local header_block= user_block= system_apps_block= user_apps_block=
  local backup_block= restore_block= deploy_block= cleanup_block=

  # header
  local name="${backup_arg_name:-${backup_profile_name}}"
  header_block="version='${profile_version}'
name='${name}'"
  header_block="${header_block}
appdir='${backup_appdir}'
datadir='${backup_datadir}/${name}'"
  if adb_is-online; then
    header_block="${header_block}
android_api_level=$(adb_get-android-api-level)  # android $(adb_get-android-version)"
  fi

  # user block
  local user_block= system_apps_block= user_apps_block=
  local appid=  options_str=
  local users=() user= users_str=
  if [ -n "${backup_arg_spec_data}" -o -n "${backup_arg_sys_app}" -o -n "${backup_arg_freq_app}" ]; then
    users_str="system"
  fi
  if adb_is-online; then
    users_str="${users_str} $(adb_list-users | awk '{print $1}')"
  else
    users_str="${users_str} 0"
  fi
  for user in ${users_str}; do
    if [ ${#backup_arg_users[@]} -gt 0 ]; then
      if contains "${user}" "${backup_arg_users[@]}"; then
        users+=("${user}")
      fi
    else
      users+=("${user}")
    fi
  done
  user_block="## user info\n# users=(${users[@]})"
  if adb_is-online; then
    for user in "${users[@]}"; do
      if [ "${user}" != "system" ]; then
        user_block="${user_block}\n# user_${user}_name='$(adb_get-user-name ${user})'"
      fi
    done
  fi

  # for system user
  if contains "system" "${users[@]}"; then
    system_apps_block="system_apps+=(\n"

    # sys apps
    if [ -n "${backup_arg_sys_app}" ]; then
      if adb_is-online; then
        system_apps_block="${system_apps_block}  # sys apps\n"
        for appid in $(adb_list-sys-packages ${user} | sort); do
          options_str="type=app"
          if contains "deploy" "${backup_arg_profile_types[@]}" || contains "cleanup" "${backup_arg_profile_types[@]}"; then
            options_str="${options_str};version_code=$(adb_get-package-version-code ${appid})"
          fi
          system_apps_block="${system_apps_block}  '${appid}:${options_str}'$(backup_get_comment ${appid})\n"
        done
      else
        msg2 "ignore --sys-app for adb offline" 1>&2
      fi
    fi

    # freq apps
    if [ -n "${backup_arg_freq_app}" ]; then
      system_apps_block="${system_apps_block}  # freq apps\n"
      for appid in "${backup_freq_apps[@]}"; do
        if ! adb_is-online || adb_is-sys-package "${appid}"; then
          options_str="type=app"
          if adb_is-online; then
            if contains "deploy" "${backup_arg_profile_types[@]}" || contains "cleanup" "${backup_arg_profile_types[@]}"; then
              options_str="${options_str};version_code=$(adb_get-package-version-code ${appid})"
            fi
          fi
          system_apps_block="${system_apps_block}  '${appid}:${options_str}'$(backup_get_comment ${appid})\n"
        fi
      done
    fi

    # spec system data
    if [ -n "${backup_arg_spec_data}" ]; then
      system_apps_block="${system_apps_block}  # spec system data\n"
      local appid=
      for appid in "${backup_spec_apps[@]}"; do
        if ! backup_is_spec_app_support_multiuser "${appid}"; then
          system_apps_block="${system_apps_block}  '${appid}'\n"
        fi
      done
    fi

    system_apps_block="${system_apps_block})"
  fi

  # for normal user
  user_apps_block="user_apps+=(\n"
  for user in "${users[@]}"; do
    if [ "${user}" = "system" ] ; then
      continue
    fi
    local user_prefix= user_comment=
    if [ "${user}" != "0" ]; then
      user_prefix="user_${user}:"
    fi
    if adb_is-online && ! adb_is-only-one-user; then
      user_comment=" for user ${user}"
    fi

    # sys apps data
    if [ -n "${backup_arg_sys_data}" ]; then
      if adb_is-online; then
        user_apps_block="${user_apps_block}  # sys apps data${user_comment}\n"
        for appid in $(adb_list-sys-packages ${user} | sort); do
          # for com.android.providers applications, the data files only
          # exists in the default user
          if [[ "${appid}" =~ ^com.android.providers ]]; then
            if [ "${user}" = "0" ]; then
              user_apps_block="${user_apps_block}  '${user_prefix}${appid}:type=data'$(backup_get_comment ${appid})\n"
            fi
          else
            user_apps_block="${user_apps_block}  '${user_prefix}${appid}:type=data'$(backup_get_comment ${appid})\n"
          fi
        done
      else
        msg2 "ignore --sys-data for adb offline" 1>&2
      fi
    fi

    # freq apps and data
    if [ -n "${backup_arg_freq_app}" -o -n "${backup_arg_freq_data}" ]; then
      user_apps_block="${user_apps_block}  # freq apps${user_comment}\n"
      for appid in "${backup_freq_apps[@]}"; do
        if adb_is-online && adb_is-3rd-package "${appid}"; then
          local backup_type=$(backup_get_backup_type "${backup_arg_freq_app}" "${backup_arg_freq_data}")
          options_str="type=${backup_type}"
          if [[ "${backup_type}" =~ "app" ]]; then
            if contains "deploy" "${backup_arg_profile_types[@]}" || contains "cleanup" "${backup_arg_profile_types[@]}"; then
              options_str="${options_str};version_code=$(adb_get-package-version-code ${appid})"
            fi
          fi
          user_apps_block="${user_apps_block}  '${user_prefix}${appid}:${options_str}'$(backup_get_comment ${appid})\n"
        elif ! adb_is-online || adb_is-sys-package "${appid}" && [ -n "${backup_arg_freq_data}" ];then
          # for com.android.providers applications, the data files only
          # exists in the default user
          if [[ "${appid}" =~ ^com.android.providers ]]; then
            if [ "${user}" = "0" ]; then
              user_apps_block="${user_apps_block}  '${user_prefix}${appid}:type=data'$(backup_get_comment ${appid})\n"
            fi
          else
            user_apps_block="${user_apps_block}  '${user_prefix}${appid}:type=data'$(backup_get_comment ${appid})\n"
          fi
        fi
      done
    fi

    # 3rd apps and data
    if [ -n "${backup_arg_3rd_app}" -o -n "${backup_arg_3rd_data}" ]; then
      if adb_is-online; then
        user_apps_block="${user_apps_block}  # 3rd apps${user_comment}\n"
        for appid in $(adb_list-3rd-packages ${user} | sort); do
          local backup_type=$(backup_get_backup_type "${backup_arg_3rd_app}" "${backup_arg_3rd_data}")
          options_str="type=${backup_type}"
          if [[ "${backup_type}" =~ "app" ]]; then
            if contains "deploy" "${backup_arg_profile_types[@]}" || contains "cleanup" "${backup_arg_profile_types[@]}"; then
              options_str="${options_str};version_code=$(adb_get-package-version-code ${appid})"
            fi
          fi
          user_apps_block="${user_apps_block}  '${user_prefix}${appid}:${options_str}'$(backup_get_comment ${appid})\n"
        done
      else
        msg2 "ignore --3rd-app and --3rd-data for adb offline" 1>&2
      fi
    fi

    if [ -n "${backup_arg_spec_data}" ]; then
      user_apps_block="${user_apps_block}  # spec user data${user_comment}\n"
      for appid in "${backup_spec_apps[@]}"; do
        if backup_is_spec_app_support_multiuser "${appid}"; then
          user_apps_block="${user_apps_block}  '${user_prefix}${appid}'\n"
        fi
      done
    fi
  done
  user_apps_block="${user_apps_block})"

  all_hook_block="## pre/post hooks for all commands
# function pre_all { echo pre_all for \${arg_cmd}; }
# function pre_all_adb {} # run in adb shell
# function all { echo all for \${arg_cmd}; }
# function all_adb {} # run in adb shell
# function post_all_adb {} # run in adb shell
# function post_all { echo pre_all for \${arg_cmd}; }"

  backup_block="## pre/post hooks for backup command
# function pre_backup {}
# function pre_backup_adb {} # run in adb shell
# function backup {}
# function backup_adb {} # run in adb shell
# function pre_backup_app { echo pre_backup_app \$appline \${!appline_options[@]}; }
# function post_backup_app { echo post_backup_app \$appline \${!appline_options[@]}; }
# function pre_backup_data { echo pre_backup_data \$appline \${!appline_options[@]}; }
# function post_backup_data { echo post_backup_data \$appline \${!appline_options[@]}; }
# function post_backup_adb {} # run in adb shell
# function post_backup {}"
  restore_block=$(echo "${backup_block}" | sed 's/backup/restore/g')

  files_block="## files
# files_tmpdir=./tmp
# files=('https://rom-url::hash=sha1:xxx;push_dir=/sdcard')"

  deploy_block="## pre/post hooks for deploy command
# function pre_deploy {}
# function pre_deploy_adb {} # run in adb shell
# function pre_deploy_file { echo pre_deploy_file \$fileline \${!fileline_options[@]}; }
# function post_deploy_file { echo post_deploy_file \$fileline \${!fileline_options[@]}; }
# function deploy {}
# function deploy_adb {} # run in adb shell
# function pre_deploy_app { echo pre_deploy_app \$appline \${!appline_options[@]}; }
# function post_deploy_app { echo pre_deploy_app \$appline \${!appline_options[@]}; }
# function post_deploy_adb {} # run in adb shell
# function post_deploy {}
# function my_deploy_file_func { local host_path=\$1; local exists =\$2; }
# function my_deploy_dir_func { local host_path=\$1; local exists =\$2; }
# deploy_device_file_funcs['file-path']=my_deploy_file_func # wrapper function to edit device file in local
# deploy_device_file_funcs['dir-path']=my_deploy_dir_func"
  cleanup_block=$(echo "${deploy_block}" | sed 's/deploy/cleanup/g')
  disable_block=$(echo "${deploy_block}" | sed 's/deploy/disable/g')
  enable_block=$(echo "${deploy_block}" | sed 's/deploy/enable/g')

  content="${header_block}\n\n${user_block}\n\n${system_apps_block}\n\n${user_apps_block}\n\n${files_block}\n\n${all_hook_block}"
  if contains "backup" "${backup_arg_profile_types[@]}"; then
    content="${content}\n\n${backup_block}"
  fi
  if contains "restore" "${backup_arg_profile_types[@]}"; then
    content="${content}\n\n${restore_block}"
  fi
  if contains "deploy" "${backup_arg_profile_types[@]}"; then
    content="${content}\n\n${deploy_block}"
  fi
  if contains "cleanup" "${backup_arg_profile_types[@]}"; then
    content="${content}\n\n${cleanup_block}"
  fi
  if contains "disable" "${backup_arg_profile_types[@]}"; then
    content="${content}\n\n${disable_block}"
  fi
  if contains "enable" "${backup_arg_profile_types[@]}"; then
    content="${content}\n\n${enable_block}"
  fi
  echo -e "${content}"
}

function _cmd_usage_diff {
  echo "diff [--device-tmpdir dir] <--profile file.sh>
    compare applications in current device with target profile"
}
function cmd_diff {
  msg "Compare applications in current device with profile ${backup_arg_profile}"

  local profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=() files=() files_tmpdir= backup_prepare_args
  backup_prepare_args "${@}"

  # collect only appuser and appid, ignore options
  local item=
  local apps_only_in_profile=() apps_only_in_device=()

  # collect applications in profile
  local profile_user_apps=()
  local appline=
  for appline in $(for i in "${user_apps[@]}"; do echo $i; done | sort); do
    local appuser= appid=; unset appline_options; local -A appline_options
    backup_parse_appline "${appline}"
    if backup_is_spec_app "${appid}"; then
      continue
    fi
    if [ "${appuser}" = "0" ]; then
      item="${appid}"
    else
      item="user_${appuser}:${appid}"
    fi
    if ! contains "${item}" "${profile_user_apps[@]}"; then
      profile_user_apps+=("${item}")
    else
      warning "repeat item: ${item}"
    fi
  done

  # collect applications in devices
  local device_user_apps=()
  local device_sys_apps_str=$(adb_list-sys-packages)
  local user= users_str="$(adb_list-users | awk '{print $1}')"
  for user in ${users_str}; do
    for appid in $(adb_list-3rd-packages ${user} | sort); do
      if [ "${user}" = "0" ]; then
        item="${appid}"
      else
        item="user_${user}:${appid}"
      fi
      device_user_apps+=("${item}")
      if ! contains "${item}" "${profile_user_apps[@]}"; then
        apps_only_in_device+=("${item}")
      fi
    done
  done

  for item in "${profile_user_apps[@]}"; do
    if ! contains "${item}" "${device_user_apps[@]}"; then
      backup_parse_appline "${item}"
      if ! contains "${appid}" ${device_sys_apps_str}; then
        apps_only_in_profile+=("${item}")
      fi
    fi
  done

  # print result
  if [ "${#apps_only_in_profile[@]}" -gt 0 ]; then
    msg "applications that only in profile:"
    for i in "${apps_only_in_profile[@]}"; do
      echo "  $i"
    done
  fi
  if [ "${#apps_only_in_device[@]}" -gt 0 ]; then
    msg "applications that only in device:"
    for i in "${apps_only_in_device[@]}"; do
      echo "  $i"
    done
  fi
}

function _cmd_usage_backup {
  echo "backup [--name name] [--password pwd] [--options options] [--filter filter] [--appdir dir] [--datadir dir] [--device-tmpdir dir] [--archive-side value] [--ignore-check-profile] [--ignore-check-timestamp] <--profile file.sh|BACKUPAPPLINE..|${backup_appinfo_name}..>
    backup android application and user data"
}
function cmd_backup {
  adb_assert-root
  backup_fix_archive_side

  local profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=() files=() files_tmpdir= backup_prepare_args
  backup_prepare_args "${@}"

  run_cmd_hook "backup" "pre"
  run_cmd_hook "backup"

  msg "Backup applications"
  msg2 "datadir: ${datadir}"

  local appline=
  for appline in "${system_apps[@]}" "${user_apps[@]}"; do
    local appuser= appid=; unset appline_options; local -A appline_options
    backup_parse_appline "${appline}"
    if contains "${appuser}" "${users[@]}"; then
      _cmd_do_backup
    fi
  done
  run_cmd_hook "backup" "post"
}

# arg1: appid, arg2: version_code, arg3: version_name
function get_app_filename {
  # fix version_name, replace '/' char to '-'
  echo "${1}_${2}_${3//\//-}.apk"
}
function get_app_filename_fallback {
  echo "${1}-${3}-${2}.apk"
}
function get_app_filename_fallback2 {
  echo "${1}_${2}.apk"
}
function get_app_filename_fallback3 {
  echo "${1}_${2}_v${3//\//-}.apk"
}

function _cmd_do_backup {
  # source appinfo.sh
  local user_datadir="${datadir}/user_${appuser}"
  if [ -n "${appline_options[datadir]}" ]; then
    user_datadir="${appline_options[datadir]}/user_${appuser}"
  fi
  local host_datadir="${user_datadir}/${appid}"
  local appinfo_file="${host_datadir}/${backup_appinfo_name}"
  unset appinfo_options; local -A appinfo_options

  if [ -f "${appinfo_file}" ]; then
    backup_parse_appinfo_file "${appinfo_file}"
  else
    appinfo_options[data_timestamp]=0
    appinfo_options[archive_type]="${backup_archive_type}"
  fi

  # override archive_type
  if [ -n "${appline_options[archive_type]}" ]; then
    appinfo_options[archive_type]="${appline_options[archive_type]}"
  fi

  # collect app info
  if adb_is-package-installed "${appid}"; then
    appinfo_options[version_name]=$(adb_get-package-version-name "${appid}")
    appinfo_options[version_code]=$(adb_get-package-version-code "${appid}")

    if adb_is-sys-package "${appid}"; then
      appinfo_options[is_system]='1'
    fi

    # save system app directory if is not /system/app
    if [ -n "${appinfo_options[is_system]}" ]; then
      local device_appdir=$(dirname "${device_appfile}")
      local apilevel=$(adb_get-android-api-level)
      if [ "${apilevel}" -ge "${ANDROID_VERSION_CODES_LOLLIPOP}" ]; then
        device_appdir=$(dirname "${device_appdir}")
      fi
      if [ "${device_appdir}" != "/system/app" ]; then
        appinfo_options[install_path]="${device_appdir}"
      fi
    fi
  fi

  # backup app
  if [[ "${appline_options[type]}" =~ "app" ]]; then
    run_appline_hook "backup" "pre" "app" "user_${appuser}:${appid}"
    if ! adb_is-package-installed "${appid}" "${appuser}"; then
      msg2 "ignore backup app for user_${appuser}:${appid}: not installed in user ${appuser}"
      if ! backup_is_spec_app "${appid}"; then
        return
      fi
    else
      local app_filename=$(get_app_filename "${appid}" "${appinfo_options[version_code]}" "${appinfo_options[version_name]}")
      local host_appfile="${appdir}/${app_filename}"
      if [ -n "${appline_options[appdir]}" ]; then
        host_appfile="${appline_options[appdir]}/${app_filename}"
      fi
      if [ -n "${appline_options[appfile]}" ]; then
        host_appfile="${appline_options[appfile]}"
      fi

      mkdir -p "$(dirname "${host_appfile}")"
      local device_appfile=$(adb_get-package-path "${appid}")
      local ignore_app=
      if [ -f "${host_appfile}" ]; then
        if adb_is-same-file "${device_appfile}" "${host_appfile}"; then
          ignore_app=1
        else
          msg2 "backup app user_${appuser}:${appid}: ${host_appfile} exists but checksum failed, will overwrite later"
        fi
      fi
      if [ -n "${ignore_app}" ]; then
        msg2 "ignore backup app for user_${appuser}:${appid}: ${host_appfile} is up to date"
      else
        msg2 "backup app user_${appuser}:${appid}: ${device_appfile} -> ${host_appfile}"
        adb_pull "${device_appfile}" "${host_appfile}" 1 || true
      fi
    fi
    run_appline_hook "backup" "post" "app" "user_${appuser}:${appid}"
  fi

  # backup data
  if [[ "${appline_options[type]}" =~ "data" ]]; then
    run_appline_hook "backup" "pre" "data" "user_${appuser}:${appid}"
    local device_backup_files=()
    backup_get_backup_files "${appid}" "${appuser}"
    local device_timestamp=0
    local ignore_data=
    local f=
    for f in "${device_backup_files[@]}"; do
      local t=$(adb_get-file-timestamp "${f}")
      if [ "${t}" -gt "${device_timestamp}" ]; then
        device_timestamp="${t}"
      fi
    done
    if [ -f "${appinfo_file}" ]; then
      if [ "${device_timestamp}" -le "${appinfo_options[data_timestamp]}" -a -z "${backup_arg_ignore_check_timestamp}" ]; then
        ignore_data=1
      else
        appinfo_options[data_timestamp]="${device_timestamp}"
      fi
    else
      appinfo_options[data_timestamp]="${device_timestamp}"
    fi
    if [ "${device_timestamp}" -eq '0' ]; then
      # all backup files not found in device
      ignore_data=2
    fi

    if [ -n "${ignore_data}" ]; then
      case "${ignore_data}" in
        1) msg2 "ignore backup data for user_${appuser}:${appid}: ${host_datadir} is up to date";;
        2) msg2 "ignore backup data for user_${appuser}:${appid}: data files(${device_backup_files[*]}) all not found in device";;
      esac
    else
      local archive_filename="${appid}.${appinfo_options[archive_type]}"
      # pull data files from device
      mkdir -p "${host_datadir}"

      if [ "${backup_archive_side}" = "device" ]; then
        # make archive in device and pull
        local backup_filenames=()
        for f in "${device_backup_files[@]}"; do
          if [[ "${f}" =~ ^/data/user_de ]]; then
            backup_filenames+=( user_de/$(basename "${f}") )
          else
            backup_filenames+=( $(basename "${f}") )
          fi
        done

        # cleanup tmp files
        adb_rootshell "cd '${adb_device_tmpdir}' && ${adb_busybox} rm -rf ${backup_filenames[@]} user_de"

        # create symlink in tmp dir
        for f in "${device_backup_files[@]}"; do
          if [[ "${f}" =~ ^/data/user_de ]]; then
            adb_rootshell "cd '${adb_device_tmpdir}' && ${adb_busybox} mkdir -p user_de && ${adb_busybox} ln -sf "${f}" ./user_de"
          else
            adb_rootshell "cd '${adb_device_tmpdir}' && ${adb_busybox} ln -sf "${f}" ./"
          fi
        done

        msg2 "backup data user_${appuser}:${appid}: ${host_datadir}/${archive_filename}"
        adb_rawrootshell "${adb_busybox} tar -chf - -C '${adb_device_tmpdir}' ${backup_filenames[@]}" | pipe_pack_file "${host_datadir}/${archive_filename}" || true

        # cleanup tmp files again
        adb_rootshell "cd '${adb_device_tmpdir}' && ${adb_busybox} rm -rf ${backup_filenames[@]} user_de"
      else
        # pull files and make archive in host
        local backup_filenames=()
        for f in "${device_backup_files[@]}"; do
          if [[ "${f}" =~ ^/data/user_de ]]; then
            msg2 "backup data user_${appuser}:${appid}: ${f} -> ${host_datadir}/user_de"
            mkdir -p "${host_datadir}/user_de"
            rm -rf "${host_datadir}/user_de/$(basename "${f}")"
            if adb_pull "${f}" "${host_datadir}/user_de" 1; then
              backup_filenames+=( user_de/$(basename "${f}") )
            fi
          else
            msg2 "backup data user_${appuser}:${appid}: ${f} -> ${host_datadir}"
            rm -rf "${host_datadir}/$(basename "${f}")"
            if adb_pull "${f}" "${host_datadir}" 1; then
              backup_filenames+=( $(basename "${f}") )
            fi
          fi
        done

        (
          cd "${host_datadir}"
          rm -f "${archive_filename}"
          pack_file "${archive_filename}" "${backup_filenames[@]}"
          # cleanup tmp files again
          rm -rf "${backup_filenames[@]}" user_de
        )
      fi

      if [ -n "${backup_arg_password}" ]; then
        msg2 "encrypt archive: ${archive_filename} -> ${archive_filename}.enc"
        local archive_file="${host_datadir}/${archive_filename}"
        if _cmd_backup_do_encrypt_data_archive "${archive_file}"; then
          appinfo_options[is_encrypted]='1'
          rm -f "${archive_file}"
        fi
      fi
    fi
    run_appline_hook "backup" "post" "data" "user_${appuser}:${appid}"
  fi

  # generate appinfo.sh
  mkdir -p "${host_datadir}"
  appinfo_options[id]="${appid}" appinfo_options[android_api_level]=$(adb_get-android-api-level)
  backup_generate_appinfo_file "${appinfo_file}"
}

# arg1: archive_file
function _cmd_backup_do_encrypt_data_archive {
  if [ -z "${backup_arg_password}" ]; then
    warning "Encrypt data archive failed: need --password argument"
    return 1
  fi
  encrypt_file "${backup_arg_password}" "${1}" "${1}".enc
}

# arg1: archive_file (not encrypted)
function _cmd_backup_do_decrypt_data_archive {
  if [ -z "${backup_arg_password}" ]; then
    warning "Decrypt data archive failed: need --password argument"
    return 1
  fi
  decrypt_file "${backup_arg_password}" "${1}".enc "${1}"
}

function _cmd_usage_restore {
  echo "restore [--name name] [--password pwd] [-u|--user id] [--options options] [--filter filter] [--wait-sys-app seconds] [--appdir dir] [--datadir dir] [--device-tmpdir dir] [--archive-side value] [--ignore-check-profile] [--ignore-check-timestamp] <--profile file.sh|RESTOREAPPLINE..|${backup_appinfo_name}..>
    restore android application and user data, for multiple users, will create user if need"
}
function cmd_restore {
  adb_assert-root
  backup_fix_archive_side

  local error_apps=()

  local profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=() files=() files_tmpdir= backup_prepare_args
  backup_prepare_args "${@}"

  run_cmd_hook "restore" "pre"
  run_cmd_hook "restore"

  # ensure users exists
  local user= user_name=
  for user in "${users[@]}"; do
    local -n user_name="user_${user}_name"
    if [ "${user}" != "system" ]; then
      adb_ensure-user-exists "${user}" "${user_name}"
    fi
  done

  local redirect_user=
  if [ ${#backup_arg_users[@]} -gt 0 -a -z "${backup_arg_profile}" ]; then
    users=("${backup_arg_users[@]}")
    redirect_user=1
  fi

  local new_app_installed_in_prev_user=
  for user in "${users[@]}"; do
    msg "Restore applications for user ${user}"

    # if new application install in previous user, just update package cache
    if [ -n "${new_app_installed_in_prev_user}" ]; then
      _adb_fetch_pkgs_cache
      new_app_installed_in_prev_user=
    fi

    local appline=
    for appline in "${system_apps[@]}" "${user_apps[@]}"; do
      local appuser= appid=; unset appline_options; local -A appline_options
      backup_parse_appline "${appline}"
      if [ -n "${redirect_user}" ]; then
        _cmd_do_restore
      elif [ "${appuser}" = "${user}" ]; then
        _cmd_do_restore
      fi
    done
  done

  run_cmd_hook "restore" "post"
  if [ ${#error_apps[@]} -ne 0 ]; then
    warning "Restore applications failed: ${error_apps[*]}"
    return 1
  else
    return 0
  fi
}

function _cmd_do_restore {
  # source appinfo.sh
  local data_user="${appuser}"
  if [ -n "${appline_options[datauser]}" ]; then
    data_user="${appline_options[datauser]}"
  fi

  local user_datadir="${datadir}/user_${data_user}"
  if [ -n "${appline_options[datadir]}" ]; then
    user_datadir="${appline_options[datadir]}/user_${data_user}"
  fi
  local host_datadir="${user_datadir}/${appid}"
  local appinfo_file="${host_datadir}/${backup_appinfo_name}"
  unset appinfo_options; local -A appinfo_options

  if [ -f "${appinfo_file}" ]; then
    backup_parse_appinfo_file "${appinfo_file}"
  elif [ "${appline_options[type]}" = "data" ]; then
    appinfo_options[data_timestamp]=0
    appinfo_options[archive_type]="${backup_archive_type}"
  else
    msg2 "ignore restore app for user_${appuser}:${appid}: ${appinfo_file} not found"
    return
  fi

  # appline has high priority, override archive_type and version_code
  if [ -n "${appline_options[archive_type]}" ]; then
    msg2 "appline has high priority, override archive_type: ${appinfo_options[archive_type]} -> ${appline_options[archive_type]}"
    appinfo_options[archive_type]="${appline_options[archive_type]}"
  fi
  if [ -n "${appline_options[version_code]}" ]; then
    msg2 "appline has high priority, override version_code: ${appinfo_options[version_code]} -> ${appline_options[version_code]}"
    appinfo_options[version_code]="${appline_options[version_code]}"
  fi

  if ! backup_is_spec_app "${appid}"; then
    adb_stop-package "${appid}"
  fi

  # restore app
  local fresh_installed=
  if [[ "${appline_options[type]}" =~ "app" ]]; then
    run_appline_hook "restore" "pre" "app" "user_${appuser}:${appid}"
    local app_filename=$(get_app_filename "${appid}" "${appinfo_options[version_code]}" "${appinfo_options[version_name]}")
    local host_appfile="${appdir}/${app_filename}"
    if [ -n "${appline_options[appdir]}" ]; then
      host_appfile="${appline_options[appdir]}/${app_filename}"
    fi
    if [ -n "${appline_options[appfile]}" ]; then
      host_appfile="${appline_options[appfile]}"
    fi

    local ignore_app=
    if adb_is-package-installed "${appid}" "${user}"; then
      local installed_version_code=$(adb_get-package-version-code "${appid}")
      # only reinstall for newer version
      if [ 10"${installed_version_code}" -ge 10"${appinfo_options[version_code]}" ]; then
        ignore_app=1
      else
        msg2 "restore app user_${appuser}:${appid}: already installed but version is older, will reinstall later"
      fi
    elif adb_is-package-installed "${appid}"; then
      # installed in other user
      local installed_version_code=$(adb_get-package-version-code "${appid}")
      if [ 10"${installed_version_code}" -ge 10"${appinfo_options[version_code]}" ]; then
        ignore_app=2
      fi
    fi
    if [ -n "${ignore_app}" ]; then
      case "${ignore_app}" in
        1) msg2 "ignore restore app for user_${appuser}:${appid}: already installed ${installed_version_code}, prefer ${appinfo_options[version_code]}";;
        2)
          msg2 "restore app user_${appuser}:${appid}: already installed in other user"
          adb_make-pkg-installed-for-user "${appid}" "${user}"
          ;;
      esac
    else
      if [ ! -f "${host_appfile}" ]; then
        msg2 "fallback to install app from fdoird repos: ${host_appfile} not exits"
        if [ -n "${appline_options[repo]}" ]; then
          # limit repo
          local fdroid_arg_repos=("${appline_options[repo]}")
        fi
        if cmd_download "${appid}=${appinfo_options[version_code]}"; then
          host_appfileq=$(fdroid_get_app_path "${appid}" "${appinfo_options[version_code]}")
        fi
      fi

      msg2 "restore app user_${appuser}:${appid}: ${host_appfile}"
      if [ -n "${appinfo_options[is_system]}" ]; then
        if [[ "${appinfo_options[install_path]}" =~ ^/data/app ]]; then
          # shadowed system app
          if [ -n "${redirect_user}" ]; then
            adb_install "${host_appfile}" "${user}"
          else
            adb_install "${host_appfile}"
          fi
        else
          adb_install-sys "${host_appfile}" "${appinfo_options[install_path]}"
        fi
        fresh_installed=1
        new_app_installed_in_prev_user=1
      else
        if adb_install "${host_appfile}" "${user}"; then
          fresh_installed=1
          new_app_installed_in_prev_user=1
        else
          error_apps+=("user_${appuser}:${appid}:type=app")
        fi
      fi
    fi
    run_appline_hook "restore" "post" "app" "user_${appuser}:${appid}"
  fi

  # restore app data
  if [[ "${appline_options[type]}" =~ "data" ]]; then
    run_appline_hook "restore" "pre" "data" "user_${appuser}:${appid}"
    local device_backup_files=()
    backup_get_backup_files "${appid}" "${user}" "${appinfo_options[android_api_level]}"
    local backup_filenames=()
    local device_timestamp=0
    local f=
    for f in "${device_backup_files[@]}"; do
      local t=$(adb_get-file-timestamp "${f}")
      if [ "${t}" -gt "${device_timestamp}" ]; then
        device_timestamp="${t}"
      fi
      if [[ "${f}" =~ ^/data/user_de ]]; then
        backup_filenames+=( user_de/$(basename "${f}") )
      else
        backup_filenames+=( $(basename "${f}") )
      fi
    done

    local ignore_data=
    local archive_filename="${appid}.${appinfo_options[archive_type]}"
    local host_data_archive="${host_datadir}/${archive_filename}"
    if [ -n "${appline_options[datafile]}" ]; then
      host_data_archive="${appline_options[datafile]}"
      host_datadir=$(dirname "${host_data_archive}")
    fi
    if [ ! -f "${host_data_archive}" -a ! -f "${host_data_archive}.enc" ]; then
      ignore_data=1
    else
      if ! backup_is_spec_app "${appid}"; then
        local not_installed=
        if ! adb_is-package-installed "${appid}"; then
          not_installed=1
        fi

        # update application check and recheck if fresh installed just now
        if [ -n "${not_installed}" -a -n "${fresh_installed}" ]; then
          if [ -n "${appinfo_options[is_system]}" ]; then
            # wait for seconds for system app
            msg2 "wait for seconds for system application to be ready.."
            sleep "${backup_arg_wait_sys_app_timeout}"
            _adb_fetch_pkgs_cache
            if adb_is-package-installed "${appid}"; then
              not_installed=
            else
              not_installed=1
            fi
          else
            # fresh install means just installed for normal app
            not_installed=
          fi
        fi
        if [ -n "${not_installed}" ]; then
          ignore_data=2
        fi
      fi

      # just ignore if timestamp not changed
      if [ "${device_timestamp}" -eq "${appinfo_options[data_timestamp]}" -a -z "${backup_arg_ignore_check_timestamp}" ]; then
        ignore_data=3
      fi

      if ! backup_check_app_data_compatibility "${appid}" "${appinfo_options[android_api_level]}" "$(adb_get-android-api-level)"; then
        ignore_data=4
      fi
    fi

    if [ -n "${ignore_data}" ]; then
      case "${ignore_data}" in
        1) msg2 "ignore restore data for user_${appuser}:${appid}: ${host_data_archive} not found";;
        2) warning "Ignore restore data ${appid}: application not installed"
          error_apps+=("user_${appuser}:${appid}:type=data")
          ;;
        3) msg2 "ignore restore data for for user_${appuser}:${appid}: ${device_backup_files[*]} is up to date";;
        4) warning "Ignore restore data ${appid}: not compatibility with current device"
          error_apps+=("user_${appuser}:${appid}:type=data")
          ;;
      esac
    else
      (
        # push data files to device
        cd "${host_datadir}"

        # decrypt archive
        if [ -n "${appinfo_options[is_encrypted]}" ]; then
          msg2 "decrypt archive: ${archive_filename}"
          if [ -z "${backup_arg_password}" ]; then
            abort "${host_data_archive} is encrypted, need --password argument"
          fi
          _cmd_backup_do_decrypt_data_archive "${archive_filename}"
        fi

        if [ "${backup_archive_side}" = "device" ]; then
          # push and extract archive file in device

          # cleanup tmp files
          adb_rootshell "cd '${adb_device_tmpdir}' && ${adb_busybox} rm -rf ${backup_filenames[@]} user_de"

          # extract archive in device
          msg2 "restore data user_${appuser}:${appid}: ${host_data_archive} -> ${adb_device_tmpdir}"
          pipe_unpack_file "${archive_filename}" | adb_rawrootshell "${adb_busybox} tar -xf - -C '${adb_device_tmpdir}' 1>/dev/null"

          for f in "${device_backup_files[@]}"; do
            local filename=$(basename "${f}")
            if [[ "${f}" =~ ^/data/user_de ]]; then
              filename="user_de/${filename}"
            fi
            msg2 "restore data user_${appuser}:${appid}: ${adb_device_tmpdir}/${filename} -> ${f}"
            local fixed_device_path=$(backup_fix_restore_path_for_compatibility "${appid}" "${appinfo_options[android_api_level]}" "$(adb_get-android-api-level)" "${user}" "${f}")
            if [ "${f}" != "${fixed_device_path}" ]; then
              msg2 "fix path for compatibility: ${f} -> ${fixed_device_path}"
            fi
            if adb_is-file-exists "${adb_device_tmpdir}/${filename}"; then
              # NOTE: mv command will failed with "Operation not
              # permitted" for fileencryptiion enabled /data/user_de,
              # so just fallback to cp command
              adb_rootshell "cd '${adb_device_tmpdir}' && if [ -e '${filename}' ];then
  if [ -d '${filename}' ];then
    if [ ! -z \"\$( ls -A '${filename}')\" ]; then
      ${adb_busybox} mkdir -p '${fixed_device_path}'
      ${adb_busybox} rm -r '${fixed_device_path}'/* 2>/dev/null
      ${adb_busybox} mv -f '${filename}'/* '${fixed_device_path}'/ 2>/dev/null || ${adb_busybox} cp -rf '${filename}'/* '${fixed_device_path}'/
    else
      echo '  -> ignore empty dir ${filename}'
    fi
  else
    ${adb_busybox} mv -f '${filename}' '${fixed_device_path}' 2>/dev/null || ${adb_busybox} cp -f '${filename}' '${fixed_device_path}'
  fi
fi"
            else
              msg2 "ignore restore file: ${filename} not found"
            fi

            # touch to change timestamp
            adb_set-file-timestamp "${fixed_device_path}" "${appinfo_options[data_timestamp]}"
          done

          # cleanup tmp files again
          adb_rootshell "cd '${adb_device_tmpdir}' && ${adb_busybox} rm -rf ${backup_filenames[@]} user_de"
        else
          # extract archive file in host and push

          # cleanup tmp files
          rm -rf "${backup_filenames[@]}" user_de

          # extract archive in host
          unpack_file "${archive_filename}" . 1>/dev/null

          for f in "${device_backup_files[@]}"; do
            local filename=$(basename "${f}")
            if [[ "${f}" =~ ^/data/user_de ]]; then
              filename="user_de/${filename}"
            fi
            msg2 "restore data user_${appuser}:${appid}: ${host_datadir}/${filename} -> ${f}"
            local fixed_device_path=$(backup_fix_restore_path_for_compatibility "${appid}" "${appinfo_options[android_api_level]}" "$(adb_get-android-api-level)" "${user}" "${f}")
            if [ "${f}" != "${fixed_device_path}" ]; then
              msg2 "fix path for compatibility: ${f} -> ${fixed_device_path}"
            fi
            if [ -e "${filename}" ]; then
              if [ -d "${filename}" ]; then
                if ! is_empty_dir "${filename}"; then
                  adb_rootshell "${adb_busybox} rm -rf '${fixed_device_path}'/*"
                  adb_push "${filename}/" "${fixed_device_path}/" 1
                elif is_empty_dir "${filename}"; then
                  # TODO: Check for empty dir for adb push will not
                  # work it. But in this case, should we keep the
                  # device data dir empty, too?
                  msg2 "ignore restore file: ${filename} is empty direcotory"
                fi
              else
                adb_rootshell "${adb_busybox} rm -f '${fixed_device_path}'"
                adb_push "${filename}" "${fixed_device_path}" 1
              fi
            else
              msg2 "ignore restore file: ${filename} not found"
            fi

            # touch to change timestamp
            adb_set-file-timestamp "${fixed_device_path}" "${appinfo_options[data_timestamp]}"
          done

          # cleanup tmp files again
          rm -rf "${backup_filenames[@]}" user_de
        fi

        if [ -n "${appinfo_options[is_encrypted]}" ]; then
          rm -f "${host_data_archive}"
        fi

        # fix application data file permission
        msg2 "fix application data file permission: user_${appuser}:${appid}"
        if backup_is_spec_app "${appid}"; then
          backup_adb_fix-spec-datafile-permission "${appid}" "${user}"
          msg2 "maybe a reboot is necessary for special app user_${appuser}:${appid}"
        else
          adb_fix-package-datadir-permission "${appid}" "${user}"
        fi
      )
    fi
    run_appline_hook "restore" "post" "data" "user_${appuser}:${appid}"
  fi
}

function _cmd_usage_check {
  echo "check [--fix] [--clean] [--copy-to dir] [--move-to dir] [--appdir dir] [--datadir dir] [--encrypt-data] [--decrypt-data] [--password] [dir|--profile file.sh]
    check backup app and data in special directory or profile
    --fix, fix incompatible issues if possible
    --clean, remove apk files that lost reference, only works without --profile
    --copy-to, copy the related app and data to another directory, only works with --profile
    --move-to, move the related app and data to another directory, only works with --profile
    --encrypt-data, encrypt the data archive if not, need --password argument
    --decrypt-data, decrypt the data archive if not, need --password argument"
}
function cmd_check {
  local dir="${1:-${PWD}}"
  if [ -n "${backup_arg_profile}" ]; then
    msg "Check backup app and data for profile: ${backup_arg_profile}"
    if [ ! -f "${backup_arg_profile}" ]; then
      abort "Profile file not found: ${backup_arg_profile}"
    fi
    local profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=() files=() files_tmpdir= backup_prepare_args
    backup_prepare_args "${@}"

    # fix old profile format
    if compver_lt "${version}" "0.9.5"; then
      if [ -n "${backup_arg_check_fix}" ]; then
        msg2 "fix old style: profile format before 0.9.5"

        # convert user_xxx_packages to user_xxx_apps
        sed -i "s/user_\(.*\)_packages=/user_\1_apps=/" "${backup_arg_profile}"

        local user=
        for user in "${users[@]}"; do
          local applines=()
          local section=$(dump_section_headless "${backup_arg_profile}" "^user_${user}_apps=\\\\(" "^\\\\)$")
          eval "applines=(
${section}
)"
          if [ "${#applines[@]}" -eq 0 ]; then
            continue
          fi
          local fixed_applines=$(
            local IFS=$'\n'
            local appline=
            for appline in "${applines[@]}"; do
              local appid= type= appline_options=
              _backup_parse_appline_old "${appline}" "${user}"
              if [ -n "${appline_options}" ]; then
                appline_options=";${appline_options}"
              fi
              if [ "${user}" = "system" -o "${user}" = "0" ]; then
                printf "  '${appid}:type=${type}${appline_options}'\\\\n"
              else
                printf "  'user_${user}:${appid}:type=${type}${appline_options}'\\\\n"
              fi
            done
          )
          fixed_applines="${fixed_applines%\\n}"
          replace_section "${backup_arg_profile}" "user_${user}_apps=\(" "^\)$" "user_${user}_apps=(\n${fixed_applines}\n)"
          if [ "${user}" = "system" ]; then
            sed -i "s/user_system_apps=/system_apps=/" "${backup_arg_profile}"
          else
            sed -i "s/user_${user}_apps=/user_apps+=/" "${backup_arg_profile}"
          fi
        done

        sed -i "1i version='0.9.5'" "${backup_arg_profile}"

        # reload profile
        backup_prepare_args "${@}"
      else
        warning "Found old style: profile format before 0.9.5, you should use --fix argument or will report error later"
      fi
    fi

    local appline=
    for appline in "${system_apps[@]}" "${user_apps[@]}"; do
      local appuser= appid=; unset appline_options; local -A appline_options
      backup_parse_appline "${appline}"

      # source appinfo.sh
      local user_datadir="${datadir}/user_${appuser}"
      local host_datadir="${user_datadir}/${appid}"
      local appinfo_file="${host_datadir}/${backup_appinfo_name}"
      local appinfo_file_fallback="${host_datadir}/${backup_appinfo_name_fallback}"
      unset appinfo_options; local -A appinfo_options
      local id= version_name= version_code= android_version= android_api_level= is_system= is_encrypted= install_path= archive_type= data_timestamp=
      local name= install_dir=  # fallback vars

      if backup_need_fallback_file appinfo_file appinfo_file_fallback; then
        if [ -n "${backup_arg_check_fix}" ]; then
          msg2 "fix old style: rename ${appinfo_file_fallback} to ${appinfo_file}"
          mv -f "${appinfo_file_fallback}" "${appinfo_file}"
        else
          appinfo_file="${appinfo_file_fallback}"
        fi
      fi

      if [ -f "${appinfo_file}" ]; then
        safe_source "${appinfo_file}"
        _backup_convert_appinfo_vars

        # set archive_type
        if [ -n "${archive_type}" ]; then
          appinfo_options[archive_type]="${archive_type}"
        else
          appinfo_options[archive_type]="${backup_archive_type}"
        fi
        if [ -n "${appline_options[archive_type]}" ]; then
          appinfo_options[archive_type]="${appline_options[archive_type]}"
        fi
      else
        msg2 "missing ${backup_appinfo_name}: ${appinfo_file}"
        continue
      fi

      # fallback to old variables
      if backup_need_fallback_var "${appinfo_file}" install_path install_dir; then
        install_path="${install_dir}"
        appinfo_options[install_path]="${install_dir}"
        if [ -n "${backup_arg_check_fix}" ]; then
          backup_generate_appinfo_file "${appinfo_file}"
        fi
      fi
      if backup_need_fallback_var "${appinfo_file}" id name; then
        id="${name}"
        appinfo_options[id]="${name}"
        if [ -n "${backup_arg_check_fix}" ]; then
          backup_generate_appinfo_file "${appinfo_file}"
        fi
      fi

      # encrypt data archive
      if [ -n "${backup_arg_check_encrypt_data}" ]; then
        _cmd_check_do_encrypt_data_archive "${appinfo_file}"
      fi

      # decrypt data archive
      if [ -n "${backup_arg_check_decrypt_data}" ]; then
        _cmd_check_do_decrypt_data_archive "${appinfo_file}"
      fi

      # check app
      if [[ "${appline_options[type]}" =~ "app" ]]; then
        local host_appfile="${appdir}/$(get_app_filename "${appid}" "${version_code}" "${version_name}")"

        for fallback_func in get_app_filename_fallback get_app_filename_fallback2 get_app_filename_fallback3; do
          local host_appfile_fallback="${appdir}/$(${fallback_func} "${appid}" "${version_code}" "${version_name}")"
          if backup_need_fallback_file host_appfile host_appfile_fallback; then
            if [ -n "${backup_arg_check_fix}" ]; then
              msg2 "fix old style: rename ${host_appfile_fallback} to ${host_appfile}"
              mv -f "${host_appfile_fallback}" "${host_appfile}"
            else
              host_appfile="${host_appfile_fallback}"
            fi
          fi
        done

        if [ ! -f "${host_appfile}" ]; then
          msg2 "missing app: ${host_appfile}"
        else
          _cmd_check_do_copy_or_move_file "$(dirname "${appdir}")" "${host_appfile}"
        fi
      fi

      # check app data
      if [[ "${appline_options[type]}" =~ "data" ]]; then
        local archive_filename="${appid}.${appinfo_options[archive_type]}"
        if [ -n "${is_encrypted}" ]; then
          archive_filename="${archive_filename}.enc"
        fi
        local host_data_archive="${host_datadir}/${archive_filename}"
        if [ ! -f "${host_data_archive}" ]; then
          msg2 "missing data: ${host_data_archive}"
        fi
      fi
      _cmd_check_do_copy_or_move_file "$(dirname $(dirname "${datadir}"))" "${host_datadir}"
    done

    _cmd_check_do_copy_or_move_file "$(dirname "${backup_arg_profile}")" "${backup_arg_profile}"
  else
    msg "Check backup app and data for directory: ${dir}"
    local appdir=$(get_realpath "${backup_appdir}" "${dir}")
    local IFS=$'\n'

    # collect all apk files
    local all_apps= appfile=
    local -A all_apps
    for appfile in $(find "${dir}" -iname '*.apk'); do
      all_apps["$(get_realpath ${appfile})"]=0
    done

    local appinfo_file=
    for appinfo_file in $(find "${dir}" -iname "${backup_appinfo_name}" -o -iname "${backup_appinfo_name_fallback}"); do
      if [[ "${appinfo_file}" =~ "${backup_appinfo_name_fallback}"$ ]]; then
        local appinfo_new_file=$(dirname "${appinfo_file}")/"${backup_appinfo_name}"
        if backup_need_fallback_file appinfo_new_file appinfo_file && [ -n "${backup_arg_check_fix}" ]; then
          msg2 "fix old style: rename ${appinfo_file} to ${appinfo_new_file}"
          mv -f "${appinfo_file}" "${appinfo_new_file}"
          appinfo_file="${appinfo_new_file}"
        fi
      fi

      unset appinfo_options; local -A appinfo_options
      local id= version_name= version_code= android_version= android_api_level= is_system= is_encrypted= install_path= archive_type= data_timestamp=
      local name= install_dir=  # fallback vars
      if [ -f "${appinfo_file}" ]; then
        safe_source "${appinfo_file}"
        _backup_convert_appinfo_vars

        # set archive_type
        if [ -n "${archive_type}" ]; then
          appinfo_options[archive_type]="${archive_type}"
        else
          appinfo_options[archive_type]="${backup_archive_type}"
        fi
      else
        msg2 "missing ${backup_appinfo_name}: ${appinfo_file}"
        continue
      fi

      # fallback to old variables
      if backup_need_fallback_var "${appinfo_file}" install_path install_dir; then
        install_path="${install_dir}"
        appinfo_options[install_path]="${install_dir}"
        if [ -n "${backup_arg_check_fix}" ]; then
          backup_generate_appinfo_file "${appinfo_file}"
        fi
      fi
      if backup_need_fallback_var "${appinfo_file}" id name; then
        id="${name}"
        appinfo_options[id]="${name}"
        if [ -n "${backup_arg_check_fix}" ]; then
          backup_generate_appinfo_file "${appinfo_file}"
        fi
      fi

      # encrypt data archive
      if [ -n "${backup_arg_check_encrypt_data}" ]; then
        _cmd_check_do_encrypt_data_archive "${appinfo_file}"
      fi

      # decrypt data archive
      if [ -n "${backup_arg_check_decrypt_data}" ]; then
        _cmd_check_do_decrypt_data_archive "${appinfo_file}"
      fi

      if backup_is_spec_app "${id}"; then
        continue
      fi

      # check app
      local host_appfile=$(get_realpath "${appdir}/$(get_app_filename "${id}" "${version_code}" "${version_name}")")

      for fallback_func in get_app_filename_fallback get_app_filename_fallback2 get_app_filename_fallback3; do
        local host_appfile_fallback=$(get_realpath "${appdir}/$(${fallback_func} "${id}" "${version_code}" "${version_name}")")
        if backup_need_fallback_file host_appfile host_appfile_fallback; then
          if [ -n "${backup_arg_check_fix}" ]; then
            msg2 "fix old style: rename ${host_appfile_fallback} to ${host_appfile}"
            mv -f "${host_appfile_fallback}" "${host_appfile}"
            if [ -z "${all_apps[${host_appfile}]}" ]; then
              all_apps["${host_appfile}"]=0
            fi
            unset all_apps["${host_appfile_fallback}"]
          else
            host_appfile="${host_appfile_fallback}"
          fi
        fi
      done

      if [ -z "${all_apps[${host_appfile}]}" ]; then
        msg2 "missing app: ${host_appfile}"
      else
        all_apps["${host_appfile}"]=$((all_apps["${host_appfile}"] + 1))
      fi
    done

    # show all apk files that lost reference
    for f in "${!all_apps[@]}"; do
      if [ "${all_apps[${f}]}" -eq 0 ]; then
        msg2 "lost reference: ${f}"
        if [ -n "${backup_arg_check_clean}" ]; then
          msg2 "clean file: ${f}"
          rm -f "${f}"
        fi
      fi
    done
  fi
}

# arg1: src_base_dir, arg2: src_path
function _cmd_check_do_copy_or_move_file {
  [ -n "${backup_verbose}" ] && msg2 "_cmd_check_do_copy_or_move_file: $&"
  local opt_type=
  if [ -n "${backup_arg_check_copy_to}" ]; then
    opt_type='copy'
  elif [ -n "${backup_arg_check_move_to}" ]; then
    opt_type='move'
  fi
  if [ -z "${opt_type}" ]; then
    return
  fi

  local src_base_dir="${1}"
  local src_path="${2}"
  local src_sub_path="${2##${1}}"
  src_sub_path="${src_sub_path#/}" # remove prefix '/' if exists
  src_sub_path=$(dirname "${src_sub_path}")
  if [ "${src_sub_path}" = '.' ]; then
    src_sub_path=
  fi

  local dest_base_dir=
  local dest_dir=
  if [ "${opt_type}" = 'copy' ]; then
    dest_base_dir="${backup_arg_check_copy_to}"
    dest_dir="${dest_base_dir}/${src_sub_path}"
    msg2 "check: ${opt_type} ${src_path} -> ${dest_dir}"
    mkdir -p "${dest_dir}"
    cp -rf "${src_path}" "${dest_dir}"
  else
    dest_base_dir="${backup_arg_check_move_to}"
    dest_dir="${dest_base_dir}/${src_sub_path}"
    msg2 "check: ${opt_type} ${src_path} -> ${dest_dir}"
    mkdir -p "${dest_dir}"
    mv -f "${src_path}" "${dest_dir}"
    rmdir -p "${src_base_dir}/${src_sub_path}" || true
  fi
}

# arg: appinfo_file
function _cmd_check_do_encrypt_data_archive {
  local appinfo_file="${1}"

  if [ -n "${appinfo_options[is_encrypted]}" ]; then
    msg2 "ignore encrypt data archive: ${appinfo_file} already encrypted"
    return
  fi

  local appinfo_dir=$(dirname "${appinfo_file}")
  local archive_filename="${id}.${appinfo_options[archive_type]}"
  local archive_file
  if [ -n "${appinfo_dir}" ]; then
    archive_file="${appinfo_dir}/${archive_filename}"
  else
    archive_file="${archive_filename}"
  fi

  local IFS=' '
  if _cmd_backup_do_encrypt_data_archive "${archive_file}"; then
    msg2 "encrypt data archive: ${archive_file}.enc"
    rm -f "${archive_file}"
    appinfo_options[is_encrypted]='1'
    backup_generate_appinfo_file "${appinfo_file}"
  fi
}

# arg: appinfo_file
function _cmd_check_do_decrypt_data_archive {
  local appinfo_file="${1}"

  if [ -z "${appinfo_options[is_encrypted]}" ]; then
    msg2 "ignore decrypt data archive: ${appinfo_file} already decrypted"
    return
  fi

  local appinfo_dir=$(dirname "${appinfo_file}")
  local archive_filename="${id}.${appinfo_options[archive_type]}"
  local archive_file
  if [ -n "${appinfo_dir}" ]; then
    archive_file="${appinfo_dir}/${archive_filename}"
  else
    archive_file="${archive_filename}"
  fi

  local IFS=' '
  if _cmd_backup_do_decrypt_data_archive "${archive_file}"; then
    msg2 "decrypt data archive: ${archive_file}"
    rm -f "${archive_file}.enc"
    appinfo_options[is_encrypted]=''
    backup_generate_appinfo_file "${appinfo_file}"
  fi
}

###*** Dispatch backup arguments

backup_arg_profile=; declare -n arg_profile=backup_arg_profile
backup_arg_name=
backup_arg_appdir=
backup_arg_datadir=
backup_arg_options=
backup_arg_filter=
backup_arg_password=
backup_arg_3rd_app=
backup_arg_3rd_data=
backup_arg_sys_app=
backup_arg_sys_data=
backup_arg_freq_app=
backup_arg_freq_data=
backup_arg_spec_data=
backup_arg_wait_sys_app_timeout=3
backup_arg_ignore_check_profile=
backup_arg_ignore_check_timestamp=
backup_arg_check_fix=
backup_arg_check_clean=
backup_arg_check_copy_to=
backup_arg_check_move_to=
backup_arg_check_encrypt_data=
backup_arg_check_decrypt_data=
declare -a backup_arg_profile_types
declare -a backup_arg_users

declare -a backup_left_args=()
function backup_dispatch_args {
  while [ ${#} -gt 0 ]; do
    case "${1}" in
      --profile-type) backup_arg_profile_types+=("${2}"); shift;shift;;
      --3rd-app) backup_arg_3rd_app=1; shift;;
      --3rd-data) backup_arg_3rd_data=1; shift;;
      --sys-app) backup_arg_sys_app=1; shift;;
      --sys-data) backup_arg_sys_data=1; shift;;
      --freq-app) backup_arg_freq_app=1; shift;;
      --freq-data) backup_arg_freq_data=1; shift;;
      --spec-data) backup_arg_spec_data=1; shift;;
      -p|--profile) backup_arg_profile="${2}"; shift;shift;;
      --name) backup_arg_name="${2}"; shift;shift;;
      -u|--user)
        if [ "${2}" = 'cur' -o "${2}" = 'current' ]; then
          local cur=$(adb_get-current-user)
          if ! contains "${cur}" "${backup_arg_users[@]}"; then
            backup_arg_users+=("${cur}")
          fi
        elif [ "${2}" = 'sys' ]; then
          if ! contains "system" "${backup_arg_users[@]}"; then
            backup_arg_users+=("system")
          fi
        elif [ "${2}" = 'all' ]; then
          local u=
          for u in $(adb_list-users | awk '{print $1}'); do
            if ! contains "${u}" "${backup_arg_users[@]}"; then
              backup_arg_users+=("${u}")
            fi
          done
        else
          if ! contains "${2}" "${backup_arg_users[@]}"; then
            backup_arg_users+=("${2}")
          fi
        fi
        shift;shift;;
      --appdir) backup_arg_appdir="${2}"; shift;shift;;
      --datadir) backup_arg_datadir="${2}"; shift;shift;;
      --archive-side) backup_archive_side="${2}"; shift;shift;;
      --password) backup_arg_password="${2}"; shift;shift;;
      --options) backup_arg_options="${2}"; shift;shift;;
      --filter) backup_arg_filter="${2}"; shift;shift;;
      --ignore-check-profile) backup_arg_ignore_check_profile=1; shift;;
      --ignore-check-timestamp) backup_arg_ignore_check_timestamp=1; shift;;
      --wait-sys-app) backup_arg_wait_sys_app_timeout="${2}"; shift;shift;;
      --fix) backup_arg_check_fix=1; shift;;
      --clean) backup_arg_check_clean=1; shift;;
      --copy-to) backup_arg_check_copy_to="${2}"; shift;shift;;
      --move-to) backup_arg_check_move_to="${2}"; shift;shift;;
      --encrypt-data) backup_arg_check_encrypt_data=1; shift;;
      --decrypt-data) backup_arg_check_decrypt_data=1; shift;;
      *) backup_left_args+=("${1}"); shift;;
    esac
  done

  # fix password
  if [ -n "${backup_arg_password}" ]; then
    check_depends backup_depends_optional
    if [ "${backup_arg_password}" = "-" ]; then
      printf "password: "
      backup_arg_password=$(read_password)
      echo
    fi
  fi
}
