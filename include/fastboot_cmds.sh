# Depends: utils.sh

###*** Fastboot subcommands

# Configurable variables, could override in host script or config file
fastboot_cmd="fastboot"
fastboot_verbose=

function _cmd_usage_fastboot {
  echo "fastboot <fastboot-subcommand>
    run fastboot subcommands"
}
function cmd_fastboot {
  local fastboot_subcmd="${1}"; shift
  fastboot_"${fastboot_subcmd}" "${@}"
}

###**** Wrapper
function _fastboot_usage_run {
  echo "run <command> [args..], run fastboot command with args"
}
function fastboot_run {
  # if --verbose exists, print command that be executed, and redirect
  # to stderr to prevent influence of result
  [ -n "${fastboot_verbose}" ] && msg2 "${fastboot_cmd} ${*}" 1>&2
  ${fastboot_cmd} "${@}" 2>&1
}

function _fastboot_abortrun {
  fastboot_run "${@}" || abort "Execute fastboot commands failed, for logical partition related commands, ensure enter fastboot mode instead of bootlaoder mode!"
}

###**** Check online
function _fastboot_usage_assert-online {
  echo "assert-online, abort if no device online"
}
function fastboot_assert-online {
  if ! fastboot_is-online; then
    local devices=$(fastboot_list-devices)
    local device_num=$(echo "${devices}" | wc -l)
    if [ "${device_num}" -gt 1 ]; then
      abort "There are multiple fastboot devices($(join_lines "${devices}")), please select one with argument '--serial'"
    else
      abort "Looks fastboot device(${fastboot_serial:-default}) is offline"
    fi
  fi
}

function _fastboot_usage_list-devices {
  echo "list-devices, list all devices"
}
function fastboot_list-devices {
  fastboot_run devices | grep -E '(fastboot)$' | awk '{print $1}'
}

function _fastboot_usage_is-online {
  echo "is-online, check if there is device online, return 1 if not"
}
function fastboot_is-online {
  if [ -n "$(fastboot_list-devices)" ]; then
    return 0
  else
    return 1
  fi
}

###**** Partition
function _fastboot_usage_recreate-partition {
  echo "recreate-partition <part-name> <part-size>"
}
function fastboot_recreate-partition {
  local part_name="${1}"
  local part_size="${2}"
  _fastboot_abortrun delete-logical-partition "${part_name}"
  _fastboot_abortrun create-logical-partition "${part_name}" "${part_size}"
}

function _fastboot_usage_get-partition-size {
  echo "get-partition-size <part-name>"
}
function fastboot_get-partition-size {
  local part_name="${1}"
  fastboot_run getvar "partition-size:${part_name}" | head -1 | awk '{print $2}'
}

function _fastboot_usage_resize-partition {
  echo "resize-partition <part-name> [size-or-percent], default arg2 is 110% which means increase partition size 10%"
}
function fastboot_resize-partition {
  local part_name="${1}"
  local new_part_size="${2:-110%}"

  local old_part_size=$(fastboot_get-partition-size "${part_name}")
  if [[ "${new_part_size}" = *% ]]; then
    local scale="${new_part_size%%%}"
    new_part_size=$(awk "BEGIN {print ${old_part_size} * ${scale} / 100}")
    new_part_size=$(printf "%.0f" "${new_part_size}")
  fi

  [ 0"${new_part_size}" -gt 0 ] || abort "Unknown partition size: ${new_part_size}"
  _fastboot_abortrun resize-logical-partition "${part_name}" "${new_part_size}"
  msg2 "NOTE: Already inrease partition ${part_name} size from ${old_part_size} to ${new_part_size}, also need goto twrp recovery execute 'resize2fs -f /dev/block/mapper/${part_name}' to apply the new size. If run resize2fs failed in twrp, just recreate the partition and retry"
}

function _fastboot_usage_prepare-gsi {
  echo "prepare-gsi, delete product parition and recreate system partition to prepare to flash gsi system image for dynamic partition"
}
function fastboot_prepare-gsi {
  _fastboot_abortrun delete-logical-partition product
  _fastboot_abortrun delete-logical-partition system
  _fastboot_abortrun create-logical-partition system 0xC0000000 # 3GB
  msg2 "NOTE: Already delete product partion and recreate system partition with 3GB size. If use 'fastboot flash sysstem xxx.img' later, the 3GB size of system will be ignored. If flash system image under twrp recovery, remember execute 'resize2fs -f /dev/block/mapper/system' to apply the 3GB size"
}

function _fastboot_usage_factory-reset {
  echo "factory-reset"
}
function fastboot_factory-reset {
  fastboot_run erase userdata
}

function _fastboot_usage_wipe-cache {
  echo "wipe-cache"
}
function fastboot_wipe-cache {
  fastboot_run erase cache
}

function _fastboot_usage_flash {
  echo "flash <part-name> <image-file> [extra-args..], erase then flash image file to partition"
}
function fastboot_flash {
  local part_name="${1}"; shift
  local image_file="${1}"; shift
  fastboot_run erase "${part_name}"
  fastboot_run flash "${part_name}" "${image_file}" "${@}"
}

function _fastboot_usage_get-current-slot {
  echo "get-current-slot"
}
function fastboot_get-current-slot {
  fastboot_run getvar current-slot | head -1 | awk '{ print $2 }'
}

function _fastboot_usage_set-active-slot {
  echo "set-active-slot <slot>"
}
function fastboot_set-active-slot {
  fastboot_run set_active "${1}"
}
