# Depends: utils.sh, adb_utils.sh, backup_cmds.sh, fdroid_cmds.sh

###*** Deploy commands

function _cmd_usage_deploy {
  echo "deploy [--options options] [--filter filter] <--profile file.sh|APPLINE..>
    deploy applications from profile"
}
function cmd_deploy {
  declare -A deploy_pre_app_hooks
  declare -A deploy_post_app_hooks
  declare -A deploy_file_funcs
  local error_apps=()

  local profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=()
  backup_prepare_args "${@}"

  run_cmd_hook "deploy" "pre"

  # ensure users exists
  local user= user_name=
  for user in "${users[@]}"; do
    declare -n user_name="user_${user}_name"
    if [ x"${user}" != x"system" ]; then
      adb_ensure-user-exists "${user}" "${user_name}"
    fi
  done

  local new_app_installed_in_prev_user=
  for user in "${users[@]}"; do
    local user_datadir="${datadir}/user_${user}"
    msg "Install applications for user ${user}"

    # if new application install in previous user, just update package cache
    if [ -n "${new_app_installed_in_prev_user}" ]; then
      _adb_fetch_pkgs_cache
      new_app_installed_in_prev_user=
    fi

    local appline=
    for appline in "${system_apps[@]}" "${user_apps[@]}"; do
      local appuser= appid=; unset appline_options; declare -A appline_options
      backup_parse_appline "${appline}"
      if [ x"${appuser}" = x"${user}" ]; then
        if [[ "${appline_options[type]}" =~ "app" ]]; then
          # deploy app
          _cmd_do_deploy
        fi
        if [[ "${appline_options[type]}" =~ "data" ]] && [ -n "${datadir}" -o -n "${appline_options[datadir]}" -o -n "${appline_options[datafile]}" ]; then
          # restore app data
          if [[ "${appline_options[type]}" =~ "app" ]]; then
            _adb_fetch_pkgs_cache
          fi
          appline_options[type]="data"
          _cmd_do_restore
        fi
      fi
    done
  done

  run_file_funcs "deploy"
  run_cmd_hook "deploy" "post"
  if [ ${#error_apps[@]} -ne 0 ]; then
    warning "Install applications failed: ${error_apps[*]}"
    return 1
  else
    return 0
  fi
}

function _cmd_do_deploy {
  if backup_is_spec_app "${appid}"; then
    return
  fi
  if [ x"${appuser}" != x'system' ] && backup_is_app_in_system_user "${appid}"; then
    # this is a system application, don't install here
    msg2 "ignore app ${appid}: should also exists in \$system_apps"
    return
  fi

  # install app
  run_appline_hook "deploy" "pre" "app" "user_${appuser}:${appid}"
  if ! (
      local fdroid_arg_install_sys= fdroid_arg_install_path=

      # overwrite variables
      if [ -n "${appline_options[repo]}" ]; then
        local repo_url="${fdroid_repos[${appline_options[repo]}]}"
        fdroid_repos=(["${appline_options[repo]}"]="${repo_url}")
      fi
      if [ x"${appuser}" = x"system" ]; then
        fdroid_arg_install_sys=1
        fdroid_arg_users=()
      else
        fdroid_arg_users=("${appuser}")
      fi
      if [ -n "${appline_options[install_path]}" ]; then
        fdroid_arg_install_path="${appline_options[install_path]}"
      fi
      if [ -n "${appline_options[allow_unstable]}" ]; then
        fdroid_opt_allow_unstable="${appline_options[allow_unstable]}"
      fi
      local install_target="${appid}=${appline_options[version_code]}"
      if [ -n "${appline_options[appfile]}" ]; then
        install_target="${appline_options[appfile]}"
      fi
      if [ -n "${fdroid_opt_verbose}" ]; then
        local repo_names="${!fdroid_repos[@]}"
        msg2 "cmd_install ${install_target} repos=(${repo_names}) install_sys=${fdroid_arg_install_sys} install_path=${fdroid_arg_install_path}"
      fi
      local backup_arg_profile=
      cmd_install "${install_target}"
    ); then
    error_apps+=("user_${appuser}:${appid}")
  else
    new_app_installed_in_prev_user=1
  fi
  run_appline_hook "deploy" "post" "app" "user_${appuser}:${appid}"
}

function _cmd_usage_cleanup {
  echo "cleanup [--options options] [--filter filter] [--disable] [--enable] <--profile file.sh|APPLINE..>
    cleanup applications from profile
    --disable, disable applications instead of uninstalling them
    --enable, enable applications instead of uninstalling them"
}
function cmd_cleanup {
  declare -A cleanup_pre_app_hooks
  declare -A cleanup_post_app_hooks
  declare -A cleanup_file_funcs
  local error_apps=()

  local profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=()
  backup_prepare_args "${@}"

  run_cmd_hook "cleanup" "pre"

  msg "Uninstall applications"

  local appline=
  for appline in "${system_apps[@]}" "${user_apps[@]}"; do
    local appuser= appid=; unset appline_options; declare -A appline_options
    backup_parse_appline "${appline}"
    if contains "${appuser}" "${users[@]}"; then
      _cmd_do_cleanup
    fi
  done

  run_file_funcs "cleanup"
  run_cmd_hook "cleanup" "post"
  if [ ${#error_apps[@]} -ne 0 ]; then
    warning "Uninstall applications failed: ${error_apps[*]}"
    return 1
  else
    return 0
  fi
}

function _cmd_do_cleanup {
  if [ x"${appuser}" != x'system' ] && ! adb_is-user-exists "${appuser}"; then
    msg2 "ignore app user_${appuser}:${appid}: user ${appuser} not exists"
    return
  fi

  if backup_is_spec_app "${appid}"; then
    return
  fi
  if [ x"${appuser}" != x'system' ] && backup_is_app_in_system_user "${appid}"; then
    # this is a system application, don't uninstall here
    msg2 "ignore app ${appid}: should also exists in \$system_apps"
    return
  fi

  # uninstall app
  run_appline_hook "cleanup" "pre" "app" "user_${appuser}:${appid}"
  if ! (
      local user= fdroid_arg_users=()
      if [ x"${appuser}" = x"system" ]; then
        user=
        fdroid_arg_users=()
      else
        user="${appuser}"
        fdroid_arg_users=("${appuser}")
      fi
      local backup_arg_profile=
      if [ x"${appline_options[type]}" = x"data" ]; then
        # cleanup app data only
        adb_clear-package "${appid}" "${user}"
      else
        # cleanup app
        if [ -n "${deploy_arg_disable}" ]; then
          adb_disable "${appid}" "${user}"
        elif [ -n "${deploy_arg_enable}" ]; then
          adb_enable "${appid}" "${user}"
        else
          cmd_uninstall "${appid}"
        fi
      fi
    ); then
    error_apps+=("user_${appuser}:${appid}")
  fi
  run_appline_hook "cleanup" "post" "app" "user_${appuser}:${appid}"
}

###*** Dispatch deploy arguments

deploy_arg_disable=
deploy_arg_enable=

declare -a deloy_left_args=()
function deploy_dispatch_args {
  while [ ${#} -gt 0 ]; do
    case "${1}" in
      --disable) deploy_arg_disable=1; shift;;
      --enable) deploy_arg_enable=1; shift;;
      *) deploy_left_args+=("${1}"); shift;;
    esac
  done
}
