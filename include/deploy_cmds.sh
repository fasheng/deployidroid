# Depends: utils.sh, adb_utils.sh, backup_cmds.sh, fdroid_cmds.sh

###*** Deploy commands

# Configurable variables, could override in host script or config file
deploy_files_tmpdir='.'

# arg1: fileline, the format is 'local-or-remote-file-url::hash=sha1:xxx;download_dir=.;download_filename=xxx;unpack_dir=.;push_dir=/sdcard;push_filename=xxx'
# for example:
# - download remote-file-url as ./tmp/123.txt:
#   'http://remote-file-url::hash=md5:xxx;download_dir=./tmp;download_filename=123.txt'
# - download remote-archive-url and unpack as ./tmp/123.txt:
#   'http://remote-archive-url::hash=md5:xxx;download_dir=./tmp;download_filename=123.txt.gz;unpack_dir=./tmp'
# - download remote-file-url and push as /sdcard/123.t# './local-file::hash=md5:xxx;push_dir=/sdcard' # will push ./local-file to /sdcard:
#   'http://remote-file-url::hash=md5:xxx;push_dir=/sdcard;push_filename=123.txt'
function deploy_parse_fileline {
  local fileline="${1}"
  fileline_options=()
  fileline_options[file_url]="${fileline%%::*}"
  local options_str="${fileline#*::}"
  if [ -n "${options_str}" ]; then
    local hash= git_commit= git_recursive= git_depth= download_dir= download_filename= download_clean= unpack_dir= unpack_clean= push_dir= push_filename=
    safe_eval "${options_str}"
    if [ -n "${hash}" ]; then fileline_options[hash]="${hash}"; fi
    if [ -n "${git_commit}" ]; then fileline_options[git_commit]="${git_commit}"; fi
    if [ -n "${git_recursive}" ]; then fileline_options[git_recursive]="${git_recursive}"; fi
    if [ -n "${git_depth}" ]; then fileline_options[git_depth]="${git_depth}"; fi
    if [ -n "${download_dir}" ]; then fileline_options[download_dir]="${download_dir}"; fi
    if [ -n "${download_filename}" ]; then fileline_options[download_filename]="${download_filename}"; fi
    if [ -n "${download_clean}" ]; then fileline_options[download_clean]="${download_clean}"; fi
    if [ -n "${unpack_dir}" ]; then fileline_options[unpack_dir]="${unpack_dir}"; fi
    if [ -n "${unpack_clean}" ]; then fileline_options[unpack_clean]="${unpack_clean}"; fi
    if [ -n "${push_dir}" ]; then fileline_options[push_dir]="${push_dir}"; fi
    if [ -n "${push_filename}" ]; then fileline_options[push_filename]="${push_filename}"; fi
  fi

  if [ -z "${fileline_options[download_dir]}" ]; then
    fileline_options[download_dir]="${files_tmpdir:-${deploy_files_tmpdir}}"
  fi

  local fixed_url=$(decode_url "${fileline_options[file_url]}")
  local filename=$(basename "${fixed_url}")
  if [ -z "${fileline_options[download_filename]}" ]; then
    fileline_options[download_filename]="${filename}"
  fi

  if [ -n "${fileline_options[push_dir]}" -a -z "${fileline_options[push_filename]}" ]; then
    fileline_options[push_filename]="${filename}"
  fi
}

# arg1-n: filelines
function deploy_files {
  msg "Deploy files"
  msg2 "files_tmpdir: ${files_tmpdir}"

  local fileline=; unset fileline_options; local -A fileline_options
  for fileline in "$@"; do
    deploy_parse_fileline "${fileline}"

    msg2 "dispatch file: ${fileline_options[file_url]}"

    run_fileline_hook "deploy" "pre"

    local local_file=
    local url_proto="${fileline_options[file_url]%%://*}"
    if [ "${url_proto}" = "${fileline_options[file_url]}" ]; then
      # for a local file, ignore the download operation
      local_file="${fileline_options[file_url]}"
      if ! check_file_hash "${local_file}" "${fileline_options[hash]}"; then
        abort "check file hash failed: ${local_file} ${fileline_options[hash]}"
      fi
    else
      # for a url, download if need
      if [ -n "${fileline_options[download_clean]}" ]; then
        msg2 "download file: clean old ${fileline_options[download_dir]}/${fileline_options[download_filename]} if exists"
        rm -rf "${fileline_options[download_dir]}/${fileline_options[download_filename]}"
      fi

      local vcs_proto="${url_proto%+*}"
      local vcs_url="${fileline_options[file_url]#*+}"
      if [ "${vcs_proto}" != "${url_proto}" ]; then
        case "${vcs_proto}" in
          git)
            git_clone "${vcs_url}" "${fileline_options[download_dir]}/${fileline_options[download_filename]}" "${fileline_options[git_commit]}" "${fileline_options[git_recursive]}" "${fileline_options[git_depth]}"
            ;;
          *)
            warning "Unknown vcs protocol ${vcs_proto}: ${fileline_options[file_url]}"
            return 1
            ;;
        esac
      else
        local_file="${fileline_options[download_dir]}/${fileline_options[download_filename]}"
        if check_file_hash "${local_file}" "${fileline_options[hash]}"; then
          msg2 "ignore dowanload file: ${local_file} already exists in local"
        else
          # download when local file not exists or check file hash failed
          download_file "${fileline_options[file_url]}" "${fileline_options[download_dir]}" "${fileline_options[download_filename]}"
          if ! check_file_hash "${local_file}" "${fileline_options[hash]}"; then
            abort "check file hash failed: ${local_file} ${fileline_options[hash]}"
          fi
        fi
      fi
    fi

    # unpack a archive if unpack_dir defined
    if [ -n "${fileline_options[unpack_dir]}" ]; then
      if [ -z "${deploy_arg_files_ignore_unpack}" ]; then
        msg2 "unpack file: ${local_file} ${fileline_options[unpack_dir]}"
        if [ -n "${fileline_options[unpack_clean]}" ]; then
          msg2 "unpack file: clean ${fileline_options[unpack_dir]} if exists"
          rm -rf "${fileline_options[unpack_dir]}"
        fi
        unpack_file "${local_file}" "${fileline_options[unpack_dir]}"
      else
        msg2 "ignore unpack file: ${local_file}"
      fi
    fi

    # push file to device if push_dir defined
    if [ -n "${fileline_options[push_dir]}" ]; then
      # if push_dir is not empty, try to push the local_file to android device
      local device_file="${fileline_options[push_dir]}/${fileline_options[push_filename]}"

      # push file the device when the file not exists or already exists but check the hash failed
      if adb_is-file-exists "${device_file}" && adb_check-file-hash "${device_file}" "${fileline_options[hash]}"; then
        msg2 "ignore push file: ${device_file} already exists in device"
      else
        if adb_ensure-mount-rw "${device_file}" 1; then
          device_file=$(adb_get-mount-rw-path "${device_file}")
          msg2 "push file to device: ${device_file}"
          adb_push "${local_file}" "${device_file}" 1
        fi
      fi
    fi

    run_fileline_hook "deploy" "post"
  done
}

# arg1-n: filelines
function cleanup_files {
  msg "Cleanup files"

  local fileline=; unset fileline_options; local -A fileline_options
  for fileline in "$@"; do
    deploy_parse_fileline "${fileline}"

    msg2 "dispatch file: ${fileline_options[file_url]}"

    run_fileline_hook "cleanup" "pre"

    # remove device file if push_dir defined
    if [ -n "${fileline_options[push_dir]}" ]; then
      local device_file="${fileline_options[push_dir]}/${fileline_options[push_filename]}"

      # push file the device when the file not exists or already exists but check the hash failed
      if adb_is-file-exists "${device_file}"; then
        msg2 "remove file in device: ${device_file}"
        if adb_ensure-mount-rw "${device_file}" 1; then
          device_file=$(adb_get-mount-rw-path "${device_file}")
          adb_autoshell "${adb_busybox} rm -r ${device_file}"
        fi
      else
        msg2 "ignore remove file: ${device_file} not exists in device"
      fi
    fi

    run_fileline_hook "cleanup" "post"
  done
}

function _cmd_usage_deploy {
  echo "deploy [--options options] [--password pwd] [--filter filter] [--wait-sys-app seconds] [--device-tmpdir dir] [--archive-side value] [--files-ignore-unpack] [--ignore-check-profile] [--ignore-check-timestamp] <--profile file.sh|APPLINE..>
    deploy applications from profile
    --files-ignore-unpack, ignore unpack operation when prepare files"
}
function cmd_deploy {
  local -A deploy_device_file_funcs
  local error_apps=()

  local profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=() files=() files_tmpdir= support_modes=() support_arches=() support_devices=()
  backup_prepare_args "${@}"

  run_cmd_hook "deploy" "pre"
  deploy_files "${files[@]}"
  run_cmd_hook "deploy"

  # ensure users exists
  local user= user_name=
  for user in "${users[@]}"; do
    local -n user_name="user_${user}_name"
    if [ "${user}" != "system" ]; then
      adb_ensure-user-exists "${user}" "${user_name}"
    fi
  done

  local new_app_installed_in_prev_user=
  for user in "${users[@]}"; do
    local user_datadir="${datadir}/user_${user}"
    msg "Install applications for user ${user}"

    # if new application install in previous user, just update package cache
    if [ -n "${new_app_installed_in_prev_user}" ]; then
      _adb_fetch_pkgs_cache
      new_app_installed_in_prev_user=
    fi

    local appline=
    for appline in "${system_apps[@]}" "${user_apps[@]}"; do
      local appuser= appid=; unset appline_options; local -A appline_options
      backup_parse_appline "${appline}"
      if [ "${appuser}" = "${user}" ]; then
        if [[ "${appline_options[type]}" =~ "app" ]]; then
          # deploy app
          _cmd_do_deploy
        fi
        if [[ "${appline_options[type]}" =~ "data" ]] && [ -n "${datadir}" -o -n "${appline_options[datadir]}" -o -n "${appline_options[datafile]}" ]; then
          # restore app data
          if [[ "${appline_options[type]}" =~ "app" ]]; then
            _adb_fetch_pkgs_cache
          fi
          appline_options[type]="data"
          _cmd_do_restore
        fi
      fi
    done
  done

  run_device_file_funcs "deploy"
  run_cmd_hook "deploy" "post"
  if [ ${#error_apps[@]} -ne 0 ]; then
    warning "Install applications failed: ${error_apps[*]}"
    return 1
  else
    return 0
  fi
}

function _cmd_do_deploy {
  if backup_is_spec_app "${appid}"; then
    return
  fi
  if [ "${appuser}" != 'system' ] && backup_is_app_in_system_user "${appid}"; then
    # this is a system application, don't install here
    msg2 "ignore app ${appid}: should also exists in \$system_apps"
    return
  fi

  # install app
  run_appline_hook "deploy" "pre" "app" "user_${appuser}:${appid}"
  if ! (
      local fdroid_arg_install_sys= fdroid_arg_install_path=

      if [ -n "${appline_options[repo]}" ]; then
        # limit repo
        local fdroid_arg_repos=("${appline_options[repo]}")
      fi
      fdroid_arg_users=("${appuser}")
      if [ "${appuser}" = "system" ]; then
        fdroid_arg_install_sys=1
      fi
      if [ -n "${appline_options[install_path]}" ]; then
        fdroid_arg_install_path="${appline_options[install_path]}"
      fi
      if [ -n "${appline_options[allow_unstable]}" ]; then
        fdroid_allow_unstable="${appline_options[allow_unstable]}"
      fi
      local install_target="${appid}=${appline_options[version_code]}"
      if [ -n "${appline_options[appfile]}" ]; then
        install_target="${appline_options[appfile]}"
      fi
      if [ -n "${fdroid_verbose}" ]; then
        msg2 "cmd_install ${install_target} repos=($(fdroid_get_repo_names | join_lines)) install_sys=${fdroid_arg_install_sys} install_path=${fdroid_arg_install_path}"
      fi
      local backup_arg_profile=
      cmd_install "${install_target}"
    ); then
    error_apps+=("user_${appuser}:${appid}")
  else
    new_app_installed_in_prev_user=1
  fi
  run_appline_hook "deploy" "post" "app" "user_${appuser}:${appid}"
}

function _cmd_usage_disable {
  echo "disable [--options options] [--filter filter] [--ignore-check-profile] <--profile file.sh|BACKUPAPPLINE..>
    disable applications from profile or arguments"
}
function cmd_disable {
  mixcmd_cleanup "disable" "$@"
}

function _cmd_usage_enable {
  echo "enable [--options options] [--filter filter] [--ignore-check-profile] <--profile file.sh|BACKUPAPPLINE..>
    enable applications from profile or arguments"
}
function cmd_enable {
  mixcmd_cleanup "enable" "$@"
}

function _cmd_usage_cleanup {
  echo "cleanup [--options options] [--filter filter] [--options options] <--profile file.sh|BACKUPAPPLINE..>
    cleanup applications from profile or arguments"
}
function cmd_cleanup {
  mixcmd_cleanup "cleanup" "$@"
}

function mixcmd_cleanup {
  local subcmd="${1}"; shift

  local -A cleanup_file_funcs
  local error_apps=()

  local profile_dir= name= version= appdir= datadir= users=() system_apps=() user_apps=() files=() files_tmpdir support_modes=() support_arches=() support_devices=()
  backup_prepare_args "${@}"

  run_cmd_hook "${subcmd}" "pre"
  cleanup_files "${files[@]}"
  run_cmd_hook "${subcmd}"

  if [ "${#system_apps[@]}" -gt 0 ] || [ "${#user_apps[@]}" -gt 0 ]; then
    if [ "${subcmd}" = "cleanup" ]; then
      msg "Uninstall applications"
    elif [ "${subcmd}" = "disable" ]; then
      msg "Disable applications"
    elif [ "${subcmd}" = "enable" ]; then
      msg "Enable applications"
    fi

    local appline=
    for appline in "${system_apps[@]}" "${user_apps[@]}"; do
      local appuser= appid=; unset appline_options; local -A appline_options
      backup_parse_appline "${appline}"
      if contains "${appuser}" "${users[@]}"; then
        _mixcmd_do_cleanup "${subcmd}"
      fi
    done
  fi

  run_device_file_funcs "${subcmd}"
  run_cmd_hook "${subcmd}" "post"

  if [ ${#error_apps[@]} -ne 0 ]; then
    if [ "${subcmd}" = "cleanup" ]; then
      warning "Uninstall applications failed: ${error_apps[*]}"
    elif [ "${subcmd}" = "disable" ]; then
      warning "Disable applications failed: ${error_apps[*]}"
    elif [ "${subcmd}" = "enable" ]; then
      warning "Enable applications failed: ${error_apps[*]}"
    fi
    return 1
  else
    return 0
  fi
}

function _mixcmd_do_cleanup {
  if [ "${appuser}" != 'system' ] && ! adb_is-user-exists "${appuser}"; then
    msg2 "ignore app user_${appuser}:${appid}: user ${appuser} not exists"
    return
  fi

  if backup_is_spec_app "${appid}"; then
    return
  fi
  if [ "${appuser}" != 'system' ] && backup_is_app_in_system_user "${appid}"; then
    # this is a system application, don't uninstall here
    msg2 "ignore app ${appid}: should also exists in \$system_apps"
    return
  fi

  run_appline_hook "${subcmd}" "pre" "app" "user_${appuser}:${appid}"
  if ! (
      # prepare user args
      local user= fdroid_arg_users=()
      if [ "${appuser}" = "system" ]; then
        user=
        fdroid_arg_users=()
      else
        user="${appuser}"
        fdroid_arg_users=("${appuser}")
      fi
      local backup_arg_profile=

      # uninstall/disable/enable app
      if [ "${subcmd}" = "cleanup" ]; then
        if [ "${appline_options[type]}" = "data" ]; then
          # cleanup app data only
          adb_clear-package "${appid}" "${user}"
        else
          # uninstall app
          cmd_uninstall "${appid}"
        fi
      elif [ "${subcmd}" = "disable" ]; then
        adb_disable-package "${appid}" "${user}"
      elif [ "${subcmd}" = "enable" ]; then
        adb_enable-package "${appid}" "${user}"
      fi
    ); then
    error_apps+=("user_${appuser}:${appid}")
  fi
  run_appline_hook "${subcmd}" "post" "app" "user_${appuser}:${appid}"
}

###*** Dispatch deploy arguments

deploy_arg_files_ignore_unpack=

declare -a deloy_left_args=()
function deploy_dispatch_args {
  while [ ${#} -gt 0 ]; do
    case "${1}" in
      --files-ignore-unpack) deploy_arg_files_ignore_unpack=1; shift;;
      *) deploy_left_args+=("${1}"); shift;;
    esac
  done
}
