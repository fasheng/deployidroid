declare -A utils_depends=(
  [realpath]=''
  [wc]=''
  [awk]=''
  [mktemp]=''
  [sha256sum]=''
  [wget]=''
  [7z]=''
  [openssl]=''
)

# Constant variables
UTILS_OPENSSL_DEFAULT_OPTIONS="--aes-256-cbc -md sha512 -pbkdf2 -iter 100000"

# Configurable variables, could override in host script or config file

utils_ignore_warning=; declare -n ignore_warning=utils_ignore_warning
utils_openssl_options="${UTILS_OPENSSL_DEFAULT_OPTIONS}"
utils_ignore_pause=; declare -n ignore_pause=utils_ignore_pause

# arg1: dest file, arg2: url
utils_download_cmd='wget -c --tries=3 --waitretry=5 --quiet --show-progress -O "%s" "%s"'
# NOTE: for curl, url could not contains space
# utils_download_cmd='curl -fLC - --retry 3 --retry-delay 5 -o "%s" "%s"'
# utils_download_cmd='aria2c -c --max-tries=3 --retry-wait=5 -o "%s" "%s"'

# arg1: archive file, arg2: dest dir
utils_unpack_cmd='7z x -aoa -bso0 "%s" -o"%s"'
# utils_unpack_cmd='unar -f -D "%s" -o "%s"'
utils_unpack_tar_cmd='7z x -so "%s" | 7z x -si -aoa -bso0 -ttar -o"%s"'

# arg1: archive file, arg2: files to pack
utils_pack_cmd='7z a -bso0 "%s" %s'

# arg1: files to pack, arg2: archive file
utils_pack_tar_cmd='tar -chf - %s | 7z a -si -bso0 "%s"'

###*** Util functions

###**** Message
function msg {
  local mesg="${1}"; shift
  if [ "${#@}" -gt 0 ]; then
    printf "${GREEN}==>${ALL_OFF}${BOLD} ${mesg}${ALL_OFF}\n" "${@}"
  else
    printf "${GREEN}==>${ALL_OFF}${BOLD} %s${ALL_OFF}\n" "${mesg}"
  fi
}

function msg2 {
  local mesg="${1}"; shift
  if [ "${#@}" -gt 0 ]; then
    printf "${BLUE}  ->${ALL_OFF}${BOLD} ${mesg}${ALL_OFF}\n" "${@}"
  else
    printf "${BLUE}  ->${ALL_OFF}${BOLD} %s${ALL_OFF}\n" "${mesg}"
  fi
}

function warning {
  if [ -z "${ignore_warning}" ]; then
    local mesg="${1}"; shift
    if [ "${#@}" -gt 0 ]; then
      printf "${YELLOW}==> WARNING:${ALL_OFF}${BOLD} ${mesg}${ALL_OFF}\n" "${@}"
    else
      printf "${YELLOW}==> WARNING:${ALL_OFF}${BOLD} %s${ALL_OFF}\n" "${mesg}"
    fi
  fi
}

function error {
  local mesg="${1}"; shift
  if [ "${#@}" -gt 0 ]; then
    printf "${RED}==> ERROR:${ALL_OFF}${BOLD} ${mesg}${ALL_OFF}\n" "${@}"
  else
    printf "${RED}==> ERROR:${ALL_OFF}${BOLD} %s${ALL_OFF}\n" "${mesg}"
  fi
}

function abort {
  error "${@}"
  error "Aborting..."
  exit 1
}

function enable_colors {
  unset ALL_OFF BOLD BLUE GREEN RED YELLOW
  if [ -t 2 ] && [ "$USE_COLOR" != "n" ]; then
    # prefer terminal safe colored and bold text when tput is supported
    if tput setaf 0 &>/dev/null; then
      ALL_OFF="$(tput sgr0)"
      BOLD="$(tput bold)"
      BLUE="${BOLD}$(tput setaf 4)"
      GREEN="${BOLD}$(tput setaf 2)"
      RED="${BOLD}$(tput setaf 1)"
      YELLOW="${BOLD}$(tput setaf 3)"
    else
      ALL_OFF="\e[0m"
      BOLD="\e[1m"
      BLUE="${BOLD}\e[34m"
      GREEN="${BOLD}\e[32m"
      RED="${BOLD}\e[31m"
      YELLOW="${BOLD}\e[33m"
    fi
  fi
  # readonly ALL_OFF BOLD BLUE GREEN RED YELLOW
}

###**** Basic
function pause {
  [ -n "${ignore_pause}" ] && { msg2 "ignore pause"; return; }
  read -n1 -p "Press any key to continue..."
}

function read_password {
  stty -echo
  read password
  stty echo
  echo "${password}"
}

function has_input {
  read -t 0
}

function has_pipe_input {
  [ -p /dev/stdin ]
}

# test if index array contains item
# arg1: item to match, argN: array
function contains {
  local match="${1}"; shift
  local e
  for e; do [ "${e}" = "${match}" ] && return 0; done
  return 1
}

# remove item from index array that match the pattern
# arg1: <array variable name>, arg2: <pattern>, arg3: [message]
function remove_array_match {
  local -n array="${1}"
  local pattern="${2}"
  local message="${3}"
  local i= item=
  for i in "${!array[@]}"; do
    item="${array[i]}"
    if [[ "${item}" =~ ${pattern} ]]; then
      [ -n "${message}" ] && msg2 "${message}" "${item}"
      unset array[i]
    fi
  done
}

# remove item from index array that unmatch the pattern
# arg1: <array variable name>, arg2: <pattern>, arg3: [message]
function remove_array_unmatch {
  local -n array="${1}"
  local pattern="${2}"
  local message="${3}"
  local i= item=
  for i in "${!array[@]}"; do
    item="${array[i]}"
    if [[ ! "${item}" =~ ${pattern} ]]; then
      [ -n "${message}" ] && msg2 "${message}" "${item}"
      unset array[i]
    fi
  done
}

# replace item content from index array that match the pattern
# arg1: <array variable name>, arg2: <pattern>, arg4: <replace>, arg4: [message]
function replace_array {
  local -n array="${1}"
  local pattern="${2}"
  local replace="${3}"
  local message="${4}"
  local i= item= new_item=
  for i in "${!array[@]}"; do
    item="${array[i]}"
    new_item="${item//${pattern}/${replace}}"
    if [ "${item}" != "${new_item}" ]; then
      [ -n "${message}" ] && msg2 "${message}" "${item}" "${new_item}"
      array[i]="${new_item}"
    fi
  done
}

function is_cmd_exists {
  if type "${1}" &>/dev/null || type -a "${1}" &>/dev/null; then
    return 0
  else
    return 1
  fi
}

function is_func_exists {
  local t=$(type -t "${1}")
  if [ "${t}" = 'function' ]; then
    return 0
  else
    return 1
  fi
}

function is_var_exists {
  local vars=($(eval "echo \${!$1@}"))
  local v=
  for v in "${vars[@]}"; do
    if [ "${v}" = "${1}" ]; then
      return 0
    fi
  done
  return 1
}

function msg_eval {
  # redirect to stderr to prevent influence of result
  [ -n "${arg_verbose}" ] && msg2 "eval: $*" 1>&2
  eval "$@"
}

###**** String
# arg1: string to quote
# for example: '"123"' -> '\"123\"'
function quote {
  printf "%q" "${1}"
}

# arg1: string to unquote
# for example: '\"123\"' -> '"123"'
function unquote {
  echo "${1}" | xargs printf '%b\n'
}

function decode_url {
  local url_encoded="${1//+/ }"
  printf '%b' "${url_encoded//%/\\x}"
}

# for normal function: arg1: content, arg2(optional): separator
# for stdin pipeline: arg1(optional): separator
join_lines_depend_funcs=(has_pipe_input)
function join_lines {
  local IFS=$'\n'
  local result=
  local l= first=1
  if has_pipe_input; then
    local separator="${1:- }"
    while read l; do
      if [ -n "${first}" ]; then
        first=
        result="${l}"
      else
        result="${result}${separator}${l}"
      fi
    done
  elif [ -n "${1}" ]; then
    local separator="${2:- }"
    for l in ${1}; do
      if [ -n "${first}" ]; then
        first=
        result="${l}"
      else
        result="${result}${separator}${l}"
      fi
    done
  fi
  echo -n "${result}"
}

to_lower_depend_funcs=(has_pipe_input)
function to_lower {
  if has_pipe_input; then
    local l
    while read l; do
      echo "${l}" | tr 'A-Z' 'a-z'
    done
  elif [ -n "${1}" ]; then
    echo "${@}" | tr 'A-Z' 'a-z'
  fi
}

to_upper_depend_funcs=(has_pipe_input)
function to_upper {
  if has_pipe_input; then
    local l
    while read l; do
      echo "${l}" | tr 'a-z' 'A-Z'
    done
  elif [ -n "${1}" ]; then
    echo "${@}" | tr 'a-z' 'A-Z'
  fi
}

trim_left_depend_funcs=(has_pipe_input)
function trim_left {
  if has_pipe_input; then
    local IFS=$'\n'
    local l
    while read l; do
      local var="${l}"
      echo "${var#"${var%%[![:space:]]*}"}"
    done
  elif [ -n "${1}" ]; then
    local var="$*"
    echo "${var#"${var%%[![:space:]]*}"}"
  fi
}

###**** Safe eval
# safe_eval will never eval executed code like $() in string
# arg1: eval code, arg2-n: if not empty, only the variale names defined here will be parsed and defined (optional)
safe_eval_depend_funcs=(contains unquote)
function safe_eval {
  local code="${1}"; shift
  local IFS=$'\n'
  for i in $(echo "${code}" | sed 's/;/\n/g'); do
    local var_name=$(echo "${i%%=*}" | trim_left)
    local var_value="${i#*=}"
    if [ "$#" -eq 0 ] || contains "${var_name}" "$@"; then
      local ref_name=
      local -n ref_name="${var_name}"
      ref_name="$(unquote ${var_value})"
    fi
  done
}

# arg1: target script file
safe_source_depend_funcs=(safe_eval)
function safe_source {
  safe_eval "$(cat "${1}")"
}

###**** Compare
# compare two string versions(like 0.9.5), if two version equals, return 0, if version 1 greater, return 1, if version 2 greater, return 2
# arg1: version string 1
# arg2: version string 2
function compver {
  local IFS=.
  local i ver1=(${1}) ver2=(${2})

  for ((i=0; i<"${#ver1[@]}"; i++)); do
    if [ -z "${ver2[i]}" ]; then
      return 1
    fi
    if [ "${ver1[i]}" -gt "${ver2[i]}" ]; then
      return 1
    fi
    if [ "${ver1[i]}" -lt "${ver2[i]}" ]; then
      return 2
    fi
  done
  if [ -n "${ver2[i]}" ]; then
    return 2
  fi
  return 0
}

compver_eq_depend_funcs=(compver)
function compver_eq {
  case $(compver "${1}" "${2}"; echo "${?}") in
    0) return 0;;
  esac
  return 1
}
compver_gt_depend_funcs=(compver)
function compver_gt {
  case $(compver "${1}" "${2}"; echo "${?}") in
    1) return 0;;
  esac
  return 1
}
compver_gt_depend_funcs=(compver)
function compver_ge {
  case $(compver "${1}" "${2}"; echo "${?}") in
    0|1) return 0;;
  esac
  return 1
}
compver_lt_depend_funcs=(compver)
function compver_lt {
  case $(compver "${1}" "${2}"; echo "${?}") in
    2) return 0;;
  esac
  return 1
}
compver_le_depend_funcs=(compver)
function compver_le {
  case $(compver "${1}" "${2}"; echo "${?}") in
    0|2) return 0;;
  esac
  return 1
}

###**** Search
# dump_section_headless <file> <begin search string> <end search string>
function dump_section_headless {
  local file="${1}"
  local begin="${2}"
  local end="${3}"
  awk -v begin="${begin}" -v end="${end}" 'BEGIN{in_section=0} $0 ~ end{if (in_section==1) exit} {if (in_section==1) print} $0 ~ begin{in_section=1}' "${file}"
}

###**** File
function is_abs_path {
  case "${1}" in
    ~|~/*|/*) return 0 ;;
  esac
  return 1
}

# arg1: relative-path, arg2(optional): base-dir
get_realpath_depend_funcs=(is_abs_path)
function get_realpath {
  local rel_path="${1}"
  if is_abs_path "${rel_path}"; then
    echo "${rel_path}"
    return
  fi

  local base_dir="${2:-.}"
  local base_abs_dir=$(realpath -s "${base_dir}")
  local abs_path=
  if [ -e "${base_abs_dir}/${rel_path}" ]; then
    abs_path=$(realpath -s "${base_abs_dir}/${rel_path}")
  else
    abs_path="${base_abs_dir}/${rel_path}"
  fi
  echo "${abs_path}"
}

# arg1: file-or-dir
function is_empty_dir {
  local dir="${1}"
  if [ -f "${dir}" ]; then
    return 1
  fi
  local count=$(find "${dir}" -type f | wc | awk '{print $1}')
  if [ "${count}" -eq 0 ]; then
    return 0
  else
    return 1
  fi
}

# arg1: <dir1>, arg2: <dir2>
# check if dir2 is dir1's child dir, the same dirs also treat as child dir
function is_child_dir {
  local dir1="${1%%/}"
  local dir2="${2%%/}"
  [ "${dir2}" = "${dir1}" ] && return 0
  case "${dir2}" in
    "${dir1}"/*) return 0 ;;
  esac
  return 1
}

function is_text_file {
  file -bL --mime "${1}" | grep -q '^text'
}

# replace_section <file> <begin search string> <end search string> <replacement string>
# copy and fix last line issue from anykernel3/ak3_core.sh
replace_section_depend_funcs=(join_lines)
function replace_section {
  local begin endstr last end
  local replacement="$(join_lines "${4}" '\n')"
  [ -n "${4}" ] && replacement="${replacement}\n"
  begin=$(grep -E -n "${2}" "${1}" | head -n1 | cut -d: -f1)
  if [ "${begin}" ]; then
    if [ "${3}" == " " -o ! "${3}" ]; then
      endstr='^[[:space:]]*$'
    else
      endstr="${3}"
    fi
    last=$(wc -l "${1}" | cut -d\  -f1)
    for end in $(grep -E -n "${endstr}" "${1}" | cut -d: -f1) "${last}"; do
      if [ "${end}" ] && [ "${begin}" -lt "${end}" ]; then
        sed -i "${begin},${end}d" "${1}"
        if [ "${end}" == "$last" ]; then
          echo >> "${1}"
          sed -i "${begin}s|^|${replacement}|" "${1}"
          sed -i '$d' "${1}"
        else
          sed -i "${begin}s|^|${replacement}|" "${1}"
        fi
        break
      fi
    done
  fi
}

# is_section_exists <file> <begin search string> <end search string>
function is_section_exists {
  local begin endstr end
  begin=$(grep -E -n "${2}" "${1}" | head -n1 | cut -d: -f1)
  if [ "${begin}" ]; then
    if [ "${3}" == " " -o ! "${3}" ]; then
      endstr='^[[:space:]]*$'
    else
      endstr="${3}"
    fi
    for end in $(grep -E -n "${endstr}" "${1}" | cut -d: -f1) $last; do
      if [ "${end}" ] && [ "${begin}" -lt "${end}" ]; then
        return
      fi
    done
  fi
  return 1
}

# insert_section <file> <begin search string> <end search string> <content string>
insert_section_depend_funcs=(is_section_exists replace_section)
function insert_section {
  if is_section_exists "${1}" "${2}" "${3}"; then
    replace_section "${1}" "${2}" "${3}" "${2}\n${4}\n${3}"
  else
    echo -e "${2}\n${4}\n${3}" >> "${1}"
  fi
}

# remove_section <file> <begin search string> <end search string>
remove_section_depend_funcs=(replace_section)
function remove_section {
  replace_section "${1}" "${2}" "${3}" ""
}

###**** Download
# arg1: url, arg2: dest dir, arg3: dest filename (optional)
function download_file {
  local url="${1}"
  local dest_dir="${2}"
  local dest_filename="${3}"
  if [ -z "${dest_filename}" ]; then
    local fixed_url=$(decode_url "${url}")
    dest_filename=$(basename "${fixed_url}")
  fi
  local dest_file="${dest_dir}/${dest_filename}"

  mkdir -p "${dest_dir}"
  msg_eval "$(printf "${utils_download_cmd}" "${dest_file}" "${url}")"
}

# arg1: <url>, arg2: <dest-dir>, arg3: [commit-branch-tag], arg4: [recursive], arg5: [depth]
function git_clone {
  local url="${1}"
  local dest_dir="${2}"
  local commit="${3}"
  local recursive="${4}"
  local depth="${5}"

  mkdir -p "${dest_dir}"
  (
    cd "${dest_dir}"
    if [ ! -d .git ] || ! git rev-parse --is-inside-work-tree &>/dev/null; then
      git init
      git remote add origin "${url}"
    fi
    # NOTE: "git fetch <sha1-of-commit>" only works since 2.5.0
    msg_eval "git fetch $([ -n "${depth}" ] && echo --depth "${depth}") origin ${commit}"
    msg_eval "git checkout --force FETCH_HEAD"

    if [ -n "${recursive}" ]; then
      msg_eval "git submodule update --init --remote --recursive $([ -n "${depth}" ] && echo --depth "${depth}")"
    fi
  )
}

###**** Archive
# arg1: archive file name
# determine if a file is archive with the filename extension
# default $utils_pack_cmd is 7z, support .zip .7z .tar .gz .bz2 .xz etc
# default $utils_unpack_cmd is 7z, support .zip .7z .tar .gz .bz2 .xz .rar etc
function is_archive_file {
  if [[ "${1}" =~ .*.(7z|bz2|gz|xz|zip|apk|rar|tar)$ ]]; then
    return
  fi
  return 1
}

function is_tar_archive_file {
  if [[ "${1}" =~ .*.tar.(7z|bz2|gz|xz|zip)$ ]]; then
    return
  fi
  return 1
}

function get_prefer_unpacked_file {
  echo "${1}" | sed -r 's/\.(7z|bz2|gz|xz|zip|apk|rar|tar)$//'
}

# determine if a file is archive that could pack/unpack with pipeline
function is_pipe_archive_file {
  if [[ "${1}" =~ .*.(7z|bz2|gz|xz|zip|apk|tar)$ ]]; then
    return
  fi
  return 1
}

# arg1: output archive file to pack
# pack file and redirect to stdout
function pipe_pack_file {
  local archive_file="${1}"
  local archive_type="${archive_file##*.}"
  local cmd="7z a -si -bso0 '${archive_file}'"

  # if not --verbose exists, hide command result
  [ -z "${arg_verbose}" ] && cmd="${cmd} > /dev/null"

  # special for gzip
  if [ "${archive_type}" = "gz" ]; then
    cmd="gzip > '${archive_file}'"
  fi

  msg_eval "${cmd}"
}

# arg1: input archive file to unpack
# unpack file and redirect to stdout
function pipe_unpack_file {
  # redirect 7z progress line to stderr
  msg_eval "7z x -so -bsp2 '${1}'"
}

# arg1: dest archive file, arg2-n: unpacked file or dir
function pack_file {
  [ -n "${arg_verbose}" ] && msg2 "pack_file $*"
  local archive_file="${1}"; shift
  local unpacked_files=
  for f in "$@"; do
    unpacked_files="${unpacked_files} \"$f\""
  done

  local cmd="$(printf "${utils_pack_cmd}" "${archive_file}" "${unpacked_files}")"
  if is_tar_archive_file "${archive_file}"; then
    cmd="$(printf "${utils_pack_tar_cmd}" "${unpacked_files}" "${archive_file}")"
  fi

  # if not --verbose exists, hide command result
  [ -z "${arg_verbose}" ] && cmd="(${cmd}) > /dev/null"
  msg_eval ${cmd}
}

# arg1: archive file, arg2: dest dir, arg3-n: [filters..]
function unpack_file {
  [ -n "${arg_verbose}" ] && msg2 "unpack_file $*"
  local archive_file="${1}"
  local dest_dir="${2}"
  shift; shift
  local filters="$@"

  if [ ! -f "${archive_file}" ]; then
    warning "File not exists: ${archive_file}"
    return 1
  fi

  local cmd="$(printf "${utils_unpack_cmd}" "${archive_file}" "${dest_dir}")"
  if is_tar_archive_file "${archive_file}"; then
    cmd="$(printf "${utils_unpack_tar_cmd}" "${archive_file}" "${dest_dir}")"
  fi

  if [ -n "${filters}" ]; then
    cmd="${cmd} ${filters}"
  fi

  # if not --verbose exists, hide command result
  [ -z "${arg_verbose}" ] && cmd="(${cmd}) > /dev/null"
  msg_eval ${cmd}
}

###**** Encrypt
# arg1: password, arg2: input file, arg3: output file
function encrypt_file {
  local password="${1}"
  local input_file="${2}"
  local output_file="${3}"

  if [ -z "${password}" ]; then
    warning "Encrypt file failed: need password"
    return 1
  fi

  if [ ! -f "${input_file}" ]; then
    warning "Encrypt file failed: input file ${input_file} not exists"
    return 2
  fi

  [ 0"${arg_verbose}" -ge 2 ] && msg2 "eval: openssl enc ${utils_openssl_options} -k <password> -in "${input_file}" -out "${output_file}""
  if ! openssl enc ${utils_openssl_options} -k "${password}" -in "${input_file}" -out "${output_file}"; then
    warning "Encrypt file failed, error code $?: ${input_file}"
    return 3
  fi
}

# arg1: password, arg2: input file, arg3: output file
function decrypt_file {
  local password="${1}"
  local input_file="${2}"
  local output_file="${3}"

  if [ -z "${password}" ]; then
    warning "Decrypt file failed: need password"
    return 1
  fi

  if [ ! -f "${input_file}" ]; then
    warning "Decrypt file failed: input file ${input_file} not exists"
    return 2
  fi

  [ 0"${arg_verbose}" -ge 2 ] && msg2 "eval: openssl enc ${utils_openssl_options} -d -k <password> -in "${input_file}" -out "${output_file}""
  if ! openssl enc ${utils_openssl_options} -d -k "${password}" -in "${input_file}" -out "${output_file}"; then
    warning "Decrypt file failed, error code $?: ${input_file}"
    return 3
  fi
}

# arg1: target file, arg2: hash type and value, for example 'sha256:xxxx', 'sha1:xxxx', 'md5:xxxx'
function check_file_hash {
  local file="${1}"
  local hash="${2}"
  local hash_type="$(echo ${hash} | awk -F: '{print $1}')"
  local hash_value="$(echo ${hash} | awk -F: '{print $2}')"

  if [ ! -f "${file}" ]; then
    msg2 "file not exists: ${1}"
    return 1
  fi

  if [ -z "${hash}" -o "${hash_type}" = "ignore" -o "${hash_value}" = "ignore" ]; then
    warning "Ignore check hash for ${file}"
    return 0
  fi

  local real_hash=
  if [ "${hash_type}" = "sha256" ]; then
    real_hash="$(sha256sum "${file}" | awk '{print $1}')"
  elif [ "${hash_type}" = "sha1" ]; then
    real_hash="$(sha1sum "${file}" | awk '{print $1}')"
  elif [ "${hash_type}" = "md5" ]; then
    real_hash="$(md5sum "${file}" | awk '{print $1}')"
  else
    warning "Unknown hash type: ${2}"
    return 1
  fi

  if [ "${real_hash}" = "${hash_value}" ]; then
    return 0
  else
    warning "File hash incorrect: ${file} ${hash_type} is ${real_hash} prefer is ${hash_value}"
    return 1
  fi
}
