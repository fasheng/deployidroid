declare -A utils_depends=(
  [realpath]=''
  [wc]=''
  [awk]=''
  [mktemp]=''
)

function msg {
  local mesg="$1"; shift
  printf "${GREEN}==>${ALL_OFF}${BOLD} ${mesg}${ALL_OFF}\n" "${@}" >&2
}

function msg2 {
  local mesg="$1"; shift
  printf "${BLUE}  ->${ALL_OFF}${BOLD} ${mesg}${ALL_OFF}\n" "${@}" >&2
}

function warning {
  if [ -z "${IGNORE_WARN}" ]; then
    local mesg="$1"; shift
    printf "${YELLOW}==> WARNING:${ALL_OFF}${BOLD} ${mesg}${ALL_OFF}\n" "${@}" >&2
  fi
}

function error {
  local mesg="$1"; shift
  printf "${RED}==> ERROR:${ALL_OFF}${BOLD} ${mesg}${ALL_OFF}\n" "${@}" >&2
}

function abort {
  error "${@}"
  error "Aborting..."
  exit 1
}

function enable_colors {
  unset ALL_OFF BOLD BLUE GREEN RED YELLOW
  if [[ -t 2 && $USE_COLOR != "n" ]]; then
    # prefer terminal safe colored and bold text when tput is supported
    if tput setaf 0 &>/dev/null; then
      ALL_OFF="$(tput sgr0)"
      BOLD="$(tput bold)"
      BLUE="${BOLD}$(tput setaf 4)"
      GREEN="${BOLD}$(tput setaf 2)"
      RED="${BOLD}$(tput setaf 1)"
      YELLOW="${BOLD}$(tput setaf 3)"
    else
      ALL_OFF="\e[0m"
      BOLD="\e[1m"
      BLUE="${BOLD}\e[34m"
      GREEN="${BOLD}\e[32m"
      RED="${BOLD}\e[31m"
      YELLOW="${BOLD}\e[33m"
    fi
  fi
  # readonly ALL_OFF BOLD BLUE GREEN RED YELLOW
}

function is_cmd_exists {
  if type -a "${1}" &>/dev/null; then
    return 0
  else
    return 1
  fi
}

function is_func_exists {
  local t=$(type -t "${1}")
  if [ x"${t}" = x'function' ]; then
    return 0
  else
    return 1
  fi
}

function is_var_exists {
  local vars=($(eval "echo \${!$1@}"))
  local v=
  for v in "${vars[@]}"; do
    if [ x"${v}" = x"${1}" ]; then
      return 0
    fi
  done
  return 1
}

function is_abs_path {
  if [[ "${1}" =~ ^~?/ ]]; then
    return 0
  else
    return 1
  fi
}

# arg1: relavive-path, arg2(optional): base-dir
function get_realpath {
  local rel_path="${1}"
  if is_abs_path "${rel_path}"; then
    echo "${rel_path}"
    return
  fi

  local base_dir="${2:-.}"
  local base_abs_dir=$(realpath -s "${base_dir}")
  local abs_path=
  if [ -e "${base_abs_dir}/${rel_path}" ]; then
    abs_path=$(realpath -s "${base_abs_dir}/${rel_path}")
  else
    abs_path="${base_abs_dir}/${rel_path}"
  fi
  echo "${abs_path}"
}

# arg1: file-or-dir
function is_empty_dir {
  local dir="${1}"
  if [ -f "${dir}" ]; then
    return 1
  fi
  local count=$(find "${dir}" -type f | wc | awk '{print $1}')
  if [ "${count}" -eq 0 ]; then
    return 0
  else
    return 1
  fi
}

function read_password {
  stty -echo
  read password
  stty echo
  echo "${password}"
}

# test array contains item
# arg1: item to match, argN: array
function contains {
  local e match="${1}"
  shift
  for e; do [[ "${e}" == "$match" ]] && return 0; done
  return 1
}

# compare two string versions(like 0.9.5), if two version equals, return 0, if version 1 greater, return 1, if version 2 greater, return 2
# arg1: version string 1
# arg2: version string 2
function compver {
  local IFS=.
  local i ver1=(${1}) ver2=(${2})

  for ((i=0; i<"${#ver1[@]}"; i++)); do
    if [ -z "${ver2[i]}" ]; then
      return 1
    fi
    if [ "${ver1[i]}" -gt "${ver2[i]}" ]; then
      return 1
    fi
    if [ "${ver1[i]}" -lt "${ver2[i]}" ]; then
      return 2
    fi
  done
  if [ -n "${ver2[i]}" ]; then
    return 2
  fi
  return 0
}

function compver_eq {
  case $(compver "${1}" "${2}"; echo "${?}") in
    0) return 0;;
  esac
  return 1
}
function compver_gt {
  case $(compver "${1}" "${2}"; echo "${?}") in
    1) return 0;;
  esac
  return 1
}
function compver_ge {
  case $(compver "${1}" "${2}"; echo "${?}") in
    0|1) return 0;;
  esac
  return 1
}
function compver_lt {
  case $(compver "${1}" "${2}"; echo "${?}") in
    2) return 0;;
  esac
  return 1
}
function compver_le {
  case $(compver "${1}" "${2}"; echo "${?}") in
    0|2) return 0;;
  esac
  return 1
}

# dump_section_headless <file> <begin search string> <end search string>
function dump_section_headless {
  local file="${1}"
  local begin="${2}"
  local end="${3}"
  awk -v begin="${begin}" -v end="${end}" 'BEGIN{in_section=0} $0 ~ end{if (in_section==1) exit} {if (in_section==1) print} $0 ~ begin{in_section=1}' "${file}"
}

# insert_section <file> <begin search string> <end search string> <content string>
function insert_section {
  if is_section_exists "$1" "$2" "$3"; then
    replace_section "$1" "$2" "$3" "$2\n$4\n$3"
  else
    echo -e "$2\n$4\n$3" >> "$1"
  fi
}

# is_section_exists <file> <begin search string> <end search string>
function is_section_exists {
  local begin endstr end;
  begin=$(grep -E -n "$2" $1 | head -n1 | cut -d: -f1);
  if [ "$begin" ]; then
    if [ "$3" == " " -o ! "$3" ]; then
      endstr='^[[:space:]]*$';
    else
      endstr="$3";
    fi;
    for end in $(grep -E -n "$endstr" $1 | cut -d: -f1) $last; do
      if [ "$end" ] && [ "$begin" -lt "$end" ]; then
        return;
      fi;
    done;
  fi;
  return 1;
}

# replace_section <file> <begin search string> <end search string> <replacement string>
# copy and fix last line issue from anykernel3/ak3_core.sh
function replace_section {
  local begin endstr last end;
  begin=$(grep -E -n "$2" $1 | head -n1 | cut -d: -f1);
  if [ "$begin" ]; then
    if [ "$3" == " " -o ! "$3" ]; then
      endstr='^[[:space:]]*$';
    else
      endstr="$3";
    fi;
    last=$(wc -l $1 | cut -d\  -f1);
    for end in $(grep -E -n "$endstr" $1 | cut -d: -f1) $last; do
      if [ "$end" ] && [ "$begin" -lt "$end" ]; then
        sed -i "${begin},${end}d" $1;
        if [ "$end" == "$last" ]; then
          echo >> $1;
          sed -i "${begin}s|^|${4}\n|" $1;
          sed -i '$d' $1;
        else
          sed -i "${begin}s|^|${4}\n|" $1;
        fi
        break;
      fi;
    done;
  fi;
}
