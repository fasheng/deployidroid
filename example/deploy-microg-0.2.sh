version='0.9.5'
name='deploy-microg'

fdroid_repos=(
  ["fdroid"]="https://f-droid.org/repo"
  ["microg"]="https://microg.org/fdroid/repo"
)

system_prefix=/system
system_apps=(
  "com.google.android.gms:repo=microg;install_path=${system_prefix}/priv-app;version_code=223616050" # microG Services Core 0.2.25.22361
)

user_apps=(
  "com.android.vending:versioncode=22" # microG Companion 0.1.
  "com.google.android.gsf:versioncode=8" # microG Services Framework Proxy v0.1.
  'org.microg.nlp.backend.ichnaea'   # MozillaNlpBackend
  'org.microg.nlp.backend.apple'     # Apple UnifiedNlp Backend
  'org.microg.nlp.backend.nominatim' # NominatimNlpBackend
)

function post_deploy {
  msg2 "setup microg permissions"
  # phone
  adb_grant-permission com.google.android.gms android.permission.READ_PHONE_STATE
  adb_grant-permission com.google.android.gms android.permission.RECEIVE_SMS # looks not working

  # account
  adb_grant-permission com.google.android.gms android.permission.READ_CONTACTS
  adb_grant-permission com.google.android.gms android.permission.WRITE_CONTACTS
  adb_grant-permission com.google.android.gms android.permission.GET_ACCOUNTS

  # location
  adb_grant-permission com.google.android.gms android.permission.ACCESS_FINE_LOCATION
  adb_grant-permission com.google.android.gms android.permission.ACCESS_COARSE_LOCATION
  adb_grant-permission com.google.android.gms android.permission.ACCESS_BACKGROUND_LOCATION # looks not working

  # storage
  adb_grant-permission com.google.android.gms android.permission.READ_EXTERNAL_STORAGE
  adb_grant-permission com.google.android.gms android.permission.WRITE_EXTERNAL_STORAGE

  # vending
  adb_grant-permission com.android.vending android.permission.FAKE_PACKAGE_SIGNATURE

  msg2 "setup microg nlp backends permissions"
  adb_grant-location-permission org.microg.nlp.backend.ichnaea
  adb_grant-permission org.microg.nlp.backend.ichnaea android.permission.ACCESS_BACKGROUND_LOCATION
  adb_grant-location-permission org.microg.nlp.backend.apple
  adb_grant-permission org.microg.nlp.backend.apple android.permission.ACCESS_BACKGROUND_LOCATION

  warning "NOTE: if any magisk modules enabled, ${system_prefix}/priv-app will be read-only as overlayfs, so the installation will fail!"
  warning "NOTE: deploy this profile again after reboot to grant permissions for microG if need"
}
