version='0.9.5'
name='cleanup-ktouch-i9'
appdir='./app'
datadir='./data/cleanup-ktouch-i9'
# android_version='8.1.0'
# android_api_level=27

system_apps=(
  'com.example:type=app'                 # unknown
  'com.tencent.mm:type=app'              # wechat
  'android.translate.xuedianba:type=app' # 同声翻译
  'com.iflytek.inputmethod:type=app'     # input method
  'jp.co.omronsoft.openwnn:type=app'     # jp im
  'com.browser_llqhz:type=app'           # built-in browser
  'com.adups.fota.sysoper:type=app'      # spy app
  'com.adups.fota:type=app'              # spy app
)

user_apps=(
  'com.example:type=data'                 # unknown
  'com.tencent.mm:type=data'              # wechat
  'android.translate.xuedianba:type=data' # 同声翻译
  'com.iflytek.inputmethod:type=data'     # input method
  'jp.co.omronsoft.openwnn:type=data'     # jp im
  'com.browser_llqhz:type=data'           # built-in browser
  'com.adups.fota.sysoper:type=data'      # spy app
  'com.adups.fota:type=data'              # spy app
)
