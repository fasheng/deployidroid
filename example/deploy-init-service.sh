version='0.9.5'
name='deploy-init-service'

ro_hardware=$(adb_shell "getprop ro.hardware")
vendor_rc_file="/vendor/etc/init/hw/init.${ro_hardware}.rc"

function pre_deploy {
  adb_remount-rw "/vendor/"
}

function my_init_rc {
  local file="${1}"
  local section_begin='### BEGIN'
  local section_end='### END'
  local content="on boot\n    write /data/hello_init \"hello\""
  insert_section "${file}" "${section_begin}" "${section_end}" "${content}"
}
deploy_file_funcs["${vendor_rc_file}"]=my_init_rc

function post_deploy_adb {
  local vendor_rc_file="/vendor/etc/init/hw/init.$(getprop ro.hardware).rc"
  chown 0:0 "${vendor_rc_file}"
  chmod 0644 "${vendor_rc_file}"
  chcon u:object_r:system_file:s0 "${vendor_rc_file}"
}

function post_deploy {
  adb_remount-ro "/vendor/"
}

