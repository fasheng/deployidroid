version='0.9.5'
name='deploy-nethunter'

fdroid_repos=(
  ["nethunter"]="https://store.nethunter.com/repo"
)

system_apps=(
  'com.offsec.nethunter.store.privileged:install_path=/system/priv-app' # NetHunterStore Privileged Extension
)

user_apps=(
  'com.hijacker'                          # Hijacker
  'com.mantz_it.rfanalyzer'               # RF Analyzer
  'com.offsec.nethunter'                  # NetHunter
  'com.offsec.nethunter.store'            # NetHunter Store
  'com.offsec.nhterm'                     # NetHunter Terminal
  'com.offsec.nhvnc'                      # NetHunter VNC
  'com.softwarebakery.drivedroid'         # DriveDroid
  'de.blinkt.openvpn'                     # OpenVPN for Android
  'io.shodan.app'                         # Shodan
  'net.wigle.wigleandroid'                # WiGLE WiFi Wardriving
  'org.csploit.android'                   # cSploit
  'org.exobel.routerkeygen'               # Router Keygen
  'org.pocketworkstation.pckeyboard'      # Hacker's Keyboard
  'remote.hid.keyboard.client'            # USB Keyboard
  'su.sniff.cepter'                       # Intercepter-NG
)
