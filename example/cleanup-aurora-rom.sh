version='0.9.5'
name='cleanup-aurora-rom'
appdir='./app'
datadir='./data/cleanup-aurora-rom'

system_apps=(
  # 'com.aurora.advance.incall.flashlight:type=app'
  # 'com.aurora.advance.settings:type=app'
  # 'com.aurora.battery:type=app'

  # 极光惠购
  'com.dataoke.shoppingguide.app818260:type=app'

  # 推广app
  'com.qihoo360.mobilesafe:type=app'
  'com.qihoo.appstore:type=app'
  'com.wandoujia.phoenix2:type=app'
  'sogou.mobile.explorer:type=app'
  'com.androidmarket.dingzhi:type=app'
)

user_apps=(
  # 3rd apps for user 0
  'com.tencent.mm.wxa.sce:type=app' # WeChat Mini Programs
)
