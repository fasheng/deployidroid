version='0.9.5'
name='deploy-twrp-supersu-system-mode'

# Description:
# Some boards like rockchip don't support standard boot.img, so
# systemless root tools could not work. Thanks SuperSU provide system
# mode, and this script could install it in such devices.

# Usage:
# 1. Reboot to twrp recovery
#    $ adb reboot recovery
# 2. Download SR5-SuperSU-v2.82-SYSTEMMODE.zip from https://forum.xda-developers.com/apps/supersu/v2-64-2-66-supersu-mode-t3286120
# 2. Run this script
#    $ deployidroid deploy --profile ./example/deploy-twrp-supersu-system-mode.sh
# 3. Reboot system

# Works devices:
# - Rockchip RK3328 Android 7.1

function pre_deploy {
  local tmpdir="./"
  local supersu_filename="SR5-SuperSU-v2.82-SYSTEMMODE.zip"
  local device_supersu_file="/sdcard/${supersu_filename}"

  msg2 "download SR5-SuperSU-v2.82-SYSTEMMODE.zip"
  mkdir -p "${tmpdir}"
  cd "${tmpdir}"
  if [ ! -f "${supersu_filename}" ]; then
    abort "${tmpdir}/${supersu_filename} not found, please download from https://forum.xda-developers.com/apps/supersu/v2-64-2-66-supersu-mode-t3286120"
  fi
  adb_push "${supersu_filename}" "${device_supersu_file}"

  # check if in recovery mode
  local twrp_mode=$(adb_shell 'getprop ro.twrp.boot')
  if [ -z "${twrp_mode}" ]; then
    abort "Not in twrp recovery mode"
  fi
}

# run in adb shell
function pre_deploy_adb {
  # install supersu
  twrp install "/sdcard/SR5-SuperSU-v2.82-SYSTEMMODE.zip"

  mount /system
  if [ -f "/system/bin/install-recovery.sh" ]; then
    echo "Fix install-recovery.sh SELinux domain"
    chcon u:object_r:install_recovery_exec:s0 "/system/bin/install-recovery.sh"
  fi
  if [ -f "/system/bin/app_process64_original" ]; then
    echo "Fix app_process or will block on boot"
    rm "/system/bin/app_process" "/system/bin/app_process64"
    mv "/system/bin/app_process64_original" "/system/bin/app_process64"
    ln -s "/system/bin/app_process64" "/system/bin/app_process"
  fi
}
