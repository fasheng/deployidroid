version='0.9.5'
name='deploy-basic'

fdroid_repos=(
  ["fdroid"]="https://f-droid.org/repo"
  ["retroarch"]="https://fdroid.libretro.com/repo"
)
fdroid_allow_unstable=

users=(0 10)
user_10_name='Game'

user_apps=(
  'org.fdroid.fdroid'           # F-Droid
  'com.nextcloud.client'        # Nextcloud Client

  'user_10:org.fdroid.fdroid'                             # F-Droid
  'user_10:com.retroarch:repo=retroarch;allow_unstable=1' # RetroArch
  'user_10:org.ppsspp.ppsspp:version_code=11103'          # PPSSPP
)

# deploy/cleanup pre/post hooks
function pre_deploy {
  msg2 "pre_deploy"
}
function pre_deploy_adb {
  # run in adb shell
  echo "pre_deploy_adb"
}
function post_deploy_adb {
  # run in adb shell
  echo "post_deploy_adb"
}
function post_deploy {
  msg2 "post_deploy"
  adb_grant-storage-permission 'org.fdroid.fdroid'
  adb_disable-autostart 'org.fdroid.fdroid'
  adb_disable-run-background 'org.fdroid.fdroid'
}

# deploy/cleanup file functionss
function my_deploy_file {
  local file="${1}"
  msg2 "my_deploy_file: ${file}"
  echo "Hello World!" > "${file}"
}
function my_deploy_dir {
  local dir="${1}"
  msg2 "my_deploy_dir: ${dir}"
  echo "1" > "${dir}/1"
  echo "2" > "${dir}/2"
  echo "3" > "${dir}/3"
}
deploy_file_funcs['/sdcard/hello_file']=my_deploy_file
deploy_file_funcs['/sdcard/hello_dir/']=my_deploy_dir
