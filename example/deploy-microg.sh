version='0.9.5'
name='deploy-microg'

fdroid_repos=(
  ["fdroid"]="https://f-droid.org/repo"
  ["microg"]="https://microg.org/fdroid/repo"
  ["izzy"]="https://apt.izzysoft.de/fdroid/repo"
)

users=(system 0)

system_apps=(
  'com.google.android.gms:repo=microg;install_dir=/system/priv-app' # microG Services Core
  # 'com.google.android.gms:repo=fdroid;install_dir=/system/priv-app;version_code=20187'  # UnifiedNlp (no GAPPS)
)

user_apps=(
  'com.android.vending'              # FakeStore
  'org.microg.gms.droidguard'        # microG DroidGuard Helper
  'com.google.android.gsf'           # microG Services Framework Proxy
  'org.microg.nlp.backend.ichnaea'   # MozillaNlpBackend
  'org.microg.nlp.backend.nominatim' # NominatimNlpBackend
  'org.microg.nlp.backend.apple'     # Apple UnifiedNlp Backend
  # 'com.gitlab.fasheng.nlp.amap:repo=izzy'      # AMap UnifiedNlp Backend
  # 'com.gitlab.fasheng.nlp.baidu:repo=izzy'     # Baidu UnifiedNlp Backend
)

function post_deploy {
  msg2 "fix permissions"
  adb_grant-location-permission 'org.microg.nlp.backend.ichnaea'
  adb_grant-location-permission 'org.microg.nlp.backend.apple'
  # adb_grant-storage-permission 'com.gitlab.fasheng.nlp.amap'
  # adb_grant-location-permission 'com.gitlab.fasheng.nlp.amap'
  # adb_grant-storage-permission 'com.gitlab.fasheng.nlp.baidu'
  # adb_grant-location-permission 'com.gitlab.fasheng.nlp.baidu'
}
