version='0.9.5'
name='backup-basic'
appdir='./app'
datadir='./data/backup-basic'
android_version='9'
android_api_level=28

users=(system 0)

system_apps=(
  # spec system data
  '@datausage'
  '@bluetooth'
  '@wifi'
  # sys apps
  'com.google.android.gms:type=app;repo=microg;install_dir=/system/priv-app' # microG Services Core
)

user_apps=(
  # spec user data
  '@user.accounts'
  '@user.wallpaper'
  # sys apps data
  'com.google.android.gms:type=data'   # microG Services Core
  # 3rd apps
  'org.fdroid.fdroid:type=app+data'    # F-Droid
  'com.nextcloud.client:type=app+data' # Nextcloud Client
)

# backup/restore pre/post hooks

function pre_backup {
  msg2 "pre_backup"
}
function pre_backup_adb {
  # run in adb shell
  echo "pre_backup_adb"
}
function post_backup_adb {
  # run in adb shell
  echo "post_backup_adb"
}
function post_backup {
  msg2 "post_backup"
  adb_grant-storage-permission 'org.fdroid.fdroid'
  adb_disable-autostart 'org.fdroid.fdroid'
  adb_disable-run-background 'org.fdroid.fdroid'
}

function my_backup_pre_app_hook {
  msg2 "my_backup_pre_app_hook: user_${appuser}:${appid}"
}
function my_backup_post_app_hook {
  msg2 "my_backup_post_app_hook: user_${appuser}:${appid}"
}
function my_backup_pre_data_hook {
  msg2 "my_backup_pre_data_hook: user_${appuser}:${appid}"
}
function my_backup_post_data_hook {
  msg2 "my_backup_post_data_hook: user_${appuser}:${appid}"
}
backup_pre_app_hooks['user_0:org.fdroid.fdroid']=my_backup_pre_app_hook
backup_post_app_hooks['user_0:org.fdroid.fdroid']=my_backup_post_app_hook
backup_pre_data_hooks['user_0']=my_backup_pre_data_hook
backup_post_data_hooks['org.fdroid.fdroid']=my_backup_post_ata_hook
