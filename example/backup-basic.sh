version='0.9.5'
name='backup-basic'
appdir='./app'
datadir='./data/backup-basic'

system_apps=(
  # spec system data
  '@datausage'
  '@bluetooth'
  '@wifi'
  # sys apps
  'com.google.android.gms:type=app;repo=microg;install_path=/system/priv-app' # microG Services Core
)

user_apps=(
  # spec user data
  '@user.accounts'
  '@user.wallpaper'
  # sys apps data
  'com.google.android.gms:type=data'   # microG Services Core
  # 3rd apps
  'org.fdroid.fdroid:type=app+data'    # F-Droid
  'com.nextcloud.client:type=app+data' # Nextcloud Client
)

# backup/restore pre/post hooks

function pre_backup {
  msg2 "pre_backup"
}
function pre_backup_adb {
  # run in adb shell
  echo "pre_backup_adb"
}
function post_backup_adb {
  # run in adb shell
  echo "post_backup_adb"
}
function post_backup {
  msg2 "post_backup"
  adb_grant-storage-permission 'org.fdroid.fdroid'
  adb_disable-autostart 'org.fdroid.fdroid'
  adb_disable-run-background 'org.fdroid.fdroid'
}
