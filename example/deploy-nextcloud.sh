version='0.9.5'
name='deploy-nextcloud'

fdroid_repos=(
  ["fdroid"]="https://f-droid.org/repo"
)

users=(0)

user_apps=(
  'com.nextcloud.client'            # Nextcloud Client
  'at.bitfire.davdroid'             # DAVx⁵
  'org.dmfs.tasks'                  # OpenTasks
  'fr.unix_experience.owncloud_sms' # Nextcloud SMS
  'de.luhmer.owncloudnewsreader'    # Nextcloud News
  'org.schabi.nxbookmarks'          # Nextcloud Bookmarks
  'com.nextcloud.talk2'             # Nextcloud Talk
  'it.niedermann.owncloud.notes'    # Nextcloud Notes
  'com.nextcloudpasswords'          # Nextcloud Passwords
)

function post_deploy {
  msg2 "fix permissions"

  adb_grant-storage-permission 'com.nextcloud.client'

  adb_grant-permission 'at.bitfire.davdroid' 'android.permission.READ_CONTACTS'
  adb_grant-permission 'at.bitfire.davdroid' 'android.permission.WRITE_CONTACTS'
  adb_grant-permission 'at.bitfire.davdroid' 'android.permission.READ_CALENDAR'
  adb_grant-permission 'at.bitfire.davdroid' 'android.permission.WRITE_CALENDAR'
  adb_grant-permission 'at.bitfire.davdroid' 'org.dmfs.permission.READ_TASKS'
  adb_grant-permission 'at.bitfire.davdroid' 'org.dmfs.permission.WRITE_TASKS'
}
