version='0.9.5'

romdir='./android_rom/samsung_s9_starlte'
gsi_romdir='./android_rom/gsi'
files_tmpdir='./_tmp'

system_image="${gsi_romdir}/lineageos_andy_yan/lineage-19.1-20241019-UNOFFICIAL-arm64_bvN-vndklite.img.gz"
vendor_image="${romdir}/vendor/G960_FXXU5ETF5_gcam_Vendor_20200307.img.gz"
vendor_patch="${romdir}/twrp/samsung-vaultkeeper-kg-etc-disabler-3.1.zip"
kernel_rom="${romdir}/kernel/xxmustafacooTR-V46.zip"
files+=(
  "${system_image}::hash=sha256:5d02b61ff5d91f800deb148c61f24df8b94aa465e0239ba93506c0ded1aabe80"
  "${vendor_image}::hash=sha256:159529ebc35669cbb6864c962df6ade34a837f9281e88d402a6238a03476d2da"
  "${vendor_patch}::hash=sha256:c32af16b7045148525558a1c81ac7f901e18add05d0344945d850f06772e1814"
  "${kernel_rom}::hash=sha256:747560d10972d1ebfbfcdd5488d55b2a1e3e83c7d5ea77c51fbc7d376957906a"
)

function pre_deploy {
  msg2 "Check if under twrp(recovery) mode"
  if ! adb_is-twrp-mode; then
      abort "not twrp(recovery) mode"
  fi

  msg "Wipe cache"
  adb_twrp-wipe-cache

  msg "Flash system"
  adb_flash-image system "${system_image}"

  msg "Flash vendor"
  adb_flash-image vendor "${vendor_image}"

  msg "Fix vendor"
  adb_twrp-flash-zip "${vendor_patch}"

  msg "Flash kernel"
  warning "Need user operate manually, select CLEAN KERNELSU, LATEST or STOCK, FAKE ENFORCING"
  adb_twrp-flash-zip "${kernel_rom}"

  msg "Wipe cache"
  adb_twrp-wipe-cache

  msg "Factory reset"
  adb_twrp-factory-reset

  msg "Done, just reboot system manually"
}
